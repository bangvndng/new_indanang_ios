//
//  AppDelegate.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/12/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "AppDelegate.h"

#import "WSLocation.h"
#import "WSCategory.h"

#import "ILocation.h"
#import "ICategory.h"

#import "SBase.h"


@interface AppDelegate()<WSCategoryDelegate, SDataDelegate>{
    
}



@end

@implementation AppDelegate

@synthesize managedObjectContext        = _managedObjectContext;
@synthesize managedObjectModel          = _managedObjectModel;
@synthesize persistentStoreCoordinator  = _persistentStoreCoordinator;



+ (AppDelegate *)sharedSingleton
{
    static AppDelegate* sharedSingleton;
    if(!sharedSingleton) {
        sharedSingleton = [AppDelegate new];
        //@synchronized(sharedSingleton) {
            
        //}
    }
    
    return sharedSingleton;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    MainVC *mainVC = [[MainVC alloc] init];
    
    self.mainVC = mainVC;
    self.window.rootViewController = self.mainVC;
    [self _customizeAppearance];
    
    [self _loadLocations];
    [self _loadCategories];
    
    [self _loadLocationDetails];
    [self _loadCategoryDetails];
        
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [FBSession.activeSession handleDidBecomeActive];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    
    
    BOOL wasHandled = [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
    
    // add app-specific handling code here
    return wasHandled;
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"new_indanang_ios" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"new_indanang_ios.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - Customize Navigation Bar

- (void) _customizeAppearance
{
    UIImage *gradientImage64 = [[UIImage imageNamed:@"nav_bar_64"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    [[UINavigationBar appearance] setBackgroundImage:gradientImage64 forBarMetrics:UIBarMetricsDefault];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    
}

#pragma mark - Fetch Core Data Location into Array of Location (Static)

- (void) _loadCategories
{
    [SBase dataSource].delegate = (id)self;
    [[SBase dataSource] loadAllCategories:NO];
}

- (void)sData:(SData *)sData didLoadAllCategoriesSuccess:(NSArray *)categories
{
    if (_sharedCategoriesIcon) {
        [_sharedCategoriesIcon removeAllObjects];
    }else{
        _sharedCategoriesIcon = [[NSMutableDictionary alloc] init];
    }
    
    for (ICategory *iCategory in [[SBase dataSource] loadAllCategories]) {
        if ([iCategory.icon_class isEqualToString:@""] || !iCategory.icon_class) {
            [_sharedCategoriesIcon setObject:@"i_cl_activity" forKey:iCategory.category_id];
        }else
        {
            [_sharedCategoriesIcon setObject:iCategory.icon_class forKey:iCategory.category_id];
        }
    }
    
}

- (void)sData:(SData *)sData didLoadAllCategoriesFail:(NSError *)error
{
    
}

- (void) _loadLocations
{
    [SBase dataSource].delegate = (id)self;
    [[SBase dataSource] loadAllLocations:NO];
}

- (void)sData:(SData *)sData didLoadAllLocationsSuccess:(NSArray *)locations
{
    [SBase dataSource].delegate = nil;
}

- (void)sData:(SData *)sData didLoadAllLocationsFail:(NSError *)error
{
    [SBase dataSource].delegate = nil;
}

- (void) _loadBookmarks
{
    
}

- (void) _loadLocationDetails
{
    [SBase dataSource].delegate = (id)self;
    [[SBase dataSource] loadLocationDetails];
}

- (void) _loadCategoryDetails
{
    [SBase dataSource].delegate = (id)self;
    [[SBase dataSource] loadCategoryDetails];
}

# pragma mark - WSCategory Delegate

- (void) wsCategory:(WSCategory *)wsCategory didGetAllCategoriesSuccess:(NSArray *)categories
{
    if ([categories count] == 0) {
        
    }else{
        
        NSLog(@"AppDelegate wsLocation didGetAllCategoriesSuccess begin save core data");
        NSManagedObjectContext *context = [self managedObjectContext];
        
        for (ICategory *iCategory in categories) {
            
            // Create a new managed object
            NSManagedObject *category = [NSEntityDescription insertNewObjectForEntityForName:@"LocationCategory" inManagedObjectContext:context];
            [category setValue:iCategory.category_id        forKey:@"category_id"];
            [category setValue:iCategory.name               forKey:@"name"];
            [category setValue:iCategory.short_description  forKey:@"short_description"];
            [category setValue:iCategory.overview           forKey:@"overview"];
            [category setValue:iCategory.parent_category_id forKey:@"parent_category_id"];
            [category setValue:iCategory.icon_class forKey:@"icon_class"];
        }
        
        NSError *error = nil;
        // Save the object to persistent store
        if (![context save:&error]) {
            NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        }
        
        NSLog(@"AppDelegate wsCategory didGetAllCategoriesSuccess end save core data");
    }
}

- (void) wsCategory:(WSCategory *)wsCategory didGetAllCategoriesFail:(NSError *)error
{
    NSLog(@"AppDelegate wsCategory didGetAllCategoriesFail %@", error);
}

@end
