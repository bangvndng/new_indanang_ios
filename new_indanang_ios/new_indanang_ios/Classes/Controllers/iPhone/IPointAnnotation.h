//
//  IDNPointAnnotation.h
//  indanang
//
//  Created by Bang Ngoc Vu on 06/10/2013.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "ILocation.h"

@interface IPointAnnotation : MKPointAnnotation

- (id)initWithCoordinate:(CLLocationCoordinate2D)coord title:(NSString *)title subtitle:(NSString *)subtitle type:(NSString *)type;

@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) ILocation *locationitem;

@end
