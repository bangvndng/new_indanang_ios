//
//  SearchResultVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/19/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "SearchResultVC.h"
#import "SearchBar.h"

#import "LocationVC.h"

#import "SBase.h"

@interface SearchResultVC ()<UIScrollViewDelegate>{
    
    __weak IBOutlet UITableView *_tableView;
    
    SearchBar *_searchBar;
    UISearchDisplayController *_mySearchDisplayController;
    
    NSMutableArray *_results;
    NSString *_searchText;
}

@end

@implementation SearchResultVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _results = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if ([SDevice isIPhone5]) {
        CGRect frame = _tableView.frame;
        _tableView.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height - 64);
    }else{
        CGRect frame = _tableView.frame;
        _tableView.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height - 88 - 64);
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark - Load data

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void) performSearchWithq:(NSString *)queryString
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Location"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title CONTAINS[c] %@", queryString];
    [fetchRequest setPredicate:predicate];
    
    _results = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    [_tableView reloadData];
}

# pragma mark - TABLE DELEGATES

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_results count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellId = @"DefaultCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellId];
    }
    cell.textLabel.text = [_results[indexPath.row] valueForKey:@"title"];
    cell.detailTextLabel.text = [_results[indexPath.row] valueForKey:@"address"];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"SearchResultVC didSelectRowAtIndexPath");
    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    LocationVC *searchDetailVC = [[LocationVC alloc] init];
    searchDetailVC.location_id = [_results[indexPath.row] valueForKey:@"location_id"];
    
    [[SBase appDelegate].mainVC.centerVC pushViewController:searchDetailVC animated:YES];
    
}

# pragma mark - SEARCH DELEGATES

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    NSLog(@"SearchResultVC searchBarShouldBeginEditing");
    _searchBar = (SearchBar*)searchBar;
    
    [UIView animateWithDuration:0.2 animations:^(void) {
        [_tableView setAlpha:1];
        
    }];
    [(SubCenterAbstract *)self.parentViewController expandSearchBar];
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    NSLog(@"SearchResultVC textDidChange");
    
    if ([searchText isEqualToString:@""]) {
        [_results removeAllObjects];
        [_tableView reloadData];
    }else{
        [_results removeAllObjects];
        [self performSearchWithq:searchText];
        _searchText = searchText;
    }
    
}

- (void)searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller {
    NSLog(@"SearchResultVC searchDisplayControllerWillBeginSearch");
    self.view.alpha = 1;
    [self.parentViewController.view addSubview:_tableView];
 
    _mySearchDisplayController = controller;
    
}

- (void)searchDisplayControllerDidEndSearch:(UISearchDisplayController *)controller {
    NSLog(@"SearchResultVC searchDisplayControllerDidEndSearch");
    /*
    self.isSearching = NO;
    if([controller.searchBar.text isEqualToString:@""]){
        [UIView animateWithDuration:0.25 animations:^{
            self.view.alpha = 0;
        } completion: ^(BOOL finished) {
            //button.hidden = YES;
            [self.view removeFromSuperview];
        }];
    }
     */
    [UIView animateWithDuration:0.2 animations:^(void) {
        [_tableView setAlpha:0];
        
    }];
    [(SubCenterAbstract *)self.parentViewController collapseSearchBar];
    _mySearchDisplayController = nil;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    NSLog(@"SearchResultVC shouldReloadTableForSearchString");
    if (searchString) {
        return YES;
    }else{
        return NO;
    }
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    return YES;
}

# pragma mark - SCROLL VIEW DELEGATE

- (void) scrollViewDidScroll:(UIScrollView *)scrollView
{
    if ([_results count] == 0) {
        
    }else{
        [_searchBar resignFirstResponder];
    }
    
}

@end
