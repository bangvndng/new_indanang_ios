//
//  HomeAccessoriesCell.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/20/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "HomeAccessoriesCell.h"

@implementation HomeAccessoriesCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)openBackground:(id)sender {
    NSLog(@"HomeAccessoriesCell openBackground");
    if (self.delegate && [self.delegate respondsToSelector:@selector(homeAccessoriesCell:openBackground:)]) {
        [self.delegate homeAccessoriesCell:self openBackground:nil];
    }
}

- (IBAction)openPracticalities:(id)sender {
    NSLog(@"HomeAccessoriesCell openPracticalities");
    if (self.delegate && [self.delegate respondsToSelector:@selector(homeAccessoriesCell:openPracticalities:)]) {
        [self.delegate homeAccessoriesCell:self openPracticalities:nil];
    }

}
@end
