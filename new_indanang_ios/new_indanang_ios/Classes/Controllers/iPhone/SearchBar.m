//
//  SearchBar.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/19/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "SearchBar.h"

@implementation SearchBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)layoutSubviews{
    [super layoutSubviews];
    //[self setShowsCancelButton:NO animated:NO];
}

@end
