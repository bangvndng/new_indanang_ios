//
//  MapVC.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/17/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubCenterAbstract.h"

@interface MapVC : SubCenterAbstract

@property (nonatomic) int type;

@property (strong, nonatomic) NSString *map_location_id;
@property (strong, nonatomic) NSString *map_category_id;

@property (strong, nonatomic) NSArray *filtered_locations;

@end