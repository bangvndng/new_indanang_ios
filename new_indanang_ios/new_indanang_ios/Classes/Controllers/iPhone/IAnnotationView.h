//
//  IAnnotationView.h
//  indanang
//
//  Created by Bang Ngoc Vu on 06/10/2013.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "ILocation.h"
@protocol IAnnotationViewProtocol <NSObject>

@optional
- (void)showDetailsLocation:(ILocation *)locationitem;
@end

@interface IAnnotationView : MKAnnotationView

@property (nonatomic, weak) id<IAnnotationViewProtocol> idnAnnotationProtocol;
@property (nonatomic,weak) ILocation *locationitem;

@end
