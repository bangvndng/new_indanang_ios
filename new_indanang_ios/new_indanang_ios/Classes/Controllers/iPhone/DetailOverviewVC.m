//
//  DetailOverviewVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 2/9/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import "DetailOverviewVC.h"
#import "SBase.h"

#import "ILocation.h"

@interface DetailOverviewVC ()<UIWebViewDelegate>{
    
    __weak IBOutlet UITableView *_tableView;
}

@end

@implementation DetailOverviewVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSLog(@"CategoryOverviewVC viewDidLoad");
    NSArray *locationDetail = [[SBase dataSource] loadLocationDetailsByLocationId:_location_id];
//    [SBase dataSource] loadLocationById:<#(NSString *)#>
    NSLog(@"%@", locationDetail);
    
    ILocation *location = [[SBase dataSource] loadLocationById:_location_id];
    
    NSLog(@"%@", location.wysiwyg_content);
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TABLEVIEW DATASOURCE

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 568-64;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MyIdentifier"];
    }
    
    
    //cell.textLabel.text = _category.wysiwyg_content;
    UIWebView *aWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 568-64)];
    //init and create the UIWebView
    
    aWebView.autoresizesSubviews = YES;
    //aWebView.autoresizingMask=(UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth);
    
//    [aWebView setDelegate:(id)self];
    aWebView.delegate = (id)self;
    NSString *urlAddress = [NSString stringWithFormat:@"%@/locations/wysiwyg_view?id=%@",COMMON_WS_URL,_location_id]; // test view is working with url to webpage
    
    NSURL *url = [NSURL URLWithString:urlAddress];
    
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    [aWebView loadRequest:requestObj];
    
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 300)];
    [cell.contentView addSubview:aWebView];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - UIWebview Delegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"shouldStartLoadWithRequest");
    return YES;
}

@end
