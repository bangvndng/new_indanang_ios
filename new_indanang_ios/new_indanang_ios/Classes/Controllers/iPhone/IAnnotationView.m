//
//  IAnnotationView.m
//  indanang
//
//  Created by Bang Ngoc Vu on 06/10/2013.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "IAnnotationView.h"
#import "IPointAnnotation.h"
#import "SBase.h"
@implementation IAnnotationView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id) initWithAnnotation:(IPointAnnotation *)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if(self){
        
        UIImage *annotationImage;
        
        
        //NSString *annotationImageName = [NSString stringWithFormat:@"a_%@", annotation.type];
        NSString *annotationImageName = [[SBase appDelegate].sharedCategoriesIcon objectForKey:annotation.type];
        
        annotationImage = [UIImage imageNamed:annotationImageName];
        
        _locationitem = annotation.locationitem;
        
        self.image = annotationImage;
        self.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
        self.contentMode = UIViewContentModeScaleAspectFill;
        self.centerOffset = CGPointMake(1.0, 1.0);
        
        self.canShowCallout = YES;
        
        NSString *icon_name = [[SBase appDelegate].sharedCategoriesIcon objectForKey:annotation.type];
        //NSLog(@"%@", icon_name);
        UIImageView *leftAccesoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:icon_name]];
        leftAccesoryView.frame = CGRectMake(0.0, 0.0, 20.0, 20.0);
        leftAccesoryView.contentMode = UIViewContentModeScaleAspectFill;
        
        self.leftCalloutAccessoryView = leftAccesoryView;
        
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        [rightButton addTarget:self
                        action:@selector(showLocationDetails:)
              forControlEvents:UIControlEventTouchUpInside];
        
        self.rightCalloutAccessoryView = rightButton;
    }
    
    return self;
}

- (void) showLocationDetails:(id)sender
{
    if (self.idnAnnotationProtocol && [self.idnAnnotationProtocol respondsToSelector:@selector(showDetailsLocation:)]) {
        [self.idnAnnotationProtocol showDetailsLocation:_locationitem];
    }
}

@end
