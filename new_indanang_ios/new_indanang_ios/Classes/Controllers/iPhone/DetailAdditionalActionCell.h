//
//  DetailAdditionalActionCell.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 3/11/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DetailAdditionalActionCellDelete;

@interface DetailAdditionalActionCell : UITableViewCell

@property (weak, nonatomic) id<DetailAdditionalActionCellDelete> delegate;

- (IBAction)selectBooking:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *buttonBooking;

@end

@protocol DetailAdditionalActionCellDelete <NSObject>

@optional

- (void) detailAdditionalActionCell:(DetailAdditionalActionCell *) detailAdditionalActionCell didSelectBooking:(NSDictionary *)info;

@end
