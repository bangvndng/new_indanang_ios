//
//  SuggestionCell.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/22/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ILocation.h"

@protocol SuggestionCellDelegate;

@interface SuggestionCell : UITableViewCell

@property (weak, nonatomic) id<SuggestionCellDelegate>delegate;

@property (weak, nonatomic) IBOutlet UIView *viewWrapper;
@property (weak, nonatomic) IBOutlet UILabel *cell_lb_title;
@property (weak, nonatomic) IBOutlet UILabel *cell_lb_address;
@property (weak, nonatomic) IBOutlet UIImageView *cell_icon;

@property (weak, nonatomic) IBOutlet UIImageView *cell_image;

@property (weak, nonatomic) IBOutlet UIButton *doBookmark;
@property (weak, nonatomic) IBOutlet UIButton *doMap;
@property (weak, nonatomic) IBOutlet UIButton *doDetails;

@property (strong, nonatomic) ILocation *iLocation;

- (IBAction)doBookmark:(id)sender;
- (IBAction)doMap:(id)sender;
- (IBAction)doDetails:(id)sender;



@end

@protocol SuggestionCellDelegate <NSObject>

- (void) suggestionCell:(SuggestionCell *)suggestionCell doBookmark:(ILocation *)iLocation;
- (void) suggestionCell:(SuggestionCell *)suggestionCell doMap:(ILocation *)iLocation;
- (void) suggestionCell:(SuggestionCell *)suggestionCell doDetails:(ILocation *)iLocation;

@end