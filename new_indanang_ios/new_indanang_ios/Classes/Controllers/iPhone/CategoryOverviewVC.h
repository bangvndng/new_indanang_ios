//
//  CategoryOverviewVC.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 2/9/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICategory.h"

@interface CategoryOverviewVC : UIViewController

@property (strong, nonatomic) ICategory *category;
@property (strong, nonatomic) NSArray *categoryDetails;

@end