//
//  RegisterVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/17/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "RegisterVC.h"
#import "SBase.h"
#import "WSUser.h"
#import "LoginVC.h"

@interface RegisterVC ()<WSUserDelegate, LoginVCDelegate, SAccountDelegate>{
    
    __weak IBOutlet UIView *_formRegister;
    __weak IBOutlet UITextField *_tf_password;
    __weak IBOutlet UITextField *_tf_username;
    __weak IBOutlet UITextField *_tf_email;
    
}
- (IBAction)doRegister:(id)sender;
- (IBAction)doRegisterFB:(id)sender;

- (IBAction)doSwitchToLogin:(id)sender;

@end

@implementation RegisterVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [_formRegister.layer setCornerRadius:5.0f];
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelRegister:)];
    
    self.navigationItem.leftBarButtonItem = cancelButton;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)cancelRegister: (id)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didCancelRegister)]) {
        [self.delegate didCancelRegister];
    }
}

#pragma mark - WS DELEGATE
- (void)wsUser:(WSUser *)wsUser didRegisterSuccessWith:(IUser *)user
{
    NSLog(@"LoginVC didRegisterSuccessWith");
    if (self.delegate && [self.delegate respondsToSelector:@selector(didRegisterSuccess)]) {
        [self.delegate didRegisterSuccess];
    }
}

- (void)wsUser:(WSUser *)wsUser didRegisterFailWithError:(NSError *)error
{
    
}

- (void)wsUser:(WSUser *)wsUser didLoginFBSuccessWith:(IUser *)user
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didRegisterSuccess)]) {
        [self.delegate didRegisterSuccess];
    }
}

- (void)wsUser:(WSUser *)wsUser didLoginFBFailWithError:(NSError *)error
{
    
}

#pragma mark - LOGINVC DELEGATE

- (void)didLoginSuccess
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didRegisterSuccess)]) {
        [self.delegate didRegisterSuccess];
    }
}

- (void)didLoginFail
{
    
}

#pragma mark - SACCOUNT DELEGATE

- (void) saccount:(SAccount *)saccount didAuthenticateFBSuccess:(NSDictionary *)fbInfo
{
    NSLog(@"RegisterVC didAuthenticateFBSuccess %@", fbInfo);
    
    NSArray *keys = @[@"fbid", @"email", @"displayname"];
    NSArray *objects = @[
                         [fbInfo objectForKey:@"id"],
                         [fbInfo objectForKey:@"email"],
                         [fbInfo objectForKey:@"name"]
                         ];
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    WSUser *wsUser = [[WSUser alloc] init];
    wsUser.delegate = (id)self;
    
    [wsUser loginFBUser:userInfo];
    
}

- (void) saccount:(SAccount *)saccount didAuthenticateFBFail:(NSError *)error
{
    
    NSLog(@"RegisterVC didAuthenticateFBFail");
    
}

#pragma mark - IBACTIONS

- (IBAction)doRegister:(id)sender {
    if ([_tf_email.text isEqualToString:@""] ||
        [_tf_username.text isEqualToString:@""] ||
        [_tf_password.text isEqualToString:@""]) {
        [SBase showAlert:@"Please complete the form"];
    }else{
        NSArray *keys       = @[@"email",@"password",@"username"];
        NSArray *objects    = @[_tf_email.text, _tf_password.text, _tf_username.text];
        
        NSDictionary *postUser = [[NSDictionary alloc]initWithObjects:objects forKeys:keys];
        
        WSUser *wsUser = [[WSUser alloc] init];
        wsUser.delegate = (id) self;
        
        [wsUser registerUser:postUser];
    }
}

- (IBAction)doRegisterFB:(id)sender {
    [SBase currentAccount].delegate = (id)self;
    [[SBase currentAccount] authenticateFB];
}

- (IBAction)doSwitchToLogin:(id)sender {
    
    LoginVC *loginVC = [[LoginVC alloc] init];
    loginVC.isSwitch = YES;
    loginVC.delegate = (id) self;
    [self.navigationController pushViewController:loginVC animated:YES];
    
}



@end
