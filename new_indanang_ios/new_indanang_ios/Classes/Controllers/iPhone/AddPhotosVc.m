//
//  AddPhotosVc.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/22/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "AddPhotosVc.h"
#import "SBase.h"

#import "WSPhoto.h"

@interface AddPhotosVc ()<UIImagePickerControllerDelegate, WSPhotoDelegate>{
    UIBarButtonItem *_barButtonNext;
    
    __weak IBOutlet UISwitch *_switchFaceBook;
    
    __weak IBOutlet UIImageView *_uploadImage;
    
    __weak IBOutlet UIView *_viewUpFB;
    
}
- (IBAction)performTakePhoto:(id)sender;
- (IBAction)performChoosePhoto:(id)sender;

@end

@implementation AddPhotosVc

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *barButtonCancel = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                        style:UIBarButtonItemStyleBordered
                                                                       target:self
                                                                       action:@selector(performCancel:)];
    self.navigationItem.leftBarButtonItem = barButtonCancel;
    
    UIBarButtonItem *barButtonNext = [[UIBarButtonItem alloc] initWithTitle:@"Next"
                                                                      style:UIBarButtonItemStyleBordered
                                                                     target:self
                                                                     action:@selector(performNext:)];
    _barButtonNext = barButtonNext;
    _barButtonNext.enabled = NO;
    
    self.navigationItem.rightBarButtonItem = barButtonNext;

    if ([SAccount currentUser].isFBUser) {
        
    }else{
        _viewUpFB.alpha = 0;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)performNext: (id)sender
{
    NSLog(@"AddPhotosVc performNext");
    
    
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        [SBase showAlert:@"Thank you for added photo for this place"];
        
        
    }];
}

- (void)performCancel: (id)sender
{
    NSLog(@"AddPhotosVc perfornCancel");
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (IBAction)performTakePhoto:(id)sender {
    NSLog(@"AddPhotoVC performTakePhoto");
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.delegate=(id)self;
    imagePickController.allowsEditing=TRUE;
    imagePickController.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:imagePickController animated:YES completion:^{
    
    }];
}

- (IBAction)performChoosePhoto:(id)sender {
    NSLog(@"AddPhotoVC performChoosePhoto");
    
    UIImagePickerController *imagePickController=[[UIImagePickerController alloc]init];
    imagePickController.delegate=(id)self;
    imagePickController.allowsEditing=TRUE;
    imagePickController.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:imagePickController animated:YES completion:^{
        
    }];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
	[self dismissViewControllerAnimated:YES completion:^{}];
    
    UIImage *uploadedImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    _uploadImage.contentMode = UIViewContentModeScaleAspectFit;
        
    WSPhoto *wsPhoto = [[WSPhoto alloc] init];
    wsPhoto.delegate = (id)self;
    [wsPhoto uploadPhoto:uploadedImage forLocationId:_location_id];
    
}

#pragma mark - Upload Photo Delegate

- (void)wsPhoto:(WSPhoto *)wsPhoto didUploadPhotoSuccess:(NSDictionary *)info
{
    NSDictionary *photoData = [[info objectForKey:@"data"] objectForKey:@"Photo"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *imageUrl = [NSString stringWithFormat:@"%@/%@", COMMON_WS_URL, [photoData objectForKey:@"path"]];
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            _uploadImage.image = [UIImage imageWithData:imageData];
            _barButtonNext.enabled = YES;
            
            if (_switchFaceBook.on == YES) {
                [self performPublishAction:^{
                    FBRequestConnection *connection = [[FBRequestConnection alloc] init];
                    connection.errorBehavior = FBRequestConnectionErrorBehaviorReconnectSession
                    | FBRequestConnectionErrorBehaviorAlertUser
                    | FBRequestConnectionErrorBehaviorRetry;
                    
                    [connection addRequest:[FBRequest requestForUploadPhoto:[UIImage imageWithData:imageData]]
                         completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                             NSLog(@"AddPhotoVC Upload photo to facebook complete");
                             [SBase showAlert:@"Your Photo has been successfully post to your Facebook"];
                             //[self showAlert:@"Photo Post" result:result error:error];
                             if (FBSession.activeSession.isOpen) {
                                 //self.buttonPostPhoto.enabled = YES;
                             }
                         }];
                    [connection start];
                    
                    //self.buttonPostPhoto.enabled = NO;
                }];

            }
            
            
        });
    });
    
}

- (void)wsPhoto:(WSPhoto *)wsPhoto didUploadPhotoFail:(NSError *)error
{
    
}

#pragma mark - Facebook post

// Convenience method to perform some action that requires the "publish_actions" permissions.
- (void) performPublishAction:(void (^)(void)) action {
    // we defer request for permission to post to the moment of post, then we check for the permission
    //if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound) {
        [FBSession openActiveSessionWithPublishPermissions:@[@"publish_actions"] defaultAudience:FBSessionDefaultAudienceFriends allowLoginUI:YES completionHandler:^(FBSession *session,FBSessionState state, NSError *error) {
            if (!error) {
                action();
            } else if (error.fberrorCategory != FBErrorCategoryUserCancelled){
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Permission denied"
                                                                    message:@"Unable to get permission to post"
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                [alertView show];
            }
        }];
        
    //} else {
    //    NSLog(@"AddTipVC found publish_actions");
    //    action();
    //}
    
}

@end
