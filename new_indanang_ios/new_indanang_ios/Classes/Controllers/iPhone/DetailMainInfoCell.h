//
//  DetailMainInfoCell.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/22/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DetailMainInfoCellDelegate ;

@interface DetailMainInfoCell : UITableViewCell

@property (weak, nonatomic) id<DetailMainInfoCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;

- (IBAction)recomment:(id)sender;
- (IBAction)bookmark:(id)sender;
- (IBAction)direction:(id)sender;
- (IBAction)contact:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *lbLocationName;
@property (weak, nonatomic) IBOutlet UILabel *lbLocationAddress;
@property (weak, nonatomic) IBOutlet UIImageView *imageLocationIcon;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicatorImages;

@property (weak, nonatomic) IBOutlet UIButton *bt_bookmark;
@property (weak, nonatomic) IBOutlet UIButton *bt_recommend;


@end

@protocol DetailMainInfoCellDelegate <NSObject>

- (void) detailMainInfoCell:(DetailMainInfoCell *)detailMainInfoCell performRecomment:(NSDictionary *) recommentInfo;

- (void) detailMainInfoCell:(DetailMainInfoCell *)detailMainInfoCell performBookmark:(NSDictionary *) info;

- (void) detailMainInfoCell:(DetailMainInfoCell *)detailMainInfoCell performDirection:(NSDictionary *) info;

- (void) detailMainInfoCell:(DetailMainInfoCell *)detailMainInfoCell performContact:(NSDictionary *) info;

@end