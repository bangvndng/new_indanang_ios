//
//  DirectionPopupVC.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/22/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DirectionPopupVCDelegate;


@interface DirectionPopupVC : UIViewController

@property (weak, nonatomic) id<DirectionPopupVCDelegate> delegate;

@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSArray *transporters;

@property (weak, nonatomic) IBOutlet UILabel *lbAddress;
@property (weak, nonatomic) IBOutlet UIView *viewTransporters;

@property (weak, nonatomic) IBOutlet UITableView *tableTransporters;

- (IBAction)showMap:(id)sender;

@end

@protocol DirectionPopupVCDelegate <NSObject>

- (void) popupDirection:(DirectionPopupVC *)popupDirection showMap:(NSDictionary *)info;
- (void) popupDirection:(DirectionPopupVC *)popupDirection callTransporters:(NSDictionary *)info;

@end