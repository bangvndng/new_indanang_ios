//
//  BookingVC.m
//  new_indanang_ios
//
//  Created by Vu Ngoc Bang on 3/18/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import "BookingVC.h"
#import "BookingCell.h"

#import "SBase.h"

@interface BookingVC ()<SDataDelegate>{
    
    __weak IBOutlet UITableView *_tableView;
    
    NSMutableArray *_bookings;
    NSMutableDictionary *_locationIndex;
    NSMutableDictionary *_locationAddressIndex;
}

@end

@implementation BookingVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"Booking Manager";
    
    [self registerNibsForTableView];
    
    [self _loadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) registerNibsForTableView
{
    NSArray *cells = @[@"BookingCell"];
    
    for (NSString *cellClass in cells) {
        [_tableView registerNib:[UINib  nibWithNibName:cellClass
                                                bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:cellClass];
    }
}

#pragma mark - Load Data

- (void) _loadData
{
    _bookings = [[NSMutableArray alloc] init];
    
    [SBase dataSource].delegate = (id) self;
    [[SBase dataSource] loadAllBooking:YES];
}

- (void) sData:(SData *)sData didLoadAllBookingsSuccess:(NSArray *)bookings
{
    _bookings = [bookings mutableCopy];
    
    _locationIndex = [[NSMutableDictionary alloc] init];
    _locationAddressIndex = [[NSMutableDictionary alloc] init];
    
    for (NSDictionary *booking in bookings) {
        NSString *location_id = [booking valueForKey:@"location_id"];
        
        if ([_locationIndex objectForKey:location_id]) {
            
        }else{
            ILocation *location = [[SBase dataSource] loadLocationById:location_id];
            [_locationIndex setObject:location.title forKey:location_id];
            [_locationAddressIndex setObject:location.address forKey:location_id];
        }
    }
    
    [_tableView reloadData];
}

- (void) sData:(SData *)sData didLoadAllBookingsFail:(NSError *)error
{
    
}

#pragma mark - TABLEVIEW DATASOURCE

/*
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if ([tableView.dataSource tableView:tableView numberOfRowsInSection:section] == 0) {
        return 0;
    } else {
        // whatever height you'd want for a real section header
        return 30;
    }
}
*/

/*
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [[_categories objectForKey:[NSString stringWithFormat:@"%@", _listTypes[section]]] valueForKey:@"name"];
}
*/


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_bookings count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 116;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellId = @"BookingCell";
    BookingCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (cell == nil) {
        cell = [[BookingCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellId];
    }
    
    NSString *checkin = [[_bookings objectAtIndex:indexPath.row] objectForKey:@"checkin"];
    NSString *checkout = [[_bookings objectAtIndex:indexPath.row] objectForKey:@"checkout"];
    NSString *location_id = [[_bookings objectAtIndex:indexPath.row] objectForKey:@"location_id"];
    NSString *location_address = [_locationAddressIndex objectForKey:location_id];
    NSString *location_title = [_locationIndex objectForKey:location_id];
    
    //cell.textLabel.text = [NSString stringWithFormat:@"%@ %@",[_locationIndex objectForKey:location_id] , checkin];
    
    cell.cellTitle.text = location_title;
    cell.cellAddress.text = location_address;
    cell.cellCheckin.text = checkin;
    cell.cellCheckout.text = checkout;
    
    if ([[[_bookings objectAtIndex:indexPath.row] objectForKey:@"status"] isEqualToString:@"1"]) {
        cell.cellStatus.text = @"Confirmed";
    }
    
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

@end
