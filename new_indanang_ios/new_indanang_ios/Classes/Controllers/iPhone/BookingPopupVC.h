//
//  BookingPopupVC.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 3/22/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol BookingPopupVCDelegate;

@interface BookingPopupVC : UIViewController

@property (weak, nonatomic) id<BookingPopupVCDelegate> delegate;

@property (strong, nonatomic) NSString *location_id;

@property (weak, nonatomic) IBOutlet UITextField *tf_checkin;
@property (weak, nonatomic) IBOutlet UITextField *tf_checkout;
@property (weak, nonatomic) IBOutlet UITextField *tf_quantity;

@property (weak, nonatomic) IBOutlet UIStepper *step_quantity;
- (IBAction)step_value_change:(id)sender;
- (IBAction)checkAvaiability:(id)sender;

- (IBAction)step_editing_change:(id)sender;

@end

@protocol BookingPopupVCDelegate <NSObject>

@optional

- (void) bookingPopupVC:(BookingPopupVC *) bookingPopupVc didPressCheckButtonWithData:(NSDictionary *)data;

- (void) bookingPopupVC:(BookingPopupVC *)bookingPopupVc didCheckAvailabilitySuccessWithAvailable:(NSDictionary *)data;
- (void) bookingPopupVC:(BookingPopupVC *)bookingPopupVc didCheckAvailabilitySuccessWithNoAvailable:(NSDictionary *)data;

@end