//
//  DirectionPopupVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/22/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "DirectionPopupVC.h"

#import "ITransporter.h"

@interface DirectionPopupVC ()

@end

@implementation DirectionPopupVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSLog(@"DirectionPopupVC address %@", _address);
    NSLog(@"DirectionPopupVC transporters %@", _transporters);
    
    [self _setupView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup View

- (void) _setupView
{
    _lbAddress.text = _address;
    
    /*
    int count_transporter = [_transporters count];
    int count = 0;
    for (ITransporter *transporter in _transporters) {
        UILabel *lb_name = [[UILabel alloc] initWithFrame:CGRectMake(0, 20 * count , 200, 20)];
        lb_name.text = [transporter valueForKey:@"name"];
        [lb_name setFont:[UIFont systemFontOfSize:11]];
        
        count++;
        [_viewTransporters addSubview:lb_name];
    }
    */
    
    //_viewTransporters
}

# pragma mark - TABLE VIEW DATASOURCE

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_transporters count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *CellId = @"DefaultCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellId];
    }
    
    cell.textLabel.text = [_transporters[indexPath.row] valueForKey:@"name"];
    cell.detailTextLabel.text = [_transporters[indexPath.row] valueForKey:@"descriptions"];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(popupDirection:callTransporters:)]) {
        [self.delegate popupDirection:self callTransporters:_transporters[indexPath.row]];
    }
}

- (IBAction)showMap:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(popupDirection:showMap:)]) {
        [self.delegate popupDirection:self showMap:nil];
    }
}

@end
