//
//  HomeAccessoriesCell.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/20/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HomeAccessoriesCellDelegate;

@interface HomeAccessoriesCell : UITableViewCell

@property (weak, nonatomic) id<HomeAccessoriesCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *cellLabelTime;
@property (weak, nonatomic) IBOutlet UILabel *cellWeatherForecast;

- (IBAction)openBackground:(id)sender;
- (IBAction)openPracticalities:(id)sender;

@end

@protocol HomeAccessoriesCellDelegate <NSObject>

- (void) homeAccessoriesCell:(HomeAccessoriesCell *)homeAccessoriesCell openBackground:(NSDictionary *)info;
- (void) homeAccessoriesCell:(HomeAccessoriesCell *)homeAccessoriesCell openPracticalities:(NSDictionary *)info;

@end