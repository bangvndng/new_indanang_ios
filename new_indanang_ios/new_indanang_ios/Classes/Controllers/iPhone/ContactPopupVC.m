//
//  ContactPopupVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/22/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "ContactPopupVC.h"

@interface ContactPopupVC (){
    
    __weak IBOutlet UITableView *_tableViewContact;
    
}

@end

@implementation ContactPopupVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark - TABLE VIEW DATASOURCE

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_contactsInfo count];;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"ContactPopupVC cellForRowAtIndexPath");
    
    NSString *CellId = @"CategoryListCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellId];
    }
    NSLog(@"%ld",(long)indexPath.row);
    if (_contactsInfo) {
        cell.textLabel.text = [_contactsInfo[indexPath.row] objectAtIndex:1];
        cell.detailTextLabel.text = [_contactsInfo[indexPath.row] objectAtIndex:0];
    }
    
    
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"ContactPopupVC didSelectRowAtIndexPath");
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(popupContact:callContact:)])
    {
        NSLog(@"ContactPopupVC respondsToSelector");
        [self.delegate popupContact:self callContact:[_contactsInfo[indexPath.row] copy]];
    }
}

@end
