//
//  SuggestionsListVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/21/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "SuggestionsListVC.h"
#import "Define.h"

#import "SuggestionCell.h"
#import "LocationVC.h"

#import "Location.h"
#import "WSLocation.h"

#import <QuartzCore/QuartzCore.h>

#import "MapVC.h"
#import "SBase.h"

@interface SuggestionsListVC ()<SuggestionCellDelegate, WSLocationDelegate>{
    
    __weak IBOutlet UITableView *_tableView;
    
    //PULL TO REFRESH
    BOOL                                    _isFirstTouch, _isLoading;
    
    UIView                                  *refreshHeaderView;
    UILabel                                 *refreshLabel;
    UIImageView                             *refreshArrow;
    UIActivityIndicatorView                 *refreshSpinner;
    BOOL                                    isDragging;
    BOOL                                    isLoading;
    NSString                                *textPull;
    NSString                                *textRelease;
    NSString                                *textLoading;
    // ------------- //
    
    NSMutableArray *_locations;
    NSMutableDictionary *_images;
}

@end

@implementation SuggestionsListVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupStrings];
    [self addPullToRefreshHeader];
    [self registerNibsForTableView];
    [self loadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) registerNibsForTableView
{
    NSArray *cells = @[@"SuggestionCell"];
    
    for (NSString *cellClass in cells) {
        [_tableView registerNib:[UINib  nibWithNibName:cellClass
                                                bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:cellClass];
    }
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void) loadData
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Location"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"suggestion > %@", @0];
    [fetchRequest setPredicate:predicate];
    
    _locations = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    if ([_locations count] > 0) {
        _images = [[NSMutableDictionary alloc] init];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            int count = 0;
            for (Location *location in _locations) {
                
                if (![location valueForKey:@"thumbnail"] || [[location valueForKey:@"thumbnail"] isEqualToString:@""]) {
                    
                }else{
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        NSString *imageURL = [NSString stringWithFormat:@"%@/%@", COMMON_WS_URL, [location valueForKey:@"thumbnail"]];
                        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageURL]];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (imageData) {
                                [_images setObject:[UIImage imageWithData:imageData] forKey:[NSString stringWithFormat:@"%i", count]];
                                
                                SuggestionCell *locationCell =(SuggestionCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:count inSection:0]];
                                locationCell.cell_image.image = [UIImage imageWithData:imageData];
                            }
                            
                            
                        });
                    });
                }
                count ++;
            }
        });
    }

    [_tableView reloadData];
}

#pragma mark - INHERITANCE FUNCTION

- (void) showMap:(id) sender
{
    MapVC *mapVC = [[MapVC alloc] init];
    mapVC.type = MAP_TYPE_SUGGESTION;
    
    [self.navigationController pushViewController:mapVC animated:YES];
}

#pragma mark - TABLEVIEW DATASOURCE

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_locations count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 335;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *CellId = @"SuggestionCell";
    SuggestionCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (cell == nil) {
        cell = [[SuggestionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellId];
    }
    
    // border radius
    [cell.viewWrapper.layer setCornerRadius:5.0f];
    
    // border
    [cell.viewWrapper.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [cell.viewWrapper.layer setBorderWidth:0.5f];
    
    cell.cell_lb_title.text = [_locations[indexPath.row] valueForKey:@"title"];
    cell.cell_lb_address.text = [_locations[indexPath.row] valueForKey:@"address"];
    
    if ([_images objectForKey:[NSString stringWithFormat:@"%i", indexPath.row]]) {
        UIImage *image = [_images objectForKey:[NSString stringWithFormat:@"%i", indexPath.row]];
        cell.cell_image.image = image;
    }else{
        cell.cell_image.image = [UIImage imageNamed:@"noimage"];
    }
    
    cell.iLocation = [[SBase dataSource] loadLocationById:[_locations[indexPath.row] valueForKey:@"location_id"]];
    cell.delegate = (id)self;
    
    if ([[SBase dataSource] isBookmarked:[_locations[indexPath.row] valueForKey:@"location_id"]]) {
        [cell.doBookmark setTitle:@"Bookmarked" forState:UIControlStateNormal];
        cell.doBookmark.enabled = NO;
    }
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    LocationVC *location = [[LocationVC alloc] init];
    location.location_id = [_locations[indexPath.row] valueForKey:@"location_id"];
    
    [self.navigationController pushViewController:location animated:YES];
    
}

#pragma mark - SUGGESTION CELL DELEGATE

- (void)suggestionCell:(SuggestionCell *)suggestionCell doBookmark:(ILocation *)iLocation
{
    [[SBase dataSource] addBookmark:iLocation.location_id];
    suggestionCell.doBookmark.enabled = NO;
    [suggestionCell.doBookmark setTitle:@"Bookmarked" forState:UIControlStateNormal];
}

- (void)suggestionCell:(SuggestionCell *)suggestionCell doMap:(ILocation *)iLocation
{
    MapVC *mapVC = [[MapVC alloc] init];
    mapVC.type = MAP_TYPE_SINGLE;
    mapVC.map_location_id = iLocation.location_id;
    
    [self.navigationController pushViewController:mapVC animated:YES];
}

- (void)suggestionCell:(SuggestionCell *)suggestionCell doDetails:(ILocation *)iLocation
{
    LocationVC *location = [[LocationVC alloc] init];
    location.location_id = iLocation.location_id;
    
    [self.navigationController pushViewController:location animated:YES];
}

#pragma mark - Pull CODE
- (void)setupStrings{
    
    textPull    = @"Pull down to refresh";             //@"Pull down to refresh...";
    textRelease = @"Release to refresh";                   //@"Release to refresh...";
    textLoading = @"Now loading";                             //@"Loading...";
    
}

- (void)addPullToRefreshHeader {
    
    refreshHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 - REFRESH_HEADER_HEIGHT, 320, REFRESH_HEADER_HEIGHT)];
    refreshHeaderView.backgroundColor = [UIColor clearColor];
    
    refreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(110, 0, 320, REFRESH_HEADER_HEIGHT)];
    refreshLabel.backgroundColor = [UIColor clearColor];
    refreshLabel.font = [UIFont boldSystemFontOfSize:13.0];
    refreshLabel.textColor = [UIColor colorWithRed:197.0/255.0 green:187.0/255.0 blue:184.0/255.0 alpha:1.0];
    
    refreshArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pull_arrow.png"]];
    refreshArrow.frame = CGRectMake(floorf((REFRESH_HEADER_HEIGHT - 13) / 2 + 70),
                                    (floorf(REFRESH_HEADER_HEIGHT - 26) / 2),
                                    13, 26);
    
    refreshSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    refreshSpinner.frame = CGRectMake(floorf(floorf(REFRESH_HEADER_HEIGHT - 20) / 2) + 70 , floorf((REFRESH_HEADER_HEIGHT - 20) / 2), 20, 20);
    refreshSpinner.hidesWhenStopped = YES;
    
    [refreshHeaderView addSubview:refreshLabel];
    [refreshHeaderView addSubview:refreshArrow];
    [refreshHeaderView addSubview:refreshSpinner];
    [_tableView addSubview:refreshHeaderView];
    
}

- (void)startLoading {
    isLoading = YES;
    
    // Show the header
    [UIView animateWithDuration:0.3 animations:^{
        _tableView.contentInset = UIEdgeInsetsMake(REFRESH_HEADER_HEIGHT, 0, 0, 0);
        refreshLabel.text = textLoading;
        refreshArrow.hidden = YES;
        [refreshSpinner startAnimating];
    } completion:^(BOOL finished) {
        
        WSLocation *wsLocation = [[WSLocation alloc] init];
        wsLocation.delegate = (id)self;
        
        [wsLocation getSuggestion];
        
        [UIView animateWithDuration:0.5 animations:^{
            [self stopLoading];
        }];
    }];
}

- (void)stopLoading {
    isLoading = NO;
    
    // Hide the header
    [UIView animateWithDuration:0.3 animations:^{
        _tableView.contentInset = UIEdgeInsetsZero;
        [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
    }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(stopLoadingComplete)];
                     }];
}

- (void)stopLoadingComplete {
    // Reset the header
    refreshLabel.text = textPull;
    refreshArrow.hidden = NO;
    [refreshSpinner stopAnimating];
}

- (void)refresh {
    // This is just a demo. Override this method with your custom reload action.
    // Don't forget to call stopLoading at the end.
    [self performSelector:@selector(stopLoading) withObject:nil afterDelay:2.0];
}



#pragma mark - UIScrollDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (scrollView == _tableView) {
        if (isLoading) return;
        isDragging = YES;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == _tableView) {
        if (isLoading) {
            // Update the content inset, good for section headers
            if (scrollView.contentOffset.y > 0)
                _tableView.contentInset = UIEdgeInsetsZero;
            else if (scrollView.contentOffset.y >= -REFRESH_HEADER_HEIGHT)
                _tableView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        } else if (isDragging && scrollView.contentOffset.y < 0) {
            // Update the arrow direction and label
            [UIView animateWithDuration:0.25 animations:^{
                if (scrollView.contentOffset.y < -REFRESH_HEADER_HEIGHT) {
                    // User is scrolling above the header
                    refreshLabel.text = textRelease;
                    [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
                } else {
                    // User is scrolling somewhere within the header
                    refreshLabel.text = textPull;
                    [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
                }
            }];
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView == _tableView) {
        if (!decelerate) {
            if (scrollView.contentOffset.y <= scrollView.contentSize.height - 1.0 * scrollView.frame.size.height)// &&
                //[_feeds count] < _total &&
                //!_isLoading)
            {
                //[self loadMoreFeeds];
            }
        }
        
        if (isLoading) return;
        isDragging = NO;
        if (scrollView.contentOffset.y <= -REFRESH_HEADER_HEIGHT) {
            // Released above the header
            [self startLoading];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == _tableView) {
        if (scrollView.contentOffset.y <= scrollView.contentSize.height - 1.0 * scrollView.frame.size.height) //&&
            //[_feeds count] < _total  &&
            //!_isLoading)
        {
            //[self loadMoreFeeds];
        }
    }
}

#pragma mark - WSLOCATION DELEGATE

- (void) wsLocation:(WSLocation *)wsLocation didGetSuggestionSuccess:(NSArray *)locations
{
    [self loadData];
    [self stopLoading];
}

- (void) wsLocation:(WSLocation *)wsLocation didGetSuggestionFail:(NSError *)error
{
    [self stopLoading];
}

@end
