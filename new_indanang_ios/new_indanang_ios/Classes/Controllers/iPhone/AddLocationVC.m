//
//  AddLocationVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/31/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "AddLocationVC.h"

#import "AddLocationTextCell.h"
#import "AddLocationSelectCell.h"
#import "SelectCategoriesListVC.h"
#import "SBase.h"
#import "LocationManager.h"
#import "WSLocation.h"

@interface AddLocationVC ()<SelectCategoriesListVCDelegate, WSLocationDelegate>{
    
    __weak IBOutlet UITableView *_tableView;
    
    NSArray *_items;
    
    AddLocationSelectCell *_selectCategoryCell;
    AddLocationTextCell *_nameCell;
    AddLocationTextCell *_addressCell;
    
    ICategory *_selectedCategory;
}

@end

@implementation AddLocationVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _items = @[
               @[
                   @"textfield",
                   @"Name",
                   @"Location name",
                   @"AddLocationTextCell"
                   ],
               @[
                   @"textfield",
                   @"Address",
                   @"Location address",
                   @"AddLocationTextCell"
                   ],
               @[
                   @"select",
                   @"Category",
                   @"Location address",
                   @"AddLocationSelectCell"
                   ]
               ];
    
    [self registerNibsForTableView];
    
    UIBarButtonItem *barButtonNext = [[UIBarButtonItem alloc] initWithTitle:@"Save"
                                                                      style:UIBarButtonItemStyleBordered
                                                                     target:self
                                                                     action:@selector(performNext:)];
    self.navigationItem.rightBarButtonItem = barButtonNext;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) registerNibsForTableView
{
    NSArray *cells = @[@"AddLocationTextCell", @"AddLocationSelectCell"];
    
    for (NSString *cellClass in cells) {
        [_tableView registerNib:[UINib  nibWithNibName:cellClass
                                                bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:cellClass];
    }
}

- (void) performNext:(id)sender
{
    NSLog(@"AddLocation performNext");
    if ([_addressCell.cellTextfield.text isEqualToString:@""] || [_nameCell.cellTextfield.text isEqualToString:@""]) {
        [SBase showAlert:@"Please complete the text fields"];
    }else{
        if ([_selectCategoryCell.cellValue.text isEqualToString:@"Please select"]) {
            [SBase showAlert:@"Please select Category"];
        }else{
            
            if ([[LocationManager sharedSingleton].locationManager location]) {
                WSLocation *wsLocation = [[WSLocation alloc] init];
                
                NSArray *keys = @[@"title", @"address", @"latitude", @"longitude", @"category_id"];
                
                NSString *latitude = [NSString stringWithFormat:@"%f", [[LocationManager sharedSingleton].locationManager location].coordinate.latitude];
                NSString *longitude = [NSString stringWithFormat:@"%f", [[LocationManager sharedSingleton].locationManager location].coordinate.longitude];
                
                NSArray *objects = @[
                                     _nameCell.cellTextfield.text,
                                     _addressCell.cellTextfield.text,
                                     latitude,
                                     longitude,
                                     _selectedCategory.category_id
                                     ];

                NSDictionary *locationInfo = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
                
                wsLocation.delegate = (id)self;
                [wsLocation createLocation:locationInfo];
                
            }else{
                [SBase showAlert:@"Can not detect your current location"];
            }
            
        }
    }
    
}

# pragma mark - TABLE VIEW DATASOURCE

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_items count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[_items[indexPath.row] objectAtIndex:0] isEqualToString:@"textfield"]) {
        return [self setAddLocationTextCell:tableView cellForRowAtIndexPath:indexPath];
    }else if([[_items[indexPath.row] objectAtIndex:0] isEqualToString:@"select"]){
        return [self setAddLocationSelectCell:tableView cellForRowAtIndexPath:indexPath];
    }else{
        return nil;
    }
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[_items[indexPath.row] objectAtIndex:1] isEqualToString:@"Category"])
    {
        SelectCategoriesListVC *selectCategories = [[SelectCategoriesListVC alloc] init];
        selectCategories.delegate = (id)self;
        [self.navigationController pushViewController:selectCategories animated:YES];
    }
}

#pragma mark - CELL SETUP

- (AddLocationTextCell *) setAddLocationTextCell:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellId = @"AddLocationTextCell";
    AddLocationTextCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (cell == nil) {
        cell = [[AddLocationTextCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellId];
    }
    cell.cellTitle.text = [_items[indexPath.row] objectAtIndex:1];
    cell.cellTextfield.placeholder = [_items[indexPath.row] objectAtIndex:2];
    //[cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    if ([[_items[indexPath.row] objectAtIndex:1] isEqualToString:@"Name"])
    {
        _nameCell = cell;
    }else if ([[_items[indexPath.row] objectAtIndex:1] isEqualToString:@"Address"])
    {
        _addressCell = cell;
    }
    
    return cell;
}

- (AddLocationSelectCell *) setAddLocationSelectCell:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellId = @"AddLocationSelectCell";
    AddLocationSelectCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (cell == nil) {
        cell = [[AddLocationSelectCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellId];
    }
    cell.cellTitle.text = [_items[indexPath.row] objectAtIndex:1];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    if ([[_items[indexPath.row] objectAtIndex:1] isEqualToString:@"Category"]){
        _selectCategoryCell = cell;
    }
    
    return cell;
}

#pragma mark - SELECTCATEGORY DELEGATE

- (void)selectCategory:(SelectCategoriesListVC *)selectCategory didSelect:(ICategory *)category
{
    _selectedCategory = category;
    _selectCategoryCell.cellValue.text = category.name;
}

#pragma mark - WSLOCATION DELEGATE

-(void)wsLocation:(WSLocation *)wsLocation didCreateLocationSuccess:(ILocation *)location
{
    NSLog(@"AddLocationVC wsLocation didCreateLocationSuccess");
    [SBase showAlert:@"Your new location has been added successfully"];
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)wsLocation:(WSLocation *)wsLocation didCreateLocationFail:(NSError *)error
{
    
}

@end
