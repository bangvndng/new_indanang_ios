//
//  CategoryDetailVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/19/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "CategoryDetailVC.h"
#import "LocationVC.h"

#import "SBase.h"

#import "LocationCell.h"
#import "Location.h"

#import "WSLocation.h"
#import "WSCategory.h"

#import "CategoryDetailOverviewVC.h"

#define FLAG_UI_CATEGORYDETAIL_AZ       1
#define FLAG_UI_CATEGORYDETAIL_NEAR     2
#define FLAG_UI_CATEGORYDETAIL_POPULAR  3
#define FLAG_UI_CATEGORYDETAIL_PRICE    4
#define FLAG_UI_CATEGORYDETAIL_STAR     5

#import "Define.h"

#import "UIViewController+MJPopupViewController.h"

#import "MapVC.h"

#import "CategoryOverviewVC.h"

@interface CategoryDetailVC ()<WSLocationDelegate, WSCategoryDelegate, UITextFieldDelegate, UIActionSheetDelegate>{
    
    __weak IBOutlet UITableView *_tableView;
    
    IBOutlet UIView *_viewFilter;
    
    
    //PULL TO REFRESH
    BOOL                                    _isFirstTouch, _isLoading;
    
    UIView                                  *refreshHeaderView;
    UILabel                                 *refreshLabel;
    UIImageView                             *refreshArrow;
    UIActivityIndicatorView                 *refreshSpinner;
    BOOL                                    isDragging;
    BOOL                                    isLoading;
    NSString                                *textPull;
    NSString                                *textRelease;
    NSString                                *textLoading;
    
    
    // ------------- //
    
    NSArray * _locations;
    NSArray * _filteredLocations;
    NSMutableDictionary *_images;
    NSArray *_categoryDetails;
    
    int _flag_filter;
    
    
    __weak IBOutlet UIButton *_bt_filter_popular;
    __weak IBOutlet UIButton *_bt_filter_nearme;
    __weak IBOutlet UIButton *_bt_filterAZ;
    __weak IBOutlet UIButton *_bt_filter_price;
    __weak IBOutlet UIButton *_bt_filter_star;
    
    
    __weak IBOutlet UIScrollView *_scrollviewFilterPrice;
    
    /* UIDATEPICKER */
    UIDatePicker    *_theDatePicker;
    UIToolbar       *_pickerToolbar;
    UIActionSheet   *_pickerViewDate;
    
    UITextField     *_selectedTextField;
    
    /* DATE FILTER OUTLETS */
    IBOutlet UITextField     *_stepperValue;
    IBOutlet UITextField     *_checkinDate;
    IBOutlet UITextField    *_checkoutDate;
    IBOutlet UIButton       *_checkButton;
    IBOutlet UIStepper      *_quantityStepper;
    
    NSString *_checkinDateVal;
    NSString *_checkoutDateVal;
    NSString *_quantityVal;
    
    BOOL isFiltered;
}

@property (weak, nonatomic) IBOutlet UILabel *lb_overview;

- (IBAction)_filterAZ:(id)sender;
- (IBAction)_filterNearme:(id)sender;
- (IBAction)_filterPopular:(id)sender;
- (IBAction)_filterPrice:(id)sender;
- (IBAction)_filterStar:(id)sender;

- (IBAction)_continueReading:(id)sender;


@end

@implementation CategoryDetailVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self addPullToRefreshHeader];
    
    UIView *tableHeader;
    
    if ([self.category.category_id isEqualToString:CATEGORY_HOTEL_ID]) {
        tableHeader = [[[NSBundle mainBundle] loadNibNamed:@"CategoryDetailTableHeaderViewPrice" owner:self options:nil] firstObject];
    }else{
        tableHeader = [[[NSBundle mainBundle] loadNibNamed:@"CategoryDetailTableHeaderView" owner:self options:nil] firstObject];
    }
    
    _tableView.tableHeaderView = tableHeader;
    
    [self registerNibsForTableView];
    
    _flag_filter = FLAG_UI_CATEGORYDETAIL_AZ;
    
    [self _loadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) registerNibsForTableView
{
    NSArray *cells = @[@"LocationCell"];
    
    for (NSString *cellClass in cells) {
        [_tableView registerNib:[UINib  nibWithNibName:cellClass
                                                bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:cellClass];
    }
}

#pragma mark - LOAD DATA

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (NSSortDescriptor *) appendShortDescriptior:(NSString *)key andAscending:(BOOL)ascending
{
    return [[NSSortDescriptor alloc] initWithKey:key ascending:ascending];
}

- (void) _filterData:(NSArray *)filteredLocations
{
    NSMutableArray *updateLocations = [[NSMutableArray alloc] init];
    
    for (ILocation *iLocation in filteredLocations) {
        [updateLocations addObject:iLocation.location_id];
    }
    
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Location"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"location_id IN %@", updateLocations];
    [fetchRequest setPredicate:predicate];
    
    [self _applyFilter:fetchRequest];
    
    [self _loadLocationsWithFetchRequest:fetchRequest inManagedContext:managedObjectContext];
    
}

- (void) _reloadData
{
    NSLog(@"CategoryDetailVC _reloadData");

    [[SBase dataSource] loadWSCategoryDetailsByCategoryId:self.category.category_id];
    
    ICategory *currentCategory = [[SBase dataSource] loadCategoryById:self.category.category_id];
    self.category = currentCategory;
    
    [self _loadData];
}

- (void) _applyFilter:(NSFetchRequest *)fetchRequest
{
    switch (_flag_filter) {
        case FLAG_UI_CATEGORYDETAIL_AZ:
            [fetchRequest setSortDescriptors:@[[self appendShortDescriptior:@"title" andAscending:YES]]];
            break;
        case FLAG_UI_CATEGORYDETAIL_NEAR:
        {
            NSSortDescriptor *number     = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES selector:@selector(localizedStandardCompare:)];
            [fetchRequest setSortDescriptors:@[number]];
        }
            break;
        case FLAG_UI_CATEGORYDETAIL_POPULAR:
            [fetchRequest setSortDescriptors:@[[self appendShortDescriptior:@"popular" andAscending:YES]]];
            break;
        
        case FLAG_UI_CATEGORYDETAIL_PRICE:
            [fetchRequest setSortDescriptors:@[[self appendShortDescriptior:@"price" andAscending:YES]]];
            break;
        default:
            break;
    }
}

- (void) _loadData
{
    if ([self _isFiltering]) {
        [self _filterData:_filteredLocations];
        return;
    }
    
    NSLog(@"CategoryDetailVC _loadData");
    
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Location"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"category_id = %@", self.category.category_id];
    [fetchRequest setPredicate:predicate];
    
    [self _applyFilter:fetchRequest];
    
    NSArray *categoryDetails = [[SBase dataSource] loadCategoryDetailsByCategoryId:self.category.category_id];
    NSLog(@"CategoryDetailVC _loadData categoryDetails %@",categoryDetails);
    if ([categoryDetails count] >0) {
        _categoryDetails = categoryDetails;
    }
    
    [self _loadLocationsWithFetchRequest:fetchRequest inManagedContext:managedObjectContext];
    
}

- (void) _loadLocationsWithFetchRequest:(NSFetchRequest *)fetchRequest inManagedContext: (NSManagedObjectContext *)managedObjectContext
{
    _locations = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    if ([_locations count] > 0) {
        _images = [[NSMutableDictionary alloc] init];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            int count = 0;
            for (Location *location in _locations) {
                
                if (![location valueForKey:@"thumbnail"] || [[location valueForKey:@"thumbnail"] isEqualToString:@""]) {
                    
                }else{
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        NSString *imageURL = [NSString stringWithFormat:@"%@/%@", COMMON_WS_URL, [location valueForKey:@"thumbnail"]];
                        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageURL]];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            if (imageData) {
                                [_images setObject:[UIImage imageWithData:imageData] forKey:[NSString stringWithFormat:@"%i", count]];
                                
                                LocationCell *locationCell =(LocationCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:count inSection:0]];
                                locationCell.cellIcon.image = [UIImage imageWithData:imageData];
                            }
                            
                            
                        });
                    });
                }
                count ++;
            }
        });
    }
    [_tableView reloadData];
}

#pragma mark - PRIVATE STATUS

- (BOOL) _isFiltering
{
    if (_checkinDateVal && _checkoutDateVal && _quantityVal) {
        return true;
    }
    return false;
}

#pragma mark - INHERITANCE FUNCTION

- (void) showMap:(id) sender
{
    
    MapVC *mapVC = [[MapVC alloc] init];
    
    if ([self _isFiltering]) {
        mapVC.type = MAP_TYPE_CATEGORY_FILTER;
        mapVC.filtered_locations = _locations;
    }else{
        mapVC.type = MAP_TYPE_CATEGORY_DETAIL;
        mapVC.map_category_id = self.category.category_id;
    }
    
    
    
    [self.navigationController pushViewController:mapVC animated:YES];
    NSLog(@"CategoryDetailVC showMap");
}

#pragma mark - TABLEVIEW DATASOURCE

/*
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return [content valueForKey:@"headerTitle"];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return [indices indexOfObject:title];
}
*/

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 105;
}



-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSLog(@"CategoryDetailVC viewForHeaderInSection %@", self.category.category_id);
    
    UIView *myHeader;
    if ([self.category.category_id isEqualToString:CATEGORY_HOTEL_ID]) {
        myHeader = [[[NSBundle mainBundle] loadNibNamed:@"CategoryDetailFilter"
                                                  owner:self
                                                options:nil] firstObject];
        
        if (_checkinDateVal) {
            _checkinDate.text = _checkinDateVal;
        }
        
        if (_checkoutDateVal) {
            _checkoutDate.text = _checkoutDateVal;
        }
        
        if (_quantityVal) {
            _stepperValue.text = _quantityVal;
            _quantityStepper.value = [_quantityVal doubleValue];
        }else{
            _stepperValue.text = @"2";
            _quantityVal = @"2";
            _quantityStepper.value = [@"2" doubleValue];
        }
        
        _scrollviewFilterPrice.contentSize = CGSizeMake(370, 44);
        
    }else{
        myHeader = [[[NSBundle mainBundle] loadNibNamed:@"CategoryDetailSectionHeaderView"
                                                          owner:self
                                                        options:nil] firstObject];
        [_lb_overview setText:self.category.overview];
    }
    
    
    return myHeader;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_locations count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 72;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *CellId = @"LocationCell";
    LocationCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (cell == nil) {
        cell = [[LocationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellId];
    }
    
    cell.cellTitle.text = [_locations[indexPath.row] valueForKey:@"title"];
    
    if (_flag_filter == FLAG_UI_CATEGORYDETAIL_NEAR) {
        NSString *subtitle = [NSString stringWithFormat:@"%@ (%@ m)",
                              [_locations[indexPath.row] valueForKey:@"address"],
                              [_locations[indexPath.row] valueForKey:@"distance"]];
        cell.cellSubTitle.text = subtitle;
    }else if(_flag_filter == FLAG_UI_CATEGORYDETAIL_PRICE){
        int price = (int)[[_locations[indexPath.row] valueForKey:@"price"] integerValue];
        NSString *subtitle;
        if (price == 1) {
            subtitle = [NSString stringWithFormat:@"%@ ($)",
                                  [_locations[indexPath.row] valueForKey:@"address"]];
        }else if (price == 2){
            subtitle = [NSString stringWithFormat:@"%@ ($$)",
                        [_locations[indexPath.row] valueForKey:@"address"]];
        }else{
            subtitle = [NSString stringWithFormat:@"%@ ($$$)",
                        [_locations[indexPath.row] valueForKey:@"address"]];
        }
        cell.cellSubTitle.text = subtitle;
    }else{
        cell.cellSubTitle.text = [_locations[indexPath.row] valueForKey:@"address"];
    }
    
    if ([_images objectForKey:[NSString stringWithFormat:@"%i", indexPath.row]]) {
        UIImage *image = [_images objectForKey:[NSString stringWithFormat:@"%i", indexPath.row]];
        cell.cellIcon.image = image;
    }else{
        cell.cellIcon.image = [UIImage imageNamed:@"noimage"];
    }
    
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LocationVC *locationVC = [[LocationVC alloc] init];
    locationVC.location_id = [_locations[indexPath.row] valueForKey:@"location_id"];
    
    if ([self _isFiltering] && isFiltered) {
        NSArray *keys = @[@"checkin", @"checkout", @"quantity"];
        NSArray *objects = @[_checkinDateVal, _checkoutDateVal, _quantityVal];
        
        NSDictionary *booking_info = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
        locationVC.booking_info = booking_info;
    }
    
    [self.navigationController pushViewController:locationVC animated:YES];
}

#pragma mark - PULL CODE
- (void)setupStrings{
    textPull    = @"Pull down to refresh";             //@"Pull down to refresh...";
    textRelease = @"Release to refresh";                   //@"Release to refresh...";
    textLoading = @"Now loading";                             //@"Loading...";
}

- (void)addPullToRefreshHeader {
    
    [self setupStrings];
    
    refreshHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 - REFRESH_HEADER_HEIGHT, 320, REFRESH_HEADER_HEIGHT)];
    refreshHeaderView.backgroundColor = [UIColor clearColor];
    
    refreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(110, 0, 320, REFRESH_HEADER_HEIGHT)];
    refreshLabel.backgroundColor = [UIColor clearColor];
    refreshLabel.font = [UIFont boldSystemFontOfSize:13.0];
    refreshLabel.textColor = [UIColor colorWithRed:197.0/255.0 green:187.0/255.0 blue:184.0/255.0 alpha:1.0];
    
    refreshArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pull_arrow.png"]];
    refreshArrow.frame = CGRectMake(floorf((REFRESH_HEADER_HEIGHT - 13) / 2 + 70),
                                    (floorf(REFRESH_HEADER_HEIGHT - 26) / 2),
                                    13, 26);
    
    refreshSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    refreshSpinner.frame = CGRectMake(floorf(floorf(REFRESH_HEADER_HEIGHT - 20) / 2) + 70 , floorf((REFRESH_HEADER_HEIGHT - 20) / 2), 20, 20);
    refreshSpinner.hidesWhenStopped = YES;
    
    [refreshHeaderView addSubview:refreshLabel];
    [refreshHeaderView addSubview:refreshArrow];
    [refreshHeaderView addSubview:refreshSpinner];
    [_tableView addSubview:refreshHeaderView];
}

- (void)startLoading {
    isLoading = YES;
    
    // Show the header
    [UIView animateWithDuration:0.3 animations:^{
        _tableView.contentInset = UIEdgeInsetsMake(REFRESH_HEADER_HEIGHT, 0, 0, 0);
        refreshLabel.text = textLoading;
        refreshArrow.hidden = YES;
        [refreshSpinner startAnimating];
    } completion:^(BOOL finished) {
        
        if ([self _isFiltering]) {
            [self performCheckAvailability];
        }else{
            // call WS
            WSLocation *wsLocation = [[WSLocation alloc] init];
            wsLocation.delegate = (id)self;
            
            [wsLocation updateLocationsByCategory: self.category.category_id];
            
            WSCategory *wsCategory = [[WSCategory alloc] init];
            wsCategory.delegate = (id)self;
            
            [wsCategory getCategory:self.category.category_id];
        }
        
        

    }];
}

- (void)stopLoading {
    isLoading = NO;
    
    // Hide the header
    [UIView animateWithDuration:0.3 animations:^{
        _tableView.contentInset = UIEdgeInsetsZero;
        [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
    }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(stopLoadingComplete)];
                     }];
}

- (void)stopLoadingComplete {
    // Reset the header
    refreshLabel.text = textPull;
    refreshArrow.hidden = NO;
    [refreshSpinner stopAnimating];
}

- (void)refresh {
    // This is just a demo. Override this method with your custom reload action.
    // Don't forget to call stopLoading at the end.
    [self performSelector:@selector(stopLoading) withObject:nil afterDelay:2.0];
}



#pragma mark - TEXTVIEW DELEGATE

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.tag == 1) {
        _quantityVal = textField.text;
        [_quantityStepper setValue:[textField.text doubleValue]];
    }else if (textField.tag == 2){
        if (_checkinDateVal) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
            [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
            [dateFormatter setDateFormat:DATE_FORMAT];
            NSDate *date = [dateFormatter dateFromString:_checkinDateVal];
            NSDate *tomorrow = [NSDate dateWithTimeInterval:(24*60*60) sinceDate:date];
            
            _checkoutDateVal = [dateFormatter stringFromDate:tomorrow];
            _checkoutDate.text = _checkoutDateVal;
            
        }
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField.tag == 1) {
        return;
    }
    
    _selectedTextField = textField;
    
    NSLog(@"CategoryDetailVC textFieldDidBeginEditing");
    _pickerViewDate = [[UIActionSheet alloc] initWithTitle:@"How many?"
                                                 delegate:self
                                        cancelButtonTitle:nil
                                   destructiveButtonTitle:nil
                                        otherButtonTitles:nil];
    _theDatePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0.0, 44.0, 0.0, 0.0)];
    _theDatePicker.datePickerMode = UIDatePickerModeDate;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [dateFormatter setDateFormat:DATE_FORMAT];
    //[dateFormatter setDateFormat:@"MM/dd/YYYY"];
    
    [_theDatePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    
    if (textField.tag == 3) {
        NSString *checkin = _checkinDate.text;
        if ([checkin isEqualToString:@""]) {
            
        }else{
            
            //_theDatePicker.minimumDate = [FormatDate dateFromString:checkin];
            NSDate *date = [dateFormatter dateFromString:checkin];
            
            NSDate *tomorrow = [NSDate dateWithTimeInterval:(24*60*60) sinceDate:date];
            [_theDatePicker setDate:tomorrow];
            [_theDatePicker setMinimumDate:tomorrow];
            
        }
        
    }
    
    _pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    _pickerToolbar.barStyle= UIBarStyleDefault;
    [_pickerToolbar sizeToFit];
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(DatePickerDoneClick:)];
    
    
    [barItems addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
    [barItems addObject:flexSpace];
    
    [_pickerToolbar setItems:barItems animated:YES];
    [_pickerViewDate addSubview:_pickerToolbar];
    [_pickerViewDate addSubview:_theDatePicker];
    [_pickerViewDate  showInView:self.view];
    [_pickerViewDate setBounds:CGRectMake(0,0,320, 464)];

}

-(IBAction)DatePickerDoneClick:(id)sender{
    NSLog(@"CategoryDetailVC DatePickerDoneClick");
    
    NSDateFormatter *FormatDate = [[NSDateFormatter alloc] init];
    [FormatDate setLocale: [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [FormatDate setDateFormat:DATE_FORMAT];
    _selectedTextField.text = [FormatDate stringFromDate:[_theDatePicker date]];
    
    if (_selectedTextField.tag == 2) {
        _checkinDateVal = _selectedTextField.text;
    }
    
    if(_selectedTextField.tag == 3){
        _checkoutDateVal = _selectedTextField.text;
    }
    
    [self closeDatePicker:sender];
    //tableview.frame=CGRectMake(0, 44, 320, 416);
    
}

-(IBAction)dateChanged:(id)sender{
    NSLog(@"CategoryDetailVC dateChanged");
    NSDateFormatter *FormatDate = [[NSDateFormatter alloc] init];
    [FormatDate setLocale: [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [FormatDate setDateFormat:DATE_FORMAT];
    _selectedTextField.text = [FormatDate stringFromDate:[_theDatePicker date]];
}

-(BOOL)closeDatePicker:(id)sender{
    [_pickerViewDate dismissWithClickedButtonIndex:0 animated:YES];
    [_selectedTextField resignFirstResponder];
    
    return YES;
}

#pragma mark - STEPPER ACTIONS

- (IBAction)stepperValueChanged:(id)sender
{
    NSLog(@"CategoryDetailVC stepper ValueChaged");
    //double stepperValue = ourStepper.value;
    _stepperValue.text = [NSString stringWithFormat:@"%.f",[(UIStepper *)sender value] ];
    _quantityVal = _stepperValue.text;
}

#pragma mark - CHECK ACTION

- (IBAction)checkAvailability:(id)sender{
    NSLog(@"CategoryDetail checkAvailability");
    
    [self performCheckAvailability];
}

- (void) performCheckAvailability{
    NSString *checkin   = _checkinDate.text;
    NSString *checkout  = _checkoutDate.text;
    NSString *value     = _stepperValue.text;
    
    if ([checkin isEqualToString:@""] || [checkout isEqualToString:@""] || [value isEqualToString:@"0"] || [value isEqualToString:@""]) {
        [SBase showAlert:@"Please fill in checkin, check out, number of people"];
        
        if (_isLoading) {
            [self stopLoading];
        }
        
    }else{
        NSLog(@"Performing check Availability: %@ %@ %@", checkin, checkout, value);
        
        WSLocation *wsLocation = [[WSLocation alloc] init];
        wsLocation.delegate = (id)self;
        
        [wsLocation filterLocations:checkin andCheckout:checkout andQuantity:value];
    }

}


#pragma mark - UISCROLLVIEW DELEGATE

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (scrollView == _tableView) {
        if (isLoading) return;
        isDragging = YES;
    }
}

#define DASHBOARD_SUMMARY_PANEL_HEADER_HEIGHT 97

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == _tableView) {
        CGPoint p = scrollView.contentOffset;
        
        CGFloat height = (float) DASHBOARD_SUMMARY_PANEL_HEADER_HEIGHT;
        
        if (p.y <= height && p.y >= 0) {
            _tableView.contentInset = UIEdgeInsetsMake(-p.y, 0, 0, 0);
        } else if (p.y >= height) {
            _tableView.contentInset = UIEdgeInsetsMake(-height, 0, 0, 0);
        }
        
        if (isLoading) {
            // Update the content inset, good for section headers
            if (scrollView.contentOffset.y > 0)
                _tableView.contentInset = UIEdgeInsetsZero;
            else if (scrollView.contentOffset.y >= -REFRESH_HEADER_HEIGHT)
                _tableView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        } else if (isDragging && scrollView.contentOffset.y < 0) {
            // Update the arrow direction and label
            [UIView animateWithDuration:0.25 animations:^{
                if (scrollView.contentOffset.y < -REFRESH_HEADER_HEIGHT) {
                    // User is scrolling above the header
                    refreshLabel.text = textRelease;
                    [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
                } else {
                    // User is scrolling somewhere within the header
                    refreshLabel.text = textPull;
                    [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
                }
            }];
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView == _tableView) {
        if (!decelerate) {
            if (scrollView.contentOffset.y <= scrollView.contentSize.height - 1.0 * scrollView.frame.size.height)// &&
                //[_feeds count] < _total &&
                //!_isLoading)
            {
                //[self loadMoreFeeds];
            }
        }
        
        if (isLoading) return;
        isDragging = NO;
        if (scrollView.contentOffset.y <= -REFRESH_HEADER_HEIGHT) {
            // Released above the header
            [self startLoading];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == _tableView) {
        if (scrollView.contentOffset.y <= scrollView.contentSize.height - 1.0 * scrollView.frame.size.height) //&&
            //[_feeds count] < _total  &&
            //!_isLoading)
        {
            //[self loadMoreFeeds];
        }
    }
}

#pragma mark - WSLOCATION DELEGATE

- (void) wsLocation:(WSLocation *)wsLocation didUpdatedByCategorySuccess:(NSArray *)locations
{
    [self _reloadData];
    [self stopLoading];
}

- (void) wsLocation:(WSLocation *)wsLocation didUpdatedByCategoryFail:(NSError *)error
{
    [self stopLoading];
    
}

- (void) wsLocation:(WSLocation *)wsLocation didFilterSuccess:(NSArray *)locations{
    NSLog(@"CategoryDetailVC didFilterSuccess %@", locations);
    
    _filteredLocations = locations;
    
    [self _filterData:locations];
    [self stopLoading];
    
    isFiltered = YES;
    
}

- (void) wsLocation:(WSLocation *)wsLocation didFilterFail:(NSError *)error{
    NSLog(@"CategoryDetailVC didFilterFail");
    [self stopLoading];

}

#pragma mark - WSCATEGORY DELEGATE

- (void) wsCategory:(WSCategory *)wsCategory didGetCategorySuccess:(NSDictionary *)category
{
    NSLog(@"CategoryDetailVC wsCategory didGetCategorySuccess");
}

- (void) wsCategory:(WSCategory *)wsCategory didGetCategoryFail:(NSError *)error
{
    
}

#pragma mark - IBACTIONS

- (void) _resetFilterButton
{
    _bt_filter_nearme.titleLabel.font = [UIFont systemFontOfSize:15];
    _bt_filter_popular.titleLabel.font = [UIFont systemFontOfSize:15];
    _bt_filterAZ.titleLabel.font = [UIFont systemFontOfSize:15];
    _bt_filter_star.titleLabel.font = [UIFont systemFontOfSize:15];
    _bt_filter_price.titleLabel.font = [UIFont systemFontOfSize:15];
}

- (IBAction)_filterAZ:(id)sender {
    NSLog(@"CategoryDetailVC _filterAZ");
    
    [self _resetFilterButton];
    
    _bt_filterAZ.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    _flag_filter = FLAG_UI_CATEGORYDETAIL_AZ;
    [self _loadData];
}

- (IBAction)_filterNearme:(id)sender {
    NSLog(@"CategoryDetailVC _filterNearme");
    
    [self _resetFilterButton];
    _bt_filter_nearme.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    
    _flag_filter = FLAG_UI_CATEGORYDETAIL_NEAR;
    [self _loadData];
    
}

- (IBAction)_filterPopular:(id)sender {
    
    [self _resetFilterButton];
    _bt_filter_popular.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    
    
    NSLog(@"CategoryDetailVC _filterPopular");
    _flag_filter = FLAG_UI_CATEGORYDETAIL_POPULAR;
    [self _loadData];
}

- (IBAction)_filterPrice:(id)sender{
    [self _resetFilterButton];
    _bt_filter_price.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    
    NSLog(@"CategoryDetailVC _filterPopular");
    _flag_filter = FLAG_UI_CATEGORYDETAIL_PRICE;
    [self _loadData];
}

- (IBAction)_filterStar:(id)sender{
    [self _resetFilterButton];
    _bt_filter_star.titleLabel.font = [UIFont boldSystemFontOfSize:16];
}



- (IBAction)_continueReading:(id)sender {
    NSLog(@"CategoryDetailVC _continueReading");
    
    CategoryOverviewVC *overview = [[CategoryOverviewVC alloc] init];
    
    overview.category = self.category;
    overview.categoryDetails = _categoryDetails;
    
    [self.navigationController pushViewController:overview animated:YES];
    
}
@end
