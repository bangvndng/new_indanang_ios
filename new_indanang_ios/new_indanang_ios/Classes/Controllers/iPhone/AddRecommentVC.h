//
//  AddRecommentVC.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/22/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddRecommentVCDelegate;

@interface AddRecommentVC : UIViewController

@property (weak, nonatomic) id<AddRecommentVCDelegate> delegate;
@property (strong, nonatomic) NSString *location_id;

@end

@protocol AddRecommentVCDelegate <NSObject>

- (void) addRecommend:(AddRecommentVC *)addRecommend didAddRecommendSuccess:(NSDictionary *)recommendInfo;

@end
