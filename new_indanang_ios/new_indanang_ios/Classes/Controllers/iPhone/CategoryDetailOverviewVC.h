//
//  CategoryDetailOverviewVC.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/29/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryDetailOverviewVC : UIViewController

@property (strong, nonatomic) NSString *overview;

@property (weak, nonatomic) IBOutlet UILabel *lbContent;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollviewContent;


@end
