//
//  HomeVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/12/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "HomeVC.h"
#import "CategoryListVC.h"

#import "SBase.h"

#import "HomeImagesCell.h"
#import "HomeThingsCell.h"
#import "HomeAccessoriesCell.h"

#import "CategoryListVC.h"

#import "ICategory.h"

#import "WSForecast.h"
#import "LocationManager.h"

#define HOME_TABLE_SECTION_IMAGE        0
#define HOME_TABLE_SECTION_THINGS       1
#define HOME_TABLE_SECTION_ACCESSORIES  2

#define HOME_TABLE_THINGS_OFFSET        1

#define THINGS_TITLE_INDEX              0
#define THINGS_CLASS_INDEX              1
#define THINGS_ICON_INDEX               2
#define THINGS_SUBTITLE_INDEX           3
#define THINGS_ID_INDEX                 4

@interface HomeVC ()<HomeAccessoriesCellDelegate, SDataDelegate, WSForecastDelegate>{
    
    __weak IBOutlet UITableView *_tableView;
    
    //PULL TO REFRESH
    BOOL                                    _isFirstTouch, _isLoading;
    
    UIView                                  *refreshHeaderView;
    UILabel                                 *refreshLabel;
    UIImageView                             *refreshArrow;
    UIActivityIndicatorView                 *refreshSpinner;
    BOOL                                    isDragging;
    BOOL                                    isLoading;
    NSString                                *textPull;
    NSString                                *textRelease;
    NSString                                *textLoading;
    // ------------- //
    
    NSArray     *_thingItems;
    
    NSString *_suggeationsCount;
    
    NSDictionary *_forecastData;
    
    HomeAccessoriesCell *_homeAccessoriesCell;
}

@end

@implementation HomeVC

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupStrings];
    [self addPullToRefreshHeader];
    [self registerNibsForTableView];
    [self loadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)loadData{
    _suggeationsCount = [NSString stringWithFormat:@"%i", [[[SBase dataSource] loadLocationsBySuggestion] count]];
    
    _thingItems =
    @[
      @[
          @"Suggestions",
          @"SuggestionsListVC",
          @"ic_home_globe.png",
          [NSString stringWithFormat:@"There are %@ suggesions for you", _suggeationsCount]
          ],
      @[
          @"Things to do",
          @"CategoryListVC",
          @"ic_home_done.png",
          @"Museum, Restaurants, Bars",
          @"1"
          ],
      @[
          @"Hotels",
          @"CategoryDetailVC",
          @"ic_home_bed.png",
          @"Easy find and book hotel",
          @"7"
          ],
      @[
          @"Tours",
          @"CategoryDetailVC",
          @"ic_home_tour.png",
          @"Go see the sights",
          @"8"
          ],
      @[
          @"Timeline",
          @"TimeLineVC",
          @"ic_home_globe.png",
          @"People sharing"
          ],
      ];
    
    
    [self getForecast];
    
}

- (void) getForecast
{
    
    
    if ([LocationManager sharedSingleton].locationManager.location) {
        CLLocation *location = [LocationManager sharedSingleton].locationManager.location;
        
        NSArray *keys = @[@"latitude", @"longitude"];
        NSArray *objects = @[[NSString stringWithFormat:@"%f", location.coordinate.latitude],
                             [NSString stringWithFormat:@"%f", location.coordinate.longitude]];
        
        NSDictionary *param = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
        
        WSForecast *wsForecast = [[WSForecast alloc] init];
        wsForecast.delegate = (id)self;
        [wsForecast getForecast:param];
    }else{
        //[SBase showAlert:@"Please enable Location service to retreive data"];
    }
    
}

- (void) wsForecast:(WSForecast *)wsForecast didGetForecaseSuccess:(NSDictionary *)forecastData
{
    if (_forecastData == nil) {
        _forecastData = [forecastData mutableCopy];
    }
}

- (void) wsForecast:(WSForecast *)wsForecast didGetForecaseFail:(NSError *)error
{
    NSLog(@"HomeVC wsForecast didGetForecaseFail");
}

#pragma mark - SETUP VIEW

- (void) registerNibsForTableView
{
    NSArray *cells = @[@"HomeImagesCell", @"HomeThingsCell", @"HomeAccessoriesCell"];
    
    for (NSString *cellClass in cells) {
        [_tableView registerNib:[UINib  nibWithNibName:cellClass
                                                bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:cellClass];
    }
}

#pragma mark - TABLEVIEW DATASOURCE

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }else if(section == 1){
        return 5;
    }else{
        return 1;
    }
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    
    switch (section) {
        case HOME_TABLE_SECTION_IMAGE:
            return 205;
            break;
        case HOME_TABLE_SECTION_THINGS:
            return 70;
            break;
        case HOME_TABLE_SECTION_ACCESSORIES:
            return 170;
            break;
        default:
            return 150;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    
    switch (section) {
        case HOME_TABLE_SECTION_IMAGE:{
            return [self setHomeImagesCell:tableView];
        }
            
            break;
        case HOME_TABLE_SECTION_THINGS:{
            return [self setHomeThingsCell:tableView andIndexPath:indexPath];
        }
            
            break;
        case HOME_TABLE_SECTION_ACCESSORIES:{
            return [self setHomeAccessoriesCell:tableView andIndexPath:indexPath];
        }
            
            break;
        default:{
            NSString *CellId = @"DefaultCell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
            
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellId];
            }
            
            cell.textLabel.text = @"Title";
            [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
            
            return cell;
        }
            break;
    }
    
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSInteger section = indexPath.section;
    
    if (section == HOME_TABLE_SECTION_THINGS) {
        
        NSString *SubCenterClassName = [_thingItems[indexPath.row] objectAtIndex:THINGS_CLASS_INDEX];
        
        Class SubCenterClass = NSClassFromString(SubCenterClassName);
        
        SubCenterAbstract *SubCenter = [[SubCenterClass alloc] init];
        
        if ([SubCenterClassName isEqualToString:@"CategoryListVC"]) {
            SubCenter.category = [[SBase dataSource] loadCategoryById:[_thingItems[indexPath.row] objectAtIndex:THINGS_ID_INDEX]];

        }else if([SubCenterClassName isEqualToString:@"CategoryDetailVC"]){
            SubCenter.category = [[SBase dataSource] loadCategoryById:[_thingItems[indexPath.row] objectAtIndex:THINGS_ID_INDEX]];
        }
        
        [[[SBase appDelegate].mainVC centerVC] pushViewController:SubCenter animated:YES];
    }

}

#pragma mark - Pull CODE
- (void)setupStrings{
    textPull    = @"Pull down to refresh";             //@"Pull down to refresh...";
    textRelease = @"Release to refresh";                   //@"Release to refresh...";
    textLoading = @"Now loading";                             //@"Loading...";
}

- (void)addPullToRefreshHeader {
    refreshHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 - REFRESH_HEADER_HEIGHT, 320, REFRESH_HEADER_HEIGHT)];
    refreshHeaderView.backgroundColor = [UIColor clearColor];
    
    refreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(110, 0, 320, REFRESH_HEADER_HEIGHT)];
    refreshLabel.backgroundColor = [UIColor clearColor];
    refreshLabel.font = [UIFont boldSystemFontOfSize:13.0];
    refreshLabel.textColor = [UIColor colorWithRed:197.0/255.0 green:187.0/255.0 blue:184.0/255.0 alpha:1.0];
    
    refreshArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pull_arrow.png"]];
    refreshArrow.frame = CGRectMake(floorf((REFRESH_HEADER_HEIGHT - 13) / 2 + 70),
                                    (floorf(REFRESH_HEADER_HEIGHT - 26) / 2),
                                    13, 26);
    
    refreshSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    refreshSpinner.frame = CGRectMake(floorf(floorf(REFRESH_HEADER_HEIGHT - 20) / 2) + 70 , floorf((REFRESH_HEADER_HEIGHT - 20) / 2), 20, 20);
    refreshSpinner.hidesWhenStopped = YES;
    
    [refreshHeaderView addSubview:refreshLabel];
    [refreshHeaderView addSubview:refreshArrow];
    [refreshHeaderView addSubview:refreshSpinner];
    [_tableView addSubview:refreshHeaderView];
}

- (void)startLoading {
    isLoading = YES;
    
    // Show the header
    [UIView animateWithDuration:0.3 animations:^{
        _tableView.contentInset = UIEdgeInsetsMake(REFRESH_HEADER_HEIGHT, 0, 0, 0);
        refreshLabel.text = textLoading;
        refreshArrow.hidden = YES;
        [refreshSpinner startAnimating];
    } completion:^(BOOL finished) {
        // Refresh Locations here
        [SBase dataSource].delegate = (id) self;
        [[SBase dataSource] loadAllLocations:YES];
    }];
}

- (void)sData:(SData *)sData didLoadAllLocationsSuccess:(NSArray *)locations
{
    [self loadData];
    [_tableView reloadData];
    
    [self stopLoading];
}

- (void)sData:(SData *)sData didLoadAllLocationsFail:(NSError *)error
{
    [self stopLoading];
}

- (void)stopLoading {
    isLoading = NO;
    
    // Hide the header
    [UIView animateWithDuration:0.3 animations:^{
        _tableView.contentInset = UIEdgeInsetsZero;
        [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
    }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(stopLoadingComplete)];
                     }];
}

- (void)stopLoadingComplete {
    // Reset the header
    refreshLabel.text = textPull;
    refreshArrow.hidden = NO;
    [refreshSpinner stopAnimating];
}

- (void)refresh {
    // This is just a demo. Override this method with your custom reload action.
    // Don't forget to call stopLoading at the end.
    [self performSelector:@selector(stopLoading) withObject:nil afterDelay:2.0];
}



#pragma mark - UIScrollDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (scrollView == _tableView) {
        if (isLoading) return;
        isDragging = YES;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == _tableView) {
        if (isLoading) {
            // Update the content inset, good for section headers
            if (scrollView.contentOffset.y > 0)
                _tableView.contentInset = UIEdgeInsetsZero;
            else if (scrollView.contentOffset.y >= -REFRESH_HEADER_HEIGHT)
                _tableView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        } else if (isDragging && scrollView.contentOffset.y < 0) {
            // Update the arrow direction and label
            [UIView animateWithDuration:0.25 animations:^{
                if (scrollView.contentOffset.y < -REFRESH_HEADER_HEIGHT) {
                    // User is scrolling above the header
                    refreshLabel.text = textRelease;
                    [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
                } else {
                    // User is scrolling somewhere within the header
                    refreshLabel.text = textPull;
                    [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
                }
            }];
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView == _tableView) {
        if (!decelerate) {
            if (scrollView.contentOffset.y <= scrollView.contentSize.height - 1.0 * scrollView.frame.size.height)// &&
                //[_feeds count] < _total &&
                //!_isLoading)
            {
                //[self loadMoreFeeds];
            }
        }
        
        if (isLoading) return;
        isDragging = NO;
        if (scrollView.contentOffset.y <= -REFRESH_HEADER_HEIGHT) {
            // Released above the header
            [self startLoading];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == _tableView) {
        if (scrollView.contentOffset.y <= scrollView.contentSize.height - 1.0 * scrollView.frame.size.height) //&&
            //[_feeds count] < _total  &&
            //!_isLoading)
        {
            //[self loadMoreFeeds];
        }
    }
}

#pragma mark - CELLS SETUP

- (HomeImagesCell *) setHomeImagesCell: (UITableView *)tableView
{
    NSString *CellId = @"HomeImagesCell";
    HomeImagesCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (cell == nil) {
        cell = [[HomeImagesCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellId];
    }
    
    NSArray *homeImages = @[
                            @"large_sample_1.jpg",
                            @"large_sample_2.jpg",
                            @"large_sample_3.jpg",
                            @"large_sample_4.jpg",
                            ];
    int count = 0;
    for (NSString *homeImage in homeImages) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(320 * count, 0.0, 320.0, 205.0)];
        imageView.image = [UIImage imageNamed:homeImage];
        
        //imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        [cell.scrollview addSubview:imageView];
        count++;
    }
    
    cell.scrollview.contentSize = CGSizeMake(320.0 * [homeImages count], 205.0);
    
    return cell;
}

- (HomeThingsCell *) setHomeThingsCell: (UITableView *)tableView andIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellId = @"HomeThingsCell";
    HomeThingsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (cell == nil) {
        cell = [[HomeThingsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellId];
    }
    
    cell.cellIcon.image = [UIImage imageNamed:[_thingItems[indexPath.row] objectAtIndex:THINGS_ICON_INDEX]];
    cell.cellTitle.text = [_thingItems[indexPath.row] objectAtIndex:THINGS_TITLE_INDEX];
    cell.cellSubTitle.text = [_thingItems[indexPath.row] objectAtIndex:THINGS_SUBTITLE_INDEX];
    
    if ([[_thingItems[indexPath.row] objectAtIndex:THINGS_CLASS_INDEX] isEqualToString:@"SuggestionsListVC"]) {
        //cell.backgroundColor = [UIColor blackColor];
        cell.backgroundColor = [UIColor colorWithRed:9.0f / 255.0f
                                               green:35.0f / 255.0f
                                                blue:48.0f / 255.0f
                                               alpha:1.0f];
        
        cell.cellTitle.textColor = [UIColor whiteColor];
        
        cell.cellIcon.image = nil;
        
        //cell.cellIcon.backgroundColor = [UIColor blueColor];
        cell.cellIcon.backgroundColor = [UIColor colorWithRed:77.0f / 255.0f
                                                        green:168.0f / 255.0f
                                                         blue:208.0f / 255.0f
                                                        alpha:1.0f];
        
        cell.cellNumIcon.text = _suggeationsCount;
        cell.cellNumIcon.textColor = [UIColor whiteColor];
        cell.cellNumIcon.alpha = 1;
        
        [cell.cellViewWrapper.layer setCornerRadius:15.0f];
        
        // border
        [cell.cellViewWrapper.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [cell.cellViewWrapper.layer setBorderWidth:0.5f];
    }
    
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    return cell;

}

- (HomeAccessoriesCell *) setHomeAccessoriesCell: (UITableView *)tableView andIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellId = @"HomeAccessoriesCell";
    HomeAccessoriesCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (cell == nil) {
        cell = [[HomeAccessoriesCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellId];
    }

    _homeAccessoriesCell = cell;
    _homeAccessoriesCell.delegate = (id)self;
    
    NSDate *now = [NSDate date];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"hh:mm";
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    _homeAccessoriesCell.cellLabelTime.text = [dateFormatter stringFromDate:now];
    if (_forecastData == nil) {
        
    }else{
        if ([[_forecastData objectForKey:@"daily"] objectForKey:@"summary"]) {
            _homeAccessoriesCell.cellWeatherForecast.text = [[_forecastData objectForKey:@"daily"] objectForKey:@"summary"];
        }else{
            _homeAccessoriesCell.cellWeatherForecast.text = @"inDanang couldn't retrieve forecast information at this time, try to enable Location service and wifi and try again";
        }
    }
    
    
    
    return _homeAccessoriesCell;
}

#pragma mark - CELL DELEGATES

- (void) homeAccessoriesCell:(HomeAccessoriesCell *)homeAccessoriesCell openBackground:(NSDictionary *)info
{
    CategoryListVC *listVC = [[CategoryListVC alloc] init];
    listVC.category = [[ICategory alloc] init];
    listVC.category.category_id = @"2";
    [[[SBase appDelegate].mainVC centerVC] pushViewController:listVC animated:YES];
}

- (void) homeAccessoriesCell:(HomeAccessoriesCell *)homeAccessoriesCell openPracticalities:(NSDictionary *)info
{
    CategoryListVC *listVC = [[CategoryListVC alloc] init];
    
    listVC.category = [[ICategory alloc] init];
    listVC.category.category_id = @"3";
    [[[SBase appDelegate].mainVC centerVC] pushViewController:listVC animated:YES];
}

@end
