//
//  DetailOverviewCell.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/22/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "DetailOverviewCell.h"

@implementation DetailOverviewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)cellContinueRead:(id)sender {
    NSLog(@"DetailOverviewCell Continue Read");
    if (self.delegate && [self.delegate respondsToSelector:@selector(detailOverviewCell:continueReading:)]) {
        [self.delegate detailOverviewCell:self continueReading:nil];
    }
}
@end
