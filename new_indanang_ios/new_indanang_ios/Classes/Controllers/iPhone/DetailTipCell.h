//
//  DetailTipCell.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/22/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DetailTipCellDelegate;

@interface DetailTipCell : UITableViewCell

@property (weak, nonatomic) id<DetailTipCellDelegate> delegate;

- (IBAction)addTip:(id)sender;
- (IBAction)addPhoto:(id)sender;


@end

@protocol DetailTipCellDelegate <NSObject>

- (void) detailTipCell:(DetailTipCell *)detailTipCell addTip:(NSDictionary *)info;
- (void) detailTipCell:(DetailTipCell *)detailTipCell addPhoto:(NSDictionary *)info;

@end