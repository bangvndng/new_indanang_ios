//
//  HomeImagesCell.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/20/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeImagesCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIScrollView *scrollview;


@end
