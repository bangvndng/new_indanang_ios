//
//  AccountLoginCell.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/29/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AccountLoginCellDelegate;

@interface AccountLoginCell : UITableViewCell

@property (weak, nonatomic) id<AccountLoginCellDelegate> delegate;

- (IBAction)doRegister:(id)sender;
- (IBAction)doLogin:(id)sender;
- (IBAction)doLoginWithFacebook:(id)sender;

@end

@protocol AccountLoginCellDelegate <NSObject>

- (void)loginCell:(AccountLoginCell *)loginCell doRegister:(NSString *)info;
- (void)loginCell:(AccountLoginCell *)loginCell doLogin:(NSString *)info;
- (void)loginCell:(AccountLoginCell *)loginCell doLoginFaceBook:(NSString *)info;

@end