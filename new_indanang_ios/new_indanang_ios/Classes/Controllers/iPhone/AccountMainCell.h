//
//  AccountMainCell.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/29/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountMainCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *cellImage;
@property (weak, nonatomic) IBOutlet UIView *cellViewImage;
@property (weak, nonatomic) IBOutlet UILabel *cellUsername;
@property (weak, nonatomic) IBOutlet UILabel *cellEmail;



@end
