//
//  RegisterVC.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/17/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RegisterVCDelegate <NSObject>

@optional

- (void) didCancelRegister;
- (void) didRegisterSuccess;
- (void) didRegisterFail;

@end

@interface RegisterVC : UIViewController

@property (weak, nonatomic) id<RegisterVCDelegate> delegate;

@end
