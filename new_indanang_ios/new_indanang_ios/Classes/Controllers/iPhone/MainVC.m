//
//  MainVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/12/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "MainVC.h"
#import "HomeVC.h"
#import "Define.h"
#import "RegisterVC.h"
#import "SBase.h"
#define CORNER_RADIUS   4
#define SLIDE_TIMING    .25
#define PANEL_WIDTH     58

@interface MainVC ()<SubCenterAbstractDelegate, RegisterVCDelegate>

@end

@implementation MainVC

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self _setupView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - SETUP VIEW

- (void)_setupView
{
    if (self.centerVC == nil) {
        HomeVC *homeVC = [[HomeVC alloc] init];
        homeVC.delegate = (id)self;
        
        self.centerVC = [[CenterVC alloc] initWithRootViewController:homeVC];
        
        [self addChildViewController:self.centerVC];
        [self.view addSubview:self.centerVC.view];
        [self.centerVC didMoveToParentViewController:self];
    }

}

#pragma mark - REGISTER DELEGATE
- (void) didCancelRegister
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
    
}

- (void) didRegisterSuccess
{
    [self dismissViewControllerAnimated:YES completion:^{
        NSString *message = [NSString stringWithFormat:@"Hello %@, welcome you to inDanang.", [SAccount currentUser].displayname];
        [SBase showAlert:message];
    }];
}

- (void) didRegisterFail
{
    [self dismissViewControllerAnimated:YES completion:^{
        [SBase showAlert:@"Thank you for trying creating account with inDanang. Please try to create an account later. "];
    }];
}

#pragma mark - SubCenterAbstract DELEGATE

- (void)callShowMap
{
    
}

- (void)callShowMenu
{
    [self _showRightView];
}

- (void)callHideMenu
{
    [self _hideRightView];
}

#pragma mark - RIGHT - CENTER VIEW SET UP

- (void)_hideRightView
{
    [UIView animateWithDuration:SLIDE_TIMING delay:0 options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.centerVC.view.frame = CGRectMake(0,
                                                               0,
                                                               self.view.frame.size.width,
                                                               self.view.frame.size.height);
                     }
                     completion:^(BOOL finished) {
                         if (finished) {
                             self.centerVC.menu_state = CENTERVC_MENUSTATE_CLOSE;
                             [self _resetCenterView];
                         }
                     }];
}

- (void)_showRightView
{
    [self.view sendSubviewToBack:[self _getRightView]];
    [UIView animateWithDuration:SLIDE_TIMING delay:0 options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.centerVC.view.frame = CGRectMake(0 - self.view.frame.size.width + PANEL_WIDTH,
                                                               0,
                                                               self.view.frame.size.width,
                                                               self.view.frame.size.height);
                     }
                     completion:^(BOOL finished) {
                         if (finished) {
                             self.centerVC.menu_state = CENTERVC_MENUSTATE_OPEN;
                         }
                     }];
}

- (UIView *) _getRightView
{
    // Get the right view, if not existed, init it and return
    if (self.rightVC == nil) {
        self.rightVC = [[RightVC alloc] init];
        
        [self.view addSubview:self.rightVC.view];
        [self addChildViewController:self.rightVC];
        
        [self.rightVC didMoveToParentViewController:self];
    }
    
    //self.isShowingRightView = YES;
    
    [self _showCenterViewWithShadow:YES withOffset:-2];
    
    return self.rightVC.view;
}

- (void)_showCenterViewWithShadow:(BOOL)hasShadow withOffset:(NSInteger)offset
{
    if (hasShadow)
    {
        [self.centerVC.view.layer setCornerRadius:CORNER_RADIUS];
        [self.centerVC.view.layer setShadowColor:[UIColor blackColor].CGColor];
        [self.centerVC.view.layer setShadowOpacity:0.8];
        [self.centerVC.view.layer setShadowOffset:CGSizeMake(offset, offset)];
    }else
    {
        [self.centerVC.view.layer setCornerRadius:0.0f];
        [self.centerVC.view.layer setShadowOffset:CGSizeMake(offset, offset)];
    }
}

- (void)_resetCenterView
{
    // remove left view and reset variables, if needed
    
    if (self.rightVC != nil)
    {
        [self.rightVC.view removeFromSuperview];
        self.rightVC.view = nil;
        self.rightVC = nil;
    }
    
    
    //self.centerVC.menu_state = 0;
    // remove view shadows
    [self _showCenterViewWithShadow:NO withOffset:0];
}

@end
