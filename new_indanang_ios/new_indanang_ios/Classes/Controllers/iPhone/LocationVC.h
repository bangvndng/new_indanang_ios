//
//  LocationVC.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/19/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SubCenterAbstract.h"
#import "ILocation.h"

@interface LocationVC : SubCenterAbstract

@property (strong, nonatomic) NSString *location_id;
@property (nonatomic) int flag_from;
@property (strong, nonatomic) NSDictionary *booking_info;

@end