//
//  TimeLinePhotoCell.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/22/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeLinePhotoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *cellTime;
@property (weak, nonatomic) IBOutlet UILabel *cellUsername;
@property (weak, nonatomic) IBOutlet UILabel *cellMessage;
@property (weak, nonatomic) IBOutlet UIImageView *cellMainImage;


@property (weak, nonatomic) IBOutlet UIView *cellViewAvatar;

@property (weak, nonatomic) IBOutlet UIImageView *cellAvatar;


@end
