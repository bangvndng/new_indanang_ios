//
//  SubCenterAbstract.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/12/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SearchBar.h"
#import "ICategory.h"

@protocol SubCenterAbstractDelegate <NSObject>

@optional

- (void)callShowMap;
- (void)callShowMenu;
- (void)callHideMenu;

@end

@interface SubCenterAbstract : UIViewController

@property (weak, nonatomic) id<SubCenterAbstractDelegate> delegate;

@property (strong, nonatomic) SearchBar *searchBar;
@property (nonatomic, strong) UISearchDisplayController *centerSearchDisplayController;
@property (nonatomic, strong) UIViewController *searchResultVC;

@property (nonatomic, strong) UIBarButtonItem *menuItem;
@property (nonatomic, strong) UIBarButtonItem *mapItem;
@property (nonatomic, strong) UIBarButtonItem *searchBarItem;

@property (strong, nonatomic) UIView *leftNavBarView;

@property (strong, nonatomic) ICategory * category;

- (void) expandSearchBar;
- (void) collapseSearchBar;

@end