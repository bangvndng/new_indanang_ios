//
//  AccountVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/12/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "AccountVC.h"
#import "LoginVC.h"
#import "RegisterVC.h"

#import "WSUser.h"

#import "SBase.h"
#import "AccountCell.h"
#import "AccountLoginCell.h"
#import "AccountMainCell.h"

#define SECTION_MAINCELL    0
#define SECTION_LOGIN       1
#define SECTION_ADDITIONAL  2

@interface AccountVC ()<
LoginVCDelegate,
RegisterVCDelegate,
SAccountDelegate,
WSUserDelegate,
UIAlertViewDelegate,
AccountLoginCellDelegate
>
{
    
    
    __weak IBOutlet UIView *_viewProfile;
    __weak IBOutlet UIView *_viewActions;
    
    __weak IBOutlet UILabel *_lb_username;
    __weak IBOutlet UILabel *_lb_email;
    __weak IBOutlet UILabel *_lb_displayname;
    
    __weak IBOutlet UIImageView *_image_avatar;
 
    
    __weak IBOutlet UITableView *_tableView;
    
    
    __weak IBOutlet UIButton *_bt_logout;
    
    __weak IBOutlet UIButton *_bt_login;
    __weak IBOutlet UIButton *_bt_register;
    __weak IBOutlet UIButton *_bt_login_fb;
    
    UIImage *_myavatar;
    
    AccountMainCell *_accountMainCell;
    
    NSArray *_info;
}

- (IBAction)footerLogout:(id)sender;

@end

@implementation AccountVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _info = @[
              @[
                  @"001",
                  @"Internet Access",
                  @"Always"
                  ],
              @[
                  @"001",
                  @"Distance Unit",
                  @"Km"
                  ],
              @[
                  @"001",
                  @"Temperature Unit",
                  @"Celcius"
                  ],
              @[
                  @"001",
                  @"Currency",
                  @"USD"
                  ],
              @[
                  @"001",
                  @"Travel Log Privacy",
                  @"Public"
                  ]
              ];
    
    [self registerNibsForTableView];
    
    if ([SAccount currentUser].isLoggedIn) {
        _tableView.tableFooterView = [[[NSBundle mainBundle] loadNibNamed:@"AccountVCLogoutView" owner:self options:nil] firstObject];
    }else{
        
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
    //[self _setupView];
    
}

- (void) registerNibsForTableView
{
    
    NSArray *cells = @[@"AccountCell", @"AccountMainCell", @"AccountLoginCell"];
    
    for (NSString *cellClass in cells) {
        [_tableView registerNib:[UINib  nibWithNibName:cellClass
                                                bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:cellClass];
    }
    
}

# pragma mark - SETUP VIEW

- (void) _updateAvatar
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL *profilePictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [SAccount currentUser].fbid ]];
        NSData *picData = [NSData dataWithContentsOfURL:profilePictureURL];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            _myavatar = [UIImage imageWithData:picData];
            //_image_avatar.image = _myavatar;
            _accountMainCell.cellImage.contentMode = UIViewContentModeScaleAspectFill;
            _accountMainCell.cellImage.image = _myavatar;
        });
    });
}

- (void) _setupView
{
    AccountMainCell *cell = _accountMainCell;
    if ([SAccount currentUser].isLoggedIn) {
        if ([SAccount currentUser].isFBUser) {
            if (_myavatar == nil) {
                [self _updateAvatar];
            }
        }
        cell.cellUsername.text   = [SAccount currentUser].displayname;
        cell.cellEmail.text      = [SAccount currentUser].email;
    }else{
        cell.cellUsername.text   = @"You are not logged in";
        cell.cellEmail.text      = @"You are not logged in";
    }
    
    // border radius
    [cell.cellViewImage.layer setCornerRadius:50.0f];
    
    // border
    [cell.cellViewImage.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [cell.cellViewImage.layer setBorderWidth:0.5f];
    
    if ([SAccount currentUser].isLoggedIn) {
        _tableView.tableFooterView = [[[NSBundle mainBundle] loadNibNamed:@"AccountVCLogoutView" owner:self options:nil] firstObject];
    }else{
        
    }
    
    [_tableView reloadData];
}

#pragma mark - TABLEVIEW DATASOURCE

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case SECTION_MAINCELL:
            return 1;
            break;
        case SECTION_LOGIN:
            if ([SAccount currentUser].isLoggedIn) {
                return 0;
            }else{
                return 1;
            }
            break;
        case SECTION_ADDITIONAL:
            return [_info count];
            break;
        default:
            return 1;
            break;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int section = indexPath.section;
    
    switch (section) {
        case SECTION_MAINCELL:
            return 200;
            break;
        case SECTION_LOGIN:
            if ([SAccount currentUser].isLoggedIn) {
                return 0;
            }else{
                return 100;
            }
            
            break;
        case SECTION_ADDITIONAL:
            return 44;
            break;
        default:
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int section = indexPath.section;
    
    switch (section) {
        case SECTION_MAINCELL:
            return [self setAccountMainCell:tableView andIndexPath:indexPath];
            break;
        case SECTION_LOGIN:
            return [self setAccountLoginCell:tableView andIndexPath:indexPath];
            break;
        case SECTION_ADDITIONAL:
            return [self setAccountCell:tableView andIndexPath:indexPath];
            break;
        default:
            return nil;
            break;
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{

}

# pragma mark - BUILD CELL

- (AccountMainCell *)setAccountMainCell:(UITableView *)tableView andIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellId = @"AccountMainCell";
    AccountMainCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (cell == nil) {
        cell = [[AccountMainCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellId];
    }
    
    if ([SAccount currentUser].isLoggedIn) {
        if ([SAccount currentUser].isFBUser) {
            if (_myavatar == nil) {
                [self _updateAvatar];
            }
        }
        cell.cellUsername.text   = [SAccount currentUser].displayname;
        cell.cellEmail.text      = [SAccount currentUser].email;
    }else{
        cell.cellUsername.text   = @"You are not logged in";
        cell.cellEmail.text      = @"You are not logged in";
    }
    
    // border radius
    [cell.cellViewImage.layer setCornerRadius:50.0f];
    
    // border
    [cell.cellViewImage.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [cell.cellViewImage.layer setBorderWidth:0.5f];
    
    _accountMainCell = cell;
    
    return cell;
}

- (AccountLoginCell *)setAccountLoginCell:(UITableView *)tableView andIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellId = @"AccountLoginCell";
    AccountLoginCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (cell == nil) {
        cell = [[AccountLoginCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellId];
    }
    
    cell.delegate = (id) self;
    
    return cell;
}

- (AccountCell *)setAccountCell:(UITableView *)tableView andIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellId = @"AccountCell";
    AccountCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (cell == nil) {
        cell = [[AccountCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellId];
    }
    
    cell.cellTitle.text = [_info[indexPath.row] objectAtIndex:1];
    cell.cellSubtitle.text = [_info[indexPath.row] objectAtIndex:2];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    return cell;
}

# pragma mark - CELL DELEGATES

- (void) loginCell:(AccountLoginCell *)loginCell doLogin:(NSString *)info
{
    if ([SAccount currentUser].isLoggedIn) {
        [SBase showAlert:@"You are already logged in"];
    }else{
        LoginVC *loginVC = [[LoginVC alloc]init];
        loginVC.delegate = (id) self;
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:loginVC] animated:YES completion:^{
            
        }];
    }
}

- (void) loginCell:(AccountLoginCell *)loginCell doLoginFaceBook:(NSString *)info
{
    if ([SAccount currentUser].isLoggedIn) {
        [SBase showAlert:@"You are already logged in"];
    }else{
        [SBase currentAccount].delegate = (id)self;
        [[SBase currentAccount] authenticateFB];
    }
}

- (void) loginCell:(AccountLoginCell *)loginCell doRegister:(NSString *)info
{
    if ([SAccount currentUser].isLoggedIn) {
        [SBase showAlert:@"You are already logged in"];
    }else{
        RegisterVC *registerVC = [[RegisterVC alloc]init];
        registerVC.delegate = (id) self;
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:registerVC] animated:YES completion:^{
            
        }];
    }
}

# pragma mark - ACCOUNT DELEGATES

- (void) saccount:(SAccount *)saccount didAuthenticateFBSuccess:(NSDictionary *)fbInfo
{
    NSLog(@"AccountVC didAuthenticateFBSuccess %@", fbInfo);
    
    NSArray *keys = @[@"fbid", @"email", @"displayname"];
    NSArray *objects = @[
                         [fbInfo objectForKey:@"id"],
                         [fbInfo objectForKey:@"email"],
                         [fbInfo objectForKey:@"name"]
                         ];
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    WSUser *wsUser = [[WSUser alloc] init];
    wsUser.delegate = (id)self;
    
    [wsUser loginFBUser:userInfo];
    
}

- (void) saccount:(SAccount *)saccount didAuthenticateFBFail:(NSError *)error
{
    
    NSLog(@"AccountVC didAuthenticateFBFail");
    
}

# pragma mark - WSUSER DELEGATE

- (void) wsUser:(WSUser *)wsUser didLoginFBSuccessWith:(IUser *)user
{
    NSLog(@"AccountVC didLoginFBSuccessWith");
    [self _setupView];
}

- (void) wsUser:(WSUser *)wsUser didLoginFBFailWithError:(NSError *)error
{
    NSLog(@"AccountVC didLoginFBFailWithError");
}

# pragma mark - LOGIN REGISTER DELEGATES

- (void) didLoginSuccess
{
    NSLog(@"AccountVC didLoginSuccess");
    [self dismissViewControllerAnimated:YES completion:^{
        [SBase showAlert:@"You are now logged in"];
        [self _setupView];
    }];
    
}

- (void) didLoginFail
{
    NSLog(@"AccountVC didLoginFail");
    [SBase showAlert:@"Please try to login again"];
}

- (void) didRegisterSuccess
{
    NSLog(@"AccountVC didRegisterSuccess");
    [self dismissViewControllerAnimated:YES completion:^{
        [SBase showAlert:@"You are now logged in"];
        [self _setupView];
    }];
}

- (void) didRegisterFail
{
    
}

- (void) didCancelLogin
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];

}

- (void) didCancelRegister
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        [[SBase currentAccount] logout];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

# pragma mark - IBACTIONS

- (IBAction)footerLogout:(id)sender {
    UIAlertView *logoutConfirm = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"Are you sure you want to log out" delegate:self cancelButtonTitle:@"CANCEL" otherButtonTitles:@"OK", nil];
    [logoutConfirm show];

}

@end
