//
//  DetailAdditionalActionCell.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 3/11/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import "DetailAdditionalActionCell.h"

@implementation DetailAdditionalActionCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)selectBooking:(id)sender {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(detailAdditionalActionCell:didSelectBooking:)]) {
        [self.delegate detailAdditionalActionCell:self didSelectBooking:nil];
    }
    
}
@end
