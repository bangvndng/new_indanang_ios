//
//  ContactPopupVC.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/22/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ContactPopupVCDelegate;

@interface ContactPopupVC : UIViewController

@property (weak, nonatomic) id<ContactPopupVCDelegate>delegate;
@property (strong, nonatomic) NSArray *contactsInfo;

@end

@protocol ContactPopupVCDelegate <NSObject>

- (void) popupContact:(ContactPopupVC *)popupContact callContact:(NSArray *)contactInfo;

@end
