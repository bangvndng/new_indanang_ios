//
//  LoginVC.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/17/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LoginVCDelegate <NSObject>

@optional
- (void) didCancelLogin;
- (void) didLoginSuccess;
- (void) didLoginFail;

@end

@interface LoginVC : UIViewController

@property (weak, nonatomic) id<LoginVCDelegate> delegate;
@property (nonatomic) BOOL isSwitch;

@end
