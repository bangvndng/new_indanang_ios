//
//  HomeThingsCell.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/20/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeThingsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *cellIcon;
@property (weak, nonatomic) IBOutlet UILabel *cellTitle;
@property (weak, nonatomic) IBOutlet UILabel *cellSubTitle;
@property (weak, nonatomic) IBOutlet UILabel *cellNumIcon;
@property (weak, nonatomic) IBOutlet UIView *cellViewWrapper;

@end
