//
//  AdminVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/31/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "AdminVC.h"

@interface AdminVC (){
    
    __weak IBOutlet UITableView *_tableView;
    
    NSArray *_items;
}

@end

@implementation AdminVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _items = @[
               @[
                   @"Add New Location",
                   @"AddLocationVC"
                   ],
               ];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_items count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 61;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *CellId = @"DefaultCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellId];
    }
    
    cell.textLabel.text = [_items[indexPath.row] objectAtIndex:0];
    
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Class centerSubviewClass = NSClassFromString([[_items objectAtIndex:indexPath.row] objectAtIndex:1]);
    
    UIViewController *subview = [[centerSubviewClass alloc] init];
    [self.navigationController pushViewController:subview animated:YES];
    
}

@end
