//
//  RightLargeCell.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/15/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RightLargeCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *_cellImage;
@property (weak, nonatomic) IBOutlet UILabel *_cellTitle;


@end
