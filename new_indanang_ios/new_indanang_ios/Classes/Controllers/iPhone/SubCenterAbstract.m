//
//  SubCenterAbstract.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/12/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "SubCenterAbstract.h"
#import "CenterVC.h"
#import "SearchResultVC.h"
#import "Define.h"

#import "MapVC.h"
#import "SBase.h"
@interface SubCenterAbstract (){
    __weak IBOutlet UITableView *_tableView;
}

@end

@implementation SubCenterAbstract

- (void)viewDidLoad
{
    [super viewDidLoad];
	_searchResultVC = [[SearchResultVC alloc] init];
    [self _setupView];
    self.delegate = (id)[SBase appDelegate].mainVC;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self _cleanupSearchController];
}

#pragma mark - SETUP VIEW

- (void) _setupView
{
    [self _setupNavBars];
    
}

- (void) _setupNavBars
{
    if ( self != [self.navigationController.viewControllers objectAtIndex:0] )
    {
        [self _setupMiniRightNavBar];
    }else{
        [self _setupRightNavBar];
    }
    
    
}

- (void) _setupLeftNavBarWithBackButton
{
    
    UIView *leftNavigationBarView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 160, 44.0)];
    SearchBar *searchBar = [[SearchBar alloc] initWithFrame:CGRectMake(0.0, 0.0, 160.0, 44.0)];
    searchBar.backgroundImage = [[UIImage alloc] init];
    [searchBar setBackgroundImage:[UIImage imageNamed:@"nav_bar_64"] forBarPosition:UIBarPositionTopAttached barMetrics:UIBarMetricsDefault];
    [leftNavigationBarView addSubview: searchBar];
    self.searchBar = searchBar;
    [self _setupSearchController];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0.0, 0.0, 30.0, 30.0);
    [backButton setImage:[UIImage imageNamed:@"nav_repeat.png"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(popViewController:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    self.navigationItem.leftBarButtonItems = @[
                                               backItem,
                                               [[UIBarButtonItem alloc] initWithCustomView:leftNavigationBarView]
                                               ];
    
}

- (void) _setupLeftNavBar
{
    /*
    NSLog(@"SubCenterAbstract _setupLeftNavBar");
    UIView *leftNavigationBarView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 230, 44.0)];
    SearchBar *searchBar = [[SearchBar alloc] initWithFrame:CGRectMake(-16.0, 0.0, 230.0, 44.0)];
    searchBar.backgroundImage = [[UIImage alloc] init];
    [searchBar setBackgroundImage:[UIImage imageNamed:@"nav_bar_64"] forBarPosition:UIBarPositionTopAttached barMetrics:UIBarMetricsDefault];
    
    [leftNavigationBarView addSubview: searchBar];
    self.searchBar = searchBar;
    [self _setupSearchController];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftNavigationBarView];
    */
}

- (void)_setupSearchController
{
    UISearchDisplayController *searchDisplayController = [[UISearchDisplayController alloc] initWithSearchBar:self.searchBar contentsController:_searchResultVC];
    
    searchDisplayController.searchBar.showsCancelButton = NO;
    [searchDisplayController setDelegate:(id)_searchResultVC];
    [searchDisplayController setSearchResultsDataSource:(id)_searchResultVC];
    [searchDisplayController setSearchResultsDelegate:(id)_searchResultVC];
    
    self.centerSearchDisplayController = searchDisplayController;
    
    [self.searchBar setDelegate:(id)_searchResultVC];
    [self addChildViewController:_searchResultVC];
    [_searchResultVC didMoveToParentViewController:self];
    
}

- (void)_cleanupSearchController
{
    [self.centerSearchDisplayController setActive:NO];
}

- (void) _setupMiniRightNavBar
{
    UIButton *mapButton = [UIButton buttonWithType:UIButtonTypeCustom];
    mapButton.frame = CGRectMake(0.0, 0.0, 30.0, 30.0);
    [mapButton setImage:[UIImage imageNamed:@"nv_map"] forState:UIControlStateNormal];
    [mapButton addTarget:self action:@selector(showMap:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuButton setImage:[UIImage imageNamed:@"nv_menu"] forState:UIControlStateNormal];
    menuButton.frame = CGRectMake(0.0, 0.0, 30.0, 30.0);
    menuButton.tag = 1;
    [menuButton addTarget:self action:@selector(showMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *leftNavBarView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 155, 44.0)];
    SearchBar *searchBar = [[SearchBar alloc] initWithFrame:CGRectMake(0.0, 0.0, 155, 44.0)];
    searchBar.backgroundImage = [[UIImage alloc] init];
    [searchBar setBackgroundImage:[UIImage imageNamed:@"nav_bar_64"] forBarPosition:UIBarPositionTopAttached barMetrics:UIBarMetricsDefault];
    
    [leftNavBarView addSubview: searchBar];
    _leftNavBarView = leftNavBarView;
    
    self.searchBar = searchBar;
    [self _setupSearchController];
    
    _menuItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    _mapItem = [[UIBarButtonItem alloc] initWithCustomView:mapButton];
    _searchBarItem = [[UIBarButtonItem alloc] initWithCustomView:_leftNavBarView];
    
    if ([self isKindOfClass:[MapVC class]]) {
        self.navigationItem.rightBarButtonItems = @[_menuItem, _searchBarItem];
    }else{
        self.navigationItem.rightBarButtonItems = @[_menuItem, _mapItem, _searchBarItem];
    }
    
    

}

- (void) _setupRightNavBar
{
    UIButton *mapButton = [UIButton buttonWithType:UIButtonTypeCustom];
    mapButton.frame = CGRectMake(0.0, 0.0, 30.0, 30.0);
    [mapButton setImage:[UIImage imageNamed:@"nv_map"] forState:UIControlStateNormal];
    [mapButton addTarget:self action:@selector(showMap:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *menuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [menuButton setImage:[UIImage imageNamed:@"nv_menu"] forState:UIControlStateNormal];
    menuButton.frame = CGRectMake(0.0, 0.0, 30.0, 30.0);
    menuButton.tag = 1;
    [menuButton addTarget:self action:@selector(showMenu:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *leftNavBarView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, 230, 44.0)];
    SearchBar *searchBar = [[SearchBar alloc] initWithFrame:CGRectMake(5.0, 0.0, 230.0, 44.0)];
    searchBar.backgroundImage = [[UIImage alloc] init];
    [searchBar setBackgroundImage:[UIImage imageNamed:@"nav_bar_64"] forBarPosition:UIBarPositionTopAttached barMetrics:UIBarMetricsDefault];
    
    [leftNavBarView addSubview: searchBar];
    
    _leftNavBarView = leftNavBarView;
    self.searchBar = searchBar;
    [self _setupSearchController];
    
    _menuItem = [[UIBarButtonItem alloc] initWithCustomView:menuButton];
    _mapItem = [[UIBarButtonItem alloc] initWithCustomView:mapButton];
    _searchBarItem = [[UIBarButtonItem alloc] initWithCustomView:_leftNavBarView];
    
    
    if ([self isKindOfClass:[MapVC class]]) {
        self.navigationItem.rightBarButtonItems = @[_menuItem, _searchBarItem];
    }else{
        self.navigationItem.rightBarButtonItems = @[_menuItem, _mapItem, _searchBarItem];
    }
}

#pragma mark - SEARCH BAR ANIMATION

- (void) expandSearchBar
{
    if ( self != [self.navigationController.viewControllers objectAtIndex:0] )
    {
        _leftNavBarView.frame = CGRectMake(0, 0, 230, 44);
        _searchBar.frame = CGRectMake(0, 0, 230, 44);
        _searchBarItem = [[UIBarButtonItem alloc] initWithCustomView:_leftNavBarView];
        self.navigationItem.rightBarButtonItems = @[_searchBarItem];
    }else{
        _leftNavBarView.frame = CGRectMake(0, 0, 301, 44);
        _searchBar.frame = CGRectMake(0, 0, 230, 44);
        _searchBarItem = [[UIBarButtonItem alloc] initWithCustomView:_leftNavBarView];
        self.navigationItem.rightBarButtonItems = @[_searchBarItem];
    }
    
    
}

- (void) collapseSearchBar
{
    if ( self != [self.navigationController.viewControllers objectAtIndex:0] )
    {
        _leftNavBarView.frame = CGRectMake(0.0, 0.0, 155, 44.0);
        _searchBar.frame = CGRectMake(5.0, 0.0, 155.0, 44.0);
        _searchBarItem = [[UIBarButtonItem alloc] initWithCustomView:_leftNavBarView];
        if ([self isKindOfClass:[MapVC class]]) {
            self.navigationItem.rightBarButtonItems = @[_menuItem, _searchBarItem];
        }else{
            self.navigationItem.rightBarButtonItems = @[_menuItem, _mapItem, _searchBarItem];
        }
        
    }else{
        _leftNavBarView.frame = CGRectMake(0.0, 0.0, 230, 44.0);
        _searchBar.frame = CGRectMake(5.0, 0.0, 230.0, 44.0);
        _searchBarItem = [[UIBarButtonItem alloc] initWithCustomView:_leftNavBarView];
        if ([self isKindOfClass:[MapVC class]]) {
            self.navigationItem.rightBarButtonItems = @[_menuItem, _searchBarItem];
        }else{
            self.navigationItem.rightBarButtonItems = @[_menuItem, _mapItem, _searchBarItem];
        }
        
    }
    
}

#pragma mark - NAVIGATION BAR ACTIONS

- (void) hideRightNavBar
{
    
}

- (void) showMap:(id) sender
{
    MapVC *mapVC = [[MapVC alloc] init];
    [self.navigationController pushViewController:mapVC animated:YES];
}

- (void) showMenu: (id) sender
{
    
    if ([(CenterVC *)self.navigationController menu_state] == 0)
    {
        if (_delegate && [_delegate respondsToSelector:@selector(callShowMenu)]) {
            [_delegate callShowMenu];
        }
    }else{
        
        if (_delegate && [_delegate respondsToSelector:@selector(callHideMenu)]) {
            [_delegate callHideMenu];
        }
    }
    
}

- (void) popViewController: (id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
