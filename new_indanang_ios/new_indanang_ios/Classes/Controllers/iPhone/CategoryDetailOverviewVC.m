//
//  CategoryDetailOverviewVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/29/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "CategoryDetailOverviewVC.h"

@interface CategoryDetailOverviewVC ()

@end

@implementation CategoryDetailOverviewVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

#define FONT_SIZE 14.0f
#define CELL_CONTENT_WIDTH 300.0f
#define CELL_CONTENT_MARGIN 5.0f

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSString *text = _overview;
    CGFloat width = CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2);
    UIFont *font = [UIFont systemFontOfSize:FONT_SIZE];
    NSAttributedString *attributedText = [[NSAttributedString alloc]
                                          initWithString:text
                                          attributes:@
                                          {
                                          NSFontAttributeName: font
                                          }];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){width, CGFLOAT_MAX}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    CGSize size = rect.size;
    
    _scrollviewContent.contentSize = CGSizeMake(size.width, size.height);
    _lbContent.frame = CGRectMake(5, 5, size.width - 10, size.height - 10);
    _lbContent.text = text;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
