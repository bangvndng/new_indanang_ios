//
//  DetailOverviewCell.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/22/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DetailOverviewCellDelegate;

@interface DetailOverviewCell : UITableViewCell
@property (weak, nonatomic) id<DetailOverviewCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *cellTitle;
- (IBAction)cellContinueRead:(id)sender;
@end

@protocol DetailOverviewCellDelegate <NSObject>

@optional
- (void) detailOverviewCell:(DetailOverviewCell *)detailOverviewCell continueReading:(NSDictionary *)info;

@end
