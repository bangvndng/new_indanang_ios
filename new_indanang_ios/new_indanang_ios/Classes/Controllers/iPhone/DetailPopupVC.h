//
//  DetailPopupVC.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 3/11/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailPopupVC : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *detailTextView;

@property (strong, nonatomic) NSString *detailText;

@end
