//
//  CenterVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/12/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "CenterVC.h"
#import "Define.h"

@interface CenterVC ()

@end

@implementation CenterVC

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _menu_state = CENTERVC_MENUSTATE_CLOSE;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
