//
//  CategoryOverviewVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 2/9/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import "CategoryOverviewVC.h"
#import "SBase.h"

#import "LocationVC.h"

@interface CategoryOverviewVC ()<UIWebViewDelegate>{
    
    __weak IBOutlet UITableView *_tableView;
    //NSArray *_additionalDatas;
}

@end

@implementation CategoryOverviewVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _category = [[SBase dataSource] loadCategoryById:_category.category_id];
    
    [self _loadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Load Data

- (BOOL)_hasWYSIWYGContent
{
    if ([_category.wysiwyg_content isEqualToString:@""] || !_category.wysiwyg_content) {
        return NO;
    }else{
        return YES;
    }
}

- (void)_loadData
{
    if ([self _hasWYSIWYGContent]) {
        NSLog(@"CategoryOverviewVC _loadData wysiwyg_content %@", _category.wysiwyg_content);
    }else{
        
    }
}

#pragma mark - TABLEVIEW DATASOURCE

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
    if ([self _hasWYSIWYGContent]) {
        return @"WYSIWYG Data";
    }else{
        return @"Additional Data";
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self _hasWYSIWYGContent]) {
        return 1;
    }else{
        return [_categoryDetails count];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self _hasWYSIWYGContent]) {
        return 568-64;
    }else{
        return 44;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyIdentifier"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"MyIdentifier"];
    }
    
    if ([self _hasWYSIWYGContent]) {
        UIWebView *aWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 568-64)];
        //init and create the UIWebView
        
        aWebView.autoresizesSubviews = YES;
        
        [aWebView setDelegate:(id)self];
        //NSString *urlAddress = [NSString stringWithFormat:@"%@/categories/wysiwyg_view?id=%@",COMMON_WS_URL,_category.category_id];
        // test view is working with url to webpage
        
        //NSURL *url = [NSURL URLWithString:urlAddress];
        
        //NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        
        //[aWebView loadRequest:requestObj];
        NSString *embedHTML = _category.wysiwyg_content;
        [aWebView loadHTMLString:embedHTML baseURL:nil];
        
        [cell.contentView addSubview:aWebView];
    }else{
        //return 44;
        cell.textLabel.text = [[_categoryDetails objectAtIndex:indexPath.row] valueForKey:@"content"];
        cell.detailTextLabel.text = [[_categoryDetails objectAtIndex:indexPath.row] valueForKey:@"header"];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - UIWebview Delegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *location_url = [[request URL] absoluteString];
    NSLog(@"%@", location_url);
    
    NSArray *foo = [location_url componentsSeparatedByString: @"@"];
    
    if ([foo count] > 1) {
        NSString *location_id = [foo objectAtIndex: 1];
        
        NSLog(@"%@", location_id);
        
        LocationVC *locationVC = [[LocationVC alloc] init];
        locationVC.location_id = location_id;
        
        [self.navigationController pushViewController:locationVC animated:YES];
    }
    
    
    return YES;
}

@end
