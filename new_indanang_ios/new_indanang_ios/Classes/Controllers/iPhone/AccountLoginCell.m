//
//  AccountLoginCell.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/29/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "AccountLoginCell.h"

@implementation AccountLoginCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)doRegister:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(loginCell:doRegister:)]) {
        [self.delegate loginCell:self doRegister:@""];
    }
}

- (IBAction)doLogin:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(loginCell:doLogin:)]) {
        [self.delegate loginCell:self doLogin:@""];
    }
}

- (IBAction)doLoginWithFacebook:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(loginCell:doLoginFaceBook:)]) {
        [self.delegate loginCell:self doLoginFaceBook:@""];
    }
}
@end
