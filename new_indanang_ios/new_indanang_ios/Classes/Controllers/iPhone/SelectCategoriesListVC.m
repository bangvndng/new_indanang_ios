//
//  SelectCategoriesListVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/31/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "SelectCategoriesListVC.h"
#import "SBase.h"


@interface SelectCategoriesListVC (){
    
    __weak IBOutlet UITableView *_tableView;
    
    NSMutableArray *_items;
    
}

@end

@implementation SelectCategoriesListVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self loadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadData
{
    
    _items = [[NSMutableArray alloc] init];
    
    _items = [[[SBase dataSource] loadLeafCategories] mutableCopy];
    
    
    [_tableView reloadData];
}

# pragma mark - TABLE VIEW DATASOURCE

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_items count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *CellId = @"DefaultCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellId];
    }
    
    cell.textLabel.text = [(ICategory *)_items[indexPath.row] name];
    cell.detailTextLabel.text  = [(ICategory *)_items[indexPath.row] short_description];
    
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(selectCategory:didSelect:)]) {
        [self.delegate selectCategory:self didSelect:(ICategory *)_items[indexPath.row]];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
