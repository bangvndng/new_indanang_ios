//
//  DetailPopupVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 3/11/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import "DetailPopupVC.h"

@interface DetailPopupVC ()

@end

@implementation DetailPopupVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _detailTextView.text = _detailText;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
