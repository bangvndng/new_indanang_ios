//
//  AddRecommentVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/22/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "AddRecommentVC.h"
#import "SBase.h"
#import "WSRecommend.h"
@interface AddRecommentVC ()<WSRecommendDelegate, UITextViewDelegate>
{
    
    __weak IBOutlet UITextView *textviewContent;
    __weak IBOutlet UISwitch *switchFB;
    
}

@end

@implementation AddRecommentVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *barButtonCancel = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                        style:UIBarButtonItemStyleBordered
                                                                       target:self
                                                                       action:@selector(performCancel:)];
    self.navigationItem.leftBarButtonItem = barButtonCancel;
    
    UIBarButtonItem *barButtonNext = [[UIBarButtonItem alloc] initWithTitle:@"Next"
                                                                      style:UIBarButtonItemStyleBordered
                                                                     target:self
                                                                     action:@selector(performNext:)];
    self.navigationItem.rightBarButtonItem = barButtonNext;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [textviewContent becomeFirstResponder];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)performNext: (id)sender
{
    NSLog(@"AddTipVC performNext");
    
    if ([textviewContent.text isEqualToString:@""]) {
        
        [SBase showAlert:@"Please complete the text field"];
        
    }else{
        NSArray *keys;
        NSArray *objects;
        if (![SAccount currentUser].user_id ) {
            keys = @[@"location_id", @"content"];
            objects = @[_location_id, textviewContent.text];
        }else{
            keys = @[@"location_id", @"content", @"user_id"];
            objects = @[_location_id, textviewContent.text, [SAccount currentUser].user_id];
        }
        
        NSDictionary *tipInfo = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
        
        WSRecommend *wsRecommend = [[WSRecommend alloc] init];
        wsRecommend.delegate = (id)self;
        
        [wsRecommend addRecommend:tipInfo];
    }

    /*
    NSLog(@"AddRecommentVC performNext");
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        [SBase showAlert:@"Thank you for added recomment for this place"];
    }];
    */
}

- (void)performCancel: (id)sender
{
    NSLog(@"AddRecommentVC perfornCancel");
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    textviewContent.text = @"";
}

#pragma mark - WSRecommend Delegate

-(void)wsRecommend:(WSRecommend *)wsRecommend didAddRecommendSuccess:(NSDictionary *)info
{
    //[self.navigationController dismissViewControllerAnimated:YES completion:^{
        //[SBase showAlert:@"Thank you for added recomment for this place"];
    //}];
    
    if (switchFB.on == YES) {
        [self performPublishAction:^{
            NSString *message = [NSString stringWithFormat:@"Updating status for %@ at %@", [SAccount currentUser].displayname, [NSDate date]];
            
            FBRequestConnection *connection = [[FBRequestConnection alloc] init];
            
            connection.errorBehavior = FBRequestConnectionErrorBehaviorReconnectSession
            | FBRequestConnectionErrorBehaviorAlertUser
            | FBRequestConnectionErrorBehaviorRetry;
            
            [connection addRequest:[FBRequest requestForPostStatusUpdate:message]
                 completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                     NSLog(@"AddTipVC Post Tip to Facebook success");
                     [SBase showAlert:@"Your Recommend has been successfully post to your Facebook"];
                 }];
            [connection start];
        }];

    }
    
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(addRecommend:didAddRecommendSuccess:)]) {
        [self.delegate addRecommend:self didAddRecommendSuccess:nil];
    }
}

-(void)wsRecommend:(WSRecommend *)wsRecommend didAddRecommendFail:(NSError *)error
{
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        [SBase showAlert:@"Thank you for added recomment for this place, please try again later"];
    }];
}

#pragma mark - POST FACEBOOK

// Convenience method to perform some action that requires the "publish_actions" permissions.
- (void) performPublishAction:(void (^)(void)) action {
    // we defer request for permission to post to the moment of post, then we check for the permission
    //if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound) {
        [FBSession openActiveSessionWithPublishPermissions:@[@"publish_actions"] defaultAudience:FBSessionDefaultAudienceFriends allowLoginUI:YES completionHandler:^(FBSession *session,FBSessionState state, NSError *error) {
            if (!error) {
                action();
            } else if (error.fberrorCategory != FBErrorCategoryUserCancelled){
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Permission denied"
                                                                    message:@"Unable to get permission to post"
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                [alertView show];
            }
        }];
        
//    } else {
//        NSLog(@"AddTipVC found publish_actions");
//        action();
//    }
    
}

@end
