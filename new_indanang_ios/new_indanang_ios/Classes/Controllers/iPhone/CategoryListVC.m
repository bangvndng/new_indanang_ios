//
//  CategoryListVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/19/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "CategoryListVC.h"
#import "CategoryDetailVC.h"
#import "SBase.h"

#import "CategoryListCell.h"

#import "CategoryOverviewVC.h"


#define CL_TABLE_ID_INDEX           0
#define CL_TABLE_TITLE_INDEX        1
#define CL_TABLE_ICON_INDEX         2
#define CL_TABLE_OVERVIEW_INDEX     3
#define CL_TABLE_DESCRIPTION_INDEX  4
#define CL_TABLE_HASLOCATION_INDEX  5

#import "LocationCategory.h"

@interface CategoryListVC ()<SDataDelegate>{
    
    __weak IBOutlet UITableView *_tableView;
    
    //PULL TO REFRESH
    BOOL                                    _isFirstTouch, _isLoading;
    
    UIView                                  *refreshHeaderView;
    UILabel                                 *refreshLabel;
    UIImageView                             *refreshArrow;
    UIActivityIndicatorView                 *refreshSpinner;
    BOOL                                    isDragging;
    BOOL                                    isLoading;
    NSString                                *textPull;
    NSString                                *textRelease;
    NSString                                *textLoading;
    // ------------- //
    
    NSMutableArray *_childrenCategories;
}

@end

@implementation CategoryListVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupStrings];
    [self addPullToRefreshHeader];
    [self registerNibsForTableView];
    
    [self _loadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) registerNibsForTableView
{
    NSArray *cells = @[@"CategoryListCell"];
    
    for (NSString *cellClass in cells) {
        [_tableView registerNib:[UINib  nibWithNibName:cellClass
                                                bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:cellClass];
    }
}

# pragma mark - Load Data

- (void) _reloadData
{
    [SBase dataSource].delegate = (id)self;
    [[SBase dataSource] loadAllCategories:YES];
    
    
}

- (void) sData:(SData *)sData didLoadAllCategoriesSuccess:(NSArray *)categories
{
    [SBase dataSource].delegate = (id)self;
    [[SBase dataSource] loadCategoryDetails];
}

- (void) sData:(SData *)sData didLoadCategoryDetailsSuccess:(NSArray *)categoryDetails
{
    [self _loadData];
    [self stopLoading];
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void) _loadData
{
    if (_childrenCategories == nil) {
        _childrenCategories = [[NSMutableArray alloc] init];
    }else{
        [_childrenCategories removeAllObjects];
    }
    
    NSArray *categories = [[SBase dataSource] loadChildrenCategoriesById:self.category.category_id];
    
    for (LocationCategory *category in categories) {
        NSString *icon_class;
        if ([[category valueForKey:@"icon_class"] isEqualToString:@""] || ![category valueForKey:@"icon_class"]) {
            icon_class = @"i_cl_activity";
        }else{
            icon_class = [category valueForKey:@"icon_class"];
        }
        NSArray *addingCategory = @[
                                    [category valueForKey:@"category_id"],
                                    [category valueForKey:@"name"],
                                    icon_class,
                                    [category valueForKey:@"overview"],
                                    [category valueForKey:@"short_description"],
                                    [category valueForKey:@"hasLocation"]
                                    ];
        [_childrenCategories addObject:addingCategory];
    }
        
}

# pragma mark - TABLEVIEW DATASOURCE

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_childrenCategories count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 61;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *CellId = @"CategoryListCell";
    CategoryListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (cell == nil) {
        cell = [[CategoryListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellId];
    }
    
    cell.cellTitle.text = [_childrenCategories[indexPath.row] objectAtIndex:CL_TABLE_TITLE_INDEX];
    cell.cellIcon.image = [UIImage imageNamed:[_childrenCategories[indexPath.row] objectAtIndex:CL_TABLE_ICON_INDEX]];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ICategory *category = [[ICategory alloc] init];
    category.category_id    = [_childrenCategories[indexPath.row] objectAtIndex:CL_TABLE_ID_INDEX];
    category.name           = [_childrenCategories[indexPath.row] objectAtIndex:CL_TABLE_TITLE_INDEX];
    category.overview       = [_childrenCategories[indexPath.row] objectAtIndex:CL_TABLE_OVERVIEW_INDEX];
    
    if ([[_childrenCategories[indexPath.row] objectAtIndex:CL_TABLE_HASLOCATION_INDEX] isEqualToNumber:[NSNumber numberWithInt:0]]) {
        CategoryOverviewVC *categoryOverviewVC = [[CategoryOverviewVC alloc] init];
        
        NSArray *categoryDetails = [[SBase dataSource] loadCategoryDetailsByCategoryId:[_childrenCategories[indexPath.row] objectAtIndex:CL_TABLE_ID_INDEX]];
        
        categoryOverviewVC.categoryDetails = categoryDetails;
        categoryOverviewVC.category = category;
        
        [self.navigationController pushViewController:categoryOverviewVC animated:YES];
    }else{
        CategoryDetailVC *categoryDetail = [[CategoryDetailVC alloc] init];
        
        categoryDetail.category = category;
        
        [self.navigationController pushViewController:categoryDetail animated:YES];
    }
    
    
}

#pragma mark - Pull CODE
- (void)setupStrings{
    textPull    = @"Pull down to refresh";             //@"Pull down to refresh...";
    textRelease = @"Release to refresh";                   //@"Release to refresh...";
    textLoading = @"Now loading";                             //@"Loading...";
}

- (void)addPullToRefreshHeader {
    refreshHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 - REFRESH_HEADER_HEIGHT, 320, REFRESH_HEADER_HEIGHT)];
    refreshHeaderView.backgroundColor = [UIColor clearColor];
    
    refreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(110, 0, 320, REFRESH_HEADER_HEIGHT)];
    refreshLabel.backgroundColor = [UIColor clearColor];
    refreshLabel.font = [UIFont boldSystemFontOfSize:13.0];
    refreshLabel.textColor = [UIColor colorWithRed:197.0/255.0 green:187.0/255.0 blue:184.0/255.0 alpha:1.0];
    
    refreshArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pull_arrow.png"]];
    refreshArrow.frame = CGRectMake(floorf((REFRESH_HEADER_HEIGHT - 13) / 2 + 70),
                                    (floorf(REFRESH_HEADER_HEIGHT - 26) / 2),
                                    13, 26);
    
    refreshSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    refreshSpinner.frame = CGRectMake(floorf(floorf(REFRESH_HEADER_HEIGHT - 20) / 2) + 70 , floorf((REFRESH_HEADER_HEIGHT - 20) / 2), 20, 20);
    refreshSpinner.hidesWhenStopped = YES;
    
    [refreshHeaderView addSubview:refreshLabel];
    [refreshHeaderView addSubview:refreshArrow];
    [refreshHeaderView addSubview:refreshSpinner];
    [_tableView addSubview:refreshHeaderView];
}

- (void)startLoading {
    isLoading = YES;
    
    // Show the header
    [UIView animateWithDuration:0.3 animations:^{
        _tableView.contentInset = UIEdgeInsetsMake(REFRESH_HEADER_HEIGHT, 0, 0, 0);
        refreshLabel.text = textLoading;
        refreshArrow.hidden = YES;
        [refreshSpinner startAnimating];
    } completion:^(BOOL finished) {
        
        [self _reloadData];
        
        /*
        [UIView animateWithDuration:0.5 animations:^{
            
            [self stopLoading];
        }];
        */
    }];
}

- (void)stopLoading {
    isLoading = NO;
    
    // Hide the header
    [UIView animateWithDuration:0.3 animations:^{
        _tableView.contentInset = UIEdgeInsetsZero;
        [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
    }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(stopLoadingComplete)];
                     }];
}

- (void)stopLoadingComplete {
    // Reset the header
    refreshLabel.text = textPull;
    refreshArrow.hidden = NO;
    [refreshSpinner stopAnimating];
}

- (void)refresh {
    // This is just a demo. Override this method with your custom reload action.
    // Don't forget to call stopLoading at the end.
    [self performSelector:@selector(stopLoading) withObject:nil afterDelay:2.0];
}



#pragma mark - UIScrollDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (scrollView == _tableView) {
        if (isLoading) return;
        isDragging = YES;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == _tableView) {
        if (isLoading) {
            // Update the content inset, good for section headers
            if (scrollView.contentOffset.y > 0)
                _tableView.contentInset = UIEdgeInsetsZero;
            else if (scrollView.contentOffset.y >= -REFRESH_HEADER_HEIGHT)
                _tableView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        } else if (isDragging && scrollView.contentOffset.y < 0) {
            // Update the arrow direction and label
            [UIView animateWithDuration:0.25 animations:^{
                if (scrollView.contentOffset.y < -REFRESH_HEADER_HEIGHT) {
                    // User is scrolling above the header
                    refreshLabel.text = textRelease;
                    [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
                } else {
                    // User is scrolling somewhere within the header
                    refreshLabel.text = textPull;
                    [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
                }
            }];
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView == _tableView) {
        if (!decelerate) {
            if (scrollView.contentOffset.y <= scrollView.contentSize.height - 1.0 * scrollView.frame.size.height)// &&
                //[_feeds count] < _total &&
                //!_isLoading)
            {
                //[self loadMoreFeeds];
            }
        }
        
        if (isLoading) return;
        isDragging = NO;
        if (scrollView.contentOffset.y <= -REFRESH_HEADER_HEIGHT) {
            // Released above the header
            [self startLoading];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == _tableView) {
        if (scrollView.contentOffset.y <= scrollView.contentSize.height - 1.0 * scrollView.frame.size.height) //&&
            //[_feeds count] < _total  &&
            //!_isLoading)
        {
            //[self loadMoreFeeds];
        }
    }
}

@end
