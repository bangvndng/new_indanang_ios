//
//  LocationVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/19/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "LocationVC.h"
#import "Define.h"
#define DETAIL_TABLE_SECTION_MAININF    0
#define DETAIL_TABLE_SECTION_ADDINF     2
#define DETAIL_TABLE_SECTION_ADDACTION  1
#define DETAIL_TABLE_SECTION_OVERVIEW   3
#define DETAIL_TABLE_SECTION_TIP        4

#define DETAIL_ADDINF_EMAIL             0
#define DETAIL_ADDINF_URL               1
#define DETAIL_ADDINF_PHONE             2

#define DETAIL_WSLOCATION_FLAG_NORMAL   0
#define DETAIL_WSLOCATION_FLAG_RELOAD   1

#define FONT_SIZE                       14.0f
#define CELL_CONTENT_WIDTH              320.0f
#define CELL_CONTENT_MARGIN             10.0f

#define LOCATIONPOPUP_CONTACT_TYPE_INDEX    0
#define LOCATIONPOPUP_CONTACT_VALUE_INDEX   1

#define LOCATIONPOPUP_CONTACT_TYPE_URL      @"URL"
#define LOCATIONPOPUP_CONTACT_TYPE_PHONE    @"Phone"
#define LOCATIONPOPUP_CONTACT_TYPE_Email    @"Email"

#import "DetailMainInfoCell.h"
#import "DetailAdditionalInfoCell.h"
#import "DetailOverviewCell.h"
#import "DetailTipCell.h"
#import "DetailAdditionalActionCell.h"

#import "AddTipVC.h"
#import "AddPhotosVc.h"
#import "AddRecommentVC.h"

#import "DirectionPopupVC.h"
#import "ContactPopupVC.h"
#import "DetailPopupVC.h"
#import "BookingPopupVC.h"

#import "WSLocation.h"
#import "WSPhoto.h"

#import "SBase.h"

#import "UIViewController+MJPopupViewController.h"

#import <MessageUI/MessageUI.h>
#import "MapVC.h"

#import "RegisterVC.h"

#import "DetailOverviewVC.h"

#import "LocationManager.h"

@interface LocationVC ()<
    DetailMainInfoCellDelegate,
    DetailAdditionalActionCellDelete,
    DetailOverviewCellDelegate,
    DetailTipCellDelegate,
    AddRecommentVCDelegate,
    DirectionPopupVCDelegate,
    ContactPopupVCDelegate,
    BookingPopupVCDelegate,
    WSLocationDelegate,
    WSPhotoDelegate,
    MFMailComposeViewControllerDelegate,
    UIAlertViewDelegate>{
    
    __weak IBOutlet UITableView *_tableView;
    
    
    IBOutlet UIView *detailScrollAdd;
    
    UIView *_recommentConfirmView;
    
    NSManagedObject *_location;
        
    NSArray *_photos;
    
    DetailMainInfoCell *_detailMainInfoCell;
    DetailAdditionalActionCell *_detailAditionalActionCell;
    NSMutableArray *_additionalInfo;
    NSMutableArray *_contactInfo;
        
    NSArray *_transporters;
        
    MFMailComposeViewController *_mailComposeViewController;
        
    ContactPopupVC      *_contactPopupVC;
    DirectionPopupVC    *_directionPopupVC;
    BookingPopupVC      *_bookingPopupVC;
        
    //PULL TO REFRESH
    BOOL                                    _isFirstTouch, _isLoading;
    
    UIView                                  *refreshHeaderView;
    UILabel                                 *refreshLabel;
    UIImageView                             *refreshArrow;
    UIActivityIndicatorView                 *refreshSpinner;
    BOOL                                    isDragging;
    BOOL                                    isLoading;
    NSString                                *textPull;
    NSString                                *textRelease;
    NSString                                *textLoading;
    // ------------- //
        
    int                                     _wsLocationFlag;
        
    // BOOKING
    BOOL    _alreadyBooked;
    
}
- (IBAction)detailScrollAddPhoto:(id)sender;
- (IBAction)detailScrollAddTip:(id)sender;

- (IBAction)recommentConfirmCancel:(id)sender;
- (IBAction)recommentConfirmOk:(id)sender;


@end

@implementation LocationVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self registerNibsForTableView];
    
    [self addPullToRefreshHeader];
    
    [self loadData];
    
    [self loadAdditionalData];
    
    if (!_location_id) {
        _location_id = [_location valueForKey:@"location_id"];
    }
    
    if (!_flag_from) {
        _flag_from = 0;
    }
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [self loadPhotos];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark - VIEW SETUP

- (void) registerNibsForTableView
{
    NSArray *cells = @[
                       @"DetailMainInfoCell",
                       @"DetailAdditionalInfoCell",
                       @"DetailOverviewCell",
                       @"DetailTipCell",
                       @"DetailAdditionalActionCell"
                       ];
    
    for (NSString *cellClass in cells) {
        [_tableView registerNib:[UINib  nibWithNibName:cellClass
                                                bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:cellClass];
    }
}

- (void) setupLocationPhotos
{
    DetailMainInfoCell *cell = (DetailMainInfoCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    if ([_photos count] == 0) {
        [cell.indicatorImages stopAnimating];
    }
    
    int count = 0;
    
    for (IPhoto * iPhoto in _photos) {
        
        NSString *mainImage = iPhoto.path;
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(320 * count, 0.0, 320.0, 187.0)];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSString *imageURL = [NSString stringWithFormat:@"%@/%@", COMMON_WS_URL, mainImage];
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageURL]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                imageView.image = [UIImage imageWithData:imageData];
                [cell.indicatorImages stopAnimating];
            });
            
        });
        
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        [cell.scrollview addSubview:imageView];
        count++;
    }
    
    UIView *addtionalView = [[[NSBundle mainBundle] loadNibNamed:@"DetailScrollAdd" owner:self options:nil] firstObject];
    addtionalView.frame = CGRectMake(320 * [_photos count], 0, 320, 187);
    [cell.scrollview addSubview:addtionalView];
    
    cell.scrollview.contentSize = CGSizeMake(320.0 * ([_photos count] + 1), 187.0);
}

#pragma mark - INHERITANCE FUNCTIONS

- (void) showMap:(id) sender
{
    if (_flag_from == DETAIL_FLAG_FROM_MAP) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        MapVC *mapVC = [[MapVC alloc] init];
        mapVC.type = MAP_TYPE_SINGLE;
        mapVC.map_location_id = _location_id;
        
        [self.navigationController pushViewController:mapVC animated:YES];
    }
    
}

#pragma mark - LOAD DATA

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void) _reloadData
{
    [[SBase dataSource] loadWSLocationDetailsForLocationId:_location_id];
    
    [self loadData];
    [_tableView reloadData];
}

- (void) loadData
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Location"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"location_id = %@", _location_id];
    [fetchRequest setPredicate:predicate];
    
    _location = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] firstObject];
    
    if (_location) {
        _additionalInfo = [[NSMutableArray alloc] init];
        _contactInfo = [[NSMutableArray alloc] init];
        if (![[_location valueForKey:@"email"] isEqualToString:@""]) {
            [_additionalInfo addObject:@[@"Email",  [_location valueForKey:@"email"]]];
        }
        if (![[_location valueForKey:@"website"] isEqualToString:@""]) {
            [_additionalInfo addObject:@[@"URL",    [_location valueForKey:@"website"]]];
        }
        if (![[_location valueForKey:@"phone"] isEqualToString:@""]) {
            [_additionalInfo addObject:@[@"Phone",  [_location valueForKey:@"phone"]]];
        }
        
        _contactInfo = [_additionalInfo mutableCopy];
        
    }
    
    NSArray *locationDetails = [[SBase dataSource] loadLocationDetailsByLocationId:_location_id];
    if ([locationDetails count] > 0) {
        for (NSManagedObject *LocationDetailMO in locationDetails) {
            [_additionalInfo addObject:@[
                                         [LocationDetailMO valueForKey:@"header"],
                                         [LocationDetailMO valueForKey:@"content"]
                                         ]];
        }
    }
    NSLog(@"LocationVC loadData %@", locationDetails);
    
    _alreadyBooked = NO;
    
    if (_booking_info && [SAccount currentUser].isLoggedIn) {
        
        NSLog(@"LocationVC loadData getBookings %@",
              [[SBase dataSource] getBookingsForUser:[[SAccount currentUser] user_id]
                                         andLocation:_location_id
                                              forDay:[_booking_info objectForKey:@"checkin"]]);
        NSArray *bookings = [[SBase dataSource] getBookingsForUser:[[SAccount currentUser] user_id]
                                                       andLocation:_location_id
                                                            forDay:[_booking_info objectForKey:@"checkin"]];
        if ([bookings count] > 0) {
            _alreadyBooked = YES;
        }
    }else{
        
        NSDateFormatter *FormatDate = [[NSDateFormatter alloc] init];
        [FormatDate setLocale: [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
        [FormatDate setDateFormat:DATE_FORMAT];
        NSString *todayText = [FormatDate stringFromDate:[NSDate date]];
        
        NSLog(@"LocationVC loadData getBookings %@",
              [[SBase dataSource] getBookingsForUser:[[SAccount currentUser] user_id]
                                         andLocation:_location_id
                                              forDay:todayText]);
        NSArray *bookings = [[SBase dataSource] getBookingsForUser:[[SAccount currentUser] user_id]
                                                       andLocation:_location_id
                                                            forDay:todayText];
        if ([bookings count] > 0) {
            _alreadyBooked = YES;
        }
    }
    
}

- (void) loadAdditionalData
{
    WSLocation *wsLocation = [[WSLocation alloc] init];
    wsLocation.delegate = (id) self;
    [wsLocation getLocation:_location_id];
    
}


#pragma mark - LOAD PHOTOS

- (void) loadPhotos
{
    WSPhoto *wsPhoto = [[WSPhoto alloc] init];
    wsPhoto.delegate = (id) self;
    
    [wsPhoto listPhotos:_location_id];
    
    DetailMainInfoCell *cell = (DetailMainInfoCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [cell.indicatorImages startAnimating];
    
}

#pragma mark - WSPHOTO DELEGATE

- (void) wsPhoto:(WSPhoto *)wsPhoto didListPhotosSuccess:(NSArray *)photos
{
    _photos = [[NSArray alloc] initWithArray:[photos mutableCopy]];
    
    [self setupLocationPhotos];
    
}

- (void) wsPhoto:(WSPhoto *)wsPhoto didListPhotosFail:(NSError *)error
{
    
}

#pragma mark - PRIVATE FUNCTION

- (BOOL) isEmptyOverview
{
    NSString *text = [_location valueForKey:@"overview"];
    if ([text length] == 0) {
        return YES;
    }else{
        return NO;
    }
}

- (BOOL) isHotelCategory
{
    return [[_location valueForKey:@"category_id"] isEqualToString:CATEGORY_HOTEL_ID];
}

- (BOOL) shouldShowBookingButton
{
    if (_booking_info) { //Sure is hotel
        
        if ([self isReservable]) {
            return YES;
        }else{
            return NO;
        }
        
    }else{
        
        if ([self isHotelCategory]) {
            
            if ([self isReservable]) {
                return YES;
            }else{
                return NO;
            }
            
        }else{
            return NO;
        }
    }
    return YES;
}

- (BOOL) isReservable
{
    NSLog(@"LocationVC isReservationLocation %@", [_location valueForKey:@"reservable"]);
    if ([[_location valueForKey:@"reservable"] integerValue] != 0) {
        return YES;
    }else{
        return NO;
    }
}

- (BOOL)isBookmarked
{
    return [[SBase dataSource] isBookmarked:_location_id];
}

#pragma mark - BOOKMARK

- (void)saveBookmark
{
    if ([self isBookmarked]) {
        
    }else{
        [[SBase dataSource] addBookmark:_location_id];
        [_detailMainInfoCell.bt_bookmark setTitle:@"Saved" forState:UIControlStateNormal];
        _detailMainInfoCell.bt_bookmark.enabled = NO;
    }
}

#pragma mark - TABLEVIEW DATASOURCE

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case DETAIL_TABLE_SECTION_MAININF:
            return nil;
            break;
        case DETAIL_TABLE_SECTION_ADDINF:
            return @"Additional Info";
            break;
        case DETAIL_TABLE_SECTION_ADDACTION:
        {
            if ([self shouldShowBookingButton]) {
                return nil;
            }else{
                return nil;
            }
        }
            break;
        case DETAIL_TABLE_SECTION_OVERVIEW:{
            if ([self isEmptyOverview]) {
                return nil;
            }else{
                return @"Overview";
            }

        }
            break;
        case DETAIL_TABLE_SECTION_TIP:
            return @"Share information";
            break;
        default:
            return @"";
            break;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case DETAIL_TABLE_SECTION_MAININF:
            return 1;
            break;
        case DETAIL_TABLE_SECTION_ADDINF:{
            int count = 0;
            for (NSArray *additionalInfo in _additionalInfo) {
                if ([[additionalInfo objectAtIndex:1] isEqualToString:@""]) {
                    
                }else{
                    count++;
                }
            }
            return count;
        }
            break;
        case DETAIL_TABLE_SECTION_ADDACTION:
            if ([self shouldShowBookingButton]) {
                return 1;
            }else{
                return 0;
            }
            break;
        case DETAIL_TABLE_SECTION_OVERVIEW:
            return 1;
            break;
        case DETAIL_TABLE_SECTION_TIP:
            if ([[SAccount currentUser] isAdminUser]) {
                return 2;
            }else{
                return 1;
            }
            
            break;
        default:
            return 0;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int section = indexPath.section;
    switch (section) {
        case DETAIL_TABLE_SECTION_MAININF:
            return 314;
            break;
        case DETAIL_TABLE_SECTION_ADDINF:
            return 40;
            break;
        case DETAIL_TABLE_SECTION_ADDACTION:
        {
            if ([self shouldShowBookingButton]) {
                return 60;
            }else{
                return 0;
            }
        }
            break;
        case DETAIL_TABLE_SECTION_OVERVIEW:{
            if ([self isEmptyOverview]) {
                return 0;
            }else{
                return 208;
            }
        }
            break;
        case DETAIL_TABLE_SECTION_TIP:
            if ([[SAccount currentUser] isAdminUser]) {
                if (indexPath.row == 0) {
                    return 124;
                }else{
                    return 44;
                }
                
            }else{
                return 124;
            }
            break;
        default:
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int section = indexPath.section;
    switch (section) {
        case DETAIL_TABLE_SECTION_MAININF:
            return [self setDetailMainInfoCell:tableView];
            break;
        case DETAIL_TABLE_SECTION_ADDINF:
            return [self setDetailAdditionalInfoCell:tableView andIndexPath:indexPath];
            break;
        case DETAIL_TABLE_SECTION_ADDACTION:
            return [self setDetailAdditionalActionCell:tableView andIndexPath:indexPath];
            break;
        case DETAIL_TABLE_SECTION_OVERVIEW:
            return [self setDetailOverviewCell:tableView];
            break;
        case DETAIL_TABLE_SECTION_TIP:
            if ([[SAccount currentUser] isAdminUser]) {
                if (indexPath.row == 0) {
                    return [self setDetailTipCell:tableView];
                }else{
                    return [self setDeFaultCell:tableView];
                }
                
            }else{
                return [self setDetailTipCell:tableView];
            }
            break;
        default:
            return nil;
            break;
    }
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    int section = indexPath.section;
    int row     = indexPath.row;
    
    if (section == DETAIL_TABLE_SECTION_ADDINF) {
        NSLog(@"didSelectRowAtIndexPath %i", row);
        NSString *header = [_additionalInfo[row] objectAtIndex:0];
        NSString *content = [_additionalInfo[row] objectAtIndex:1];
        
        if ([self Contains:@"Option" on:header]) {
            DetailPopupVC *detailPopupVC = [[DetailPopupVC alloc] init];
            detailPopupVC.detailText = content;
            
            [self presentPopupViewController:detailPopupVC animationType:MJPopupViewAnimationFade];
        }
        
    }else{
        if (section == DETAIL_TABLE_SECTION_TIP) {
            if (indexPath.row == 1) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME message:@"Do you want to calibrate this Location GPS data?" delegate:self cancelButtonTitle:@"CANCEL" otherButtonTitles:@"OK", nil];
                
                [alert show];
            }
        }
    }
    
}

-(BOOL)Contains:(NSString *)StrSearchTerm on:(NSString *)StrText
{
    return [StrText rangeOfString:StrSearchTerm
                          options:NSCaseInsensitiveSearch].location == NSNotFound ? FALSE : TRUE;
}

#pragma mark - UIALERTVIEW DELEGATE

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        //Location
        if ([[LocationManager sharedSingleton].locationManager location]) {
            WSLocation *wsLocation = [[WSLocation alloc] init];
            
            NSArray *keys = @[@"id", @"latitude", @"longitude"];
            
            NSString *latitude = [NSString stringWithFormat:@"%f", [[LocationManager sharedSingleton].locationManager location].coordinate.latitude];
            NSString *longitude = [NSString stringWithFormat:@"%f", [[LocationManager sharedSingleton].locationManager location].coordinate.longitude];
            
            NSArray *objects = @[
                                 _location_id,
                                 latitude,
                                 longitude
                                 ];
            
            [_location setValue:[NSNumber numberWithDouble:[[LocationManager sharedSingleton].locationManager location].coordinate.latitude] forKey:@"latitude"];
            [_location setValue:[NSNumber numberWithDouble:[[LocationManager sharedSingleton].locationManager location].coordinate.longitude] forKey:@"longitude"];
            
            NSDictionary *locationInfo = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
            
            wsLocation.delegate = (id)self;
            [wsLocation updateLocation:locationInfo];
            
        }else{
            [SBase showAlert:@"Can not detect your current location"];
        }
    }
}

#pragma mark - WSLOCATION DELEGATES

- (void) wsLocation:(WSLocation *)wsLocation didGetLocationSuccess:(ILocation *)location
{
    if (_transporters == nil) {
        if ([location.tranporters count] > 0 ) {
            _transporters = location.tranporters;
        }
    }
    
    
    if (location.isRecommended) {
        [_detailMainInfoCell.bt_recommend setTitle:@"Done" forState:UIControlStateNormal];
        _detailMainInfoCell.bt_recommend.enabled = NO;
        
    }
    
    [self stopLoading];
    
    if (_wsLocationFlag == DETAIL_WSLOCATION_FLAG_RELOAD) {
        _wsLocationFlag = DETAIL_WSLOCATION_FLAG_NORMAL;
        
        [self _reloadData];
    }
}

- (void) wsLocation:(WSLocation *)wsLocation didGetLocationFail:(NSError *)error
{
    NSLog(@"LocationVC wsLocation didGetLocationFail %@", error);
    [self stopLoading];
}

#pragma mark - WSLOCATION DELEGATES - UPDATE BY CATEGORY

- (void)wsLocation:(WSLocation *)wsLocation didUpdatedByCategorySuccess:(NSArray *)locations
{
    NSLog(@"Calibrate location success");
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![managedObjectContext save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }

}

- (void)wsLocation:(WSLocation *)wsLocation didUpdateLocationFail:(NSError *)error
{
    
}

#pragma mark - WSLOCATION DELEGATE - ADD BOOKING

- (void) wsLocation:(WSLocation *)wsLocation didPlaceBookingSuccess:(NSDictionary *)info
{
    NSLog(@"LocationVC wsLocation didPlaceBookingSuccess");
    
    _detailAditionalActionCell.buttonBooking.titleLabel.text = @"You already book this";
    [_detailAditionalActionCell.buttonBooking setTitle:@"You already book this" forState:UIControlStateNormal];
    _detailAditionalActionCell.delegate = nil;
    
    [SBase showAlert:@"Your booking has been created, we will contact you soon for confirmation"];
}

- (void) wsLocation:(WSLocation *)wsLocation didPlaceBookingFail:(NSError *)error
{
    NSLog(@"LocationVC wsLocation didPlaceBookingFail");
    [SBase showAlert:@"Your booking has been fail to create, please try again later"];
    _booking_info = nil;
    [_tableView reloadData];
}

#pragma mark - PULL CODE
- (void)setupStrings{
    textPull    = @"Pull down to refresh";             //@"Pull down to refresh...";
    textRelease = @"Release to refresh";                   //@"Release to refresh...";
    textLoading = @"Now loading";                             //@"Loading...";
}

- (void)addPullToRefreshHeader {
    
    [self setupStrings];
    
    refreshHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 - REFRESH_HEADER_HEIGHT, 320, REFRESH_HEADER_HEIGHT)];
    refreshHeaderView.backgroundColor = [UIColor clearColor];
    
    refreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(110, 0, 320, REFRESH_HEADER_HEIGHT)];
    refreshLabel.backgroundColor = [UIColor clearColor];
    refreshLabel.font = [UIFont boldSystemFontOfSize:13.0];
    refreshLabel.textColor = [UIColor colorWithRed:197.0/255.0 green:187.0/255.0 blue:184.0/255.0 alpha:1.0];
    
    refreshArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pull_arrow.png"]];
    refreshArrow.frame = CGRectMake(floorf((REFRESH_HEADER_HEIGHT - 13) / 2 + 70),
                                    (floorf(REFRESH_HEADER_HEIGHT - 26) / 2),
                                    13, 26);
    
    refreshSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    refreshSpinner.frame = CGRectMake(floorf(floorf(REFRESH_HEADER_HEIGHT - 20) / 2) + 70 , floorf((REFRESH_HEADER_HEIGHT - 20) / 2), 20, 20);
    refreshSpinner.hidesWhenStopped = YES;
    
    [refreshHeaderView addSubview:refreshLabel];
    [refreshHeaderView addSubview:refreshArrow];
    [refreshHeaderView addSubview:refreshSpinner];
    [_tableView addSubview:refreshHeaderView];
}

- (void)startLoading {
    isLoading = YES;
    
    // Show the header
    [UIView animateWithDuration:0.3 animations:^{
        _tableView.contentInset = UIEdgeInsetsMake(REFRESH_HEADER_HEIGHT, 0, 0, 0);
        refreshLabel.text = textLoading;
        refreshArrow.hidden = YES;
        [refreshSpinner startAnimating];
    } completion:^(BOOL finished) {
        // call WS
        WSLocation *wsLocation = [[WSLocation alloc] init];
        wsLocation.delegate = (id)self;
        
        _wsLocationFlag = DETAIL_WSLOCATION_FLAG_RELOAD;
        [wsLocation getLocation:self.location_id];
        
    }];
}

- (void)stopLoading {
    isLoading = NO;
    
    // Hide the header
    [UIView animateWithDuration:0.3 animations:^{
        _tableView.contentInset = UIEdgeInsetsZero;
        [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
    }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(stopLoadingComplete)];
                     }];
}

- (void)stopLoadingComplete {
    // Reset the header
    refreshLabel.text = textPull;
    refreshArrow.hidden = NO;
    [refreshSpinner stopAnimating];
}

- (void)refresh {
    // This is just a demo. Override this method with your custom reload action.
    // Don't forget to call stopLoading at the end.
    [self performSelector:@selector(stopLoading) withObject:nil afterDelay:2.0];
}



#pragma mark - UISCROLLVIEW DELEGATE

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (scrollView == _tableView) {
        if (isLoading) return;
        isDragging = YES;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == _tableView) {
        if (isLoading) {
            // Update the content inset, good for section headers
            if (scrollView.contentOffset.y > 0)
                _tableView.contentInset = UIEdgeInsetsZero;
            else if (scrollView.contentOffset.y >= -REFRESH_HEADER_HEIGHT)
                _tableView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        } else if (isDragging && scrollView.contentOffset.y < 0) {
            // Update the arrow direction and label
            [UIView animateWithDuration:0.25 animations:^{
                if (scrollView.contentOffset.y < -REFRESH_HEADER_HEIGHT) {
                    // User is scrolling above the header
                    refreshLabel.text = textRelease;
                    [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
                } else {
                    // User is scrolling somewhere within the header
                    refreshLabel.text = textPull;
                    [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
                }
            }];
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView == _tableView) {
        if (!decelerate) {
            if (scrollView.contentOffset.y <= scrollView.contentSize.height - 1.0 * scrollView.frame.size.height)// &&
                //[_feeds count] < _total &&
                //!_isLoading)
            {
                //[self loadMoreFeeds];
            }
        }
        
        if (isLoading) return;
        isDragging = NO;
        if (scrollView.contentOffset.y <= -REFRESH_HEADER_HEIGHT) {
            // Released above the header
            [self startLoading];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView == _tableView) {
        if (scrollView.contentOffset.y <= scrollView.contentSize.height - 1.0 * scrollView.frame.size.height) //&&
            //[_feeds count] < _total  &&
            //!_isLoading)
        {
            //[self loadMoreFeeds];
        }
    }
}


#pragma mark - CELLS SETUP

- (DetailMainInfoCell *) setDetailMainInfoCell:(UITableView *)tableView
{
    NSString *CellId = @"DetailMainInfoCell";
    DetailMainInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (cell == nil) {
        cell = [[DetailMainInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellId];
    }
   
    cell.lbLocationName.text = [_location valueForKey:@"title"];
    cell.lbLocationAddress.text = [_location valueForKey:@"address"];
    
    if ([self isBookmarked] == YES) {
        [cell.bt_bookmark setTitle:@"Saved" forState:UIControlStateNormal];
        cell.bt_bookmark.enabled = NO;
    }
    
    NSString *icon_name = [[SBase appDelegate].sharedCategoriesIcon objectForKey:[_location valueForKey:@"category_id"]];
    cell.imageLocationIcon.image = [UIImage imageNamed:icon_name];
    
    cell.delegate = (id) self;
    
    [cell.bt_recommend setTitle:@"Like ?" forState:UIControlStateNormal];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    _detailMainInfoCell = cell;
    return cell;
}

- (DetailAdditionalActionCell *) setDetailAdditionalActionCell:(UITableView *)tableView andIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellId = @"DetailAdditionalActionCell";
    DetailAdditionalActionCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (cell == nil) {
        cell = [[DetailAdditionalActionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellId];
    }
    
    _detailAditionalActionCell = cell;
    
    if (_alreadyBooked) {
        cell.buttonBooking.titleLabel.text = @"You already book this";
        [cell.buttonBooking setTitle:@"You already book this" forState:UIControlStateNormal];
        cell.delegate = nil;
    }else{
        cell.delegate = (id) self;
    }
    
    
    return cell;

}

- (DetailAdditionalInfoCell *) setDetailAdditionalInfoCell:(UITableView *)tableView andIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellId = @"DetailAdditionalInfoCell";
    DetailAdditionalInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (cell == nil) {
        cell = [[DetailAdditionalInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellId];
    }
    
    if([[_additionalInfo[indexPath.row] objectAtIndex:1] isEqualToString:@""]){
        return cell;
    }else{
        cell.cellTitle.text = [_additionalInfo[indexPath.row] objectAtIndex:0];
        cell.cellSubTitle.text = [_additionalInfo[indexPath.row] objectAtIndex:1];
        
        [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
        
        return cell;
    }
    
    
}

- (DetailOverviewCell *) setDetailOverviewCell:(UITableView *)tableView
{
    NSString *CellId = @"DetailOverviewCell";
    DetailOverviewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (cell == nil) {
        cell = [[DetailOverviewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellId];
    }
    
    cell.delegate = (id)self;
    cell.cellTitle.text = [_location valueForKey:@"overview"];
    
    return cell;
}

- (DetailTipCell *) setDetailTipCell:(UITableView *)tableView
{
    NSString *CellId = @"DetailTipCell";
    DetailTipCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (cell == nil) {
        cell = [[DetailTipCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellId];
    }
    
    cell.delegate = (id)self;
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

- (UITableViewCell *) setDeFaultCell:(UITableView *)tableView
{
    NSString *CellId = @"DefaultCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellId];
    }
    
    //cell.delegate = (id)self;
    cell.textLabel.text = @"Calibrate this Location";
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

#pragma mark - CELLS DELEGATES

- (void) detailAdditionalActionCell:(DetailAdditionalActionCell *)detailAdditionalActionCell didSelectBooking:(NSDictionary *)info
{
    NSLog(@"LocationVC detailAdditionalActionCell didSelectBooking");
    if ([SAccount currentUser].isLoggedIn ) {
        
        if (_bookingPopupVC == nil) {
            _bookingPopupVC = [[BookingPopupVC alloc] init];
            _bookingPopupVC.location_id = _location_id;
            _bookingPopupVC.delegate = (id)self;
                    }
        [self presentPopupViewController:_bookingPopupVC animationType:MJPopupViewAnimationFade];
        
        /*
        WSLocation *wsLocation = [[WSLocation alloc] init];
        wsLocation.delegate = (id)self;
        
        [wsLocation placeBooking:[_booking_info objectForKey:@"checkin"]
                     andCheckout:[_booking_info objectForKey:@"checkout"]
                     andQuantity:[_booking_info objectForKey:@"quantity"]
                   forLocationId:_location_id];
        */
    }else{
        [self performRegisterOrLogin];
    }
    
    
    
}

- (void) detailMainInfoCell:(DetailMainInfoCell *)detailMainInfoCell performRecomment:(NSDictionary *)recommentInfo
{
    if ([SAccount currentUser].isLoggedIn ) {
        UIView *recommentConfirm = [[[NSBundle mainBundle] loadNibNamed:@"DetailRecommentConfirm" owner:self options:nil] firstObject];
        recommentConfirm.frame = CGRectMake(0, 187, 320, 75);
        [detailMainInfoCell addSubview:recommentConfirm];
        [recommentConfirm didMoveToSuperview];
        
        _recommentConfirmView = recommentConfirm;
        
    }else{
        RegisterVC *registerVC = [[RegisterVC alloc] init];
        registerVC.delegate = (id)[SBase appDelegate].mainVC;
        
        [[SBase appDelegate].mainVC presentViewController:[[UINavigationController alloc] initWithRootViewController:registerVC] animated:YES completion:^{
            
        }];
        
    }
    
    
}

- (void) detailMainInfoCell:(DetailMainInfoCell *)detailMainInfoCell performBookmark:(NSDictionary *)info
{
    [self saveBookmark];
}

- (void) detailMainInfoCell:(DetailMainInfoCell *)detailMainInfoCell performDirection:(NSDictionary *)info
{
    if (_directionPopupVC == nil) {
        _directionPopupVC = [[DirectionPopupVC alloc] init];
        _directionPopupVC.address = [_location valueForKey:@"address"];
        _directionPopupVC.transporters = _transporters;
        _directionPopupVC.delegate = (id)self;
    }
    
    [self presentPopupViewController:_directionPopupVC animationType:MJPopupViewAnimationFade];
    
    
}

- (void) detailMainInfoCell:(DetailMainInfoCell *)detailMainInfoCell performContact:(NSDictionary *)info
{
    if (_contactPopupVC == nil) {
        _contactPopupVC = [[ContactPopupVC alloc] init];
        _contactPopupVC.contactsInfo = [_contactInfo mutableCopy];
        _contactPopupVC.delegate = (id)self;
    }
    [self presentPopupViewController:_contactPopupVC animationType:MJPopupViewAnimationFade];
}

- (void) detailTipCell:(DetailTipCell *)detailTipCell addPhoto:(NSDictionary *)info
{
    if ([SAccount currentUser].isLoggedIn ) {
        AddPhotosVc *addPhoto = [[AddPhotosVc alloc] init];
        addPhoto.location_id = _location_id;
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:addPhoto] animated:YES completion:^{
            
        }];
    }else{
        [self performRegisterOrLogin];
    }
}

- (void) detailTipCell:(DetailTipCell *)detailTipCell addTip:(NSDictionary *)info
{
    if ([SAccount currentUser].isLoggedIn ) {
        AddTipVC *addTip = [[AddTipVC alloc] init];
        addTip.location_id = _location_id;
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:addTip] animated:YES completion:^{
            
        }];
    }else{
        [self performRegisterOrLogin];
    }
    
}

- (void) detailOverviewCell:(DetailOverviewCell *)detailOverviewCell continueReading:(NSDictionary *)info
{
    if ([[_location valueForKey:@"wysiwyg_content"] isEqualToString:@""]) {
        [SBase showAlert:@"This content is being update"];
    }else{
        DetailOverviewVC *detailOverviewVC = [[DetailOverviewVC alloc] init];
        detailOverviewVC.location_id = _location_id;
        
        [self.navigationController pushViewController:detailOverviewVC animated:YES];
    }
    
}

#pragma mark - CALLING REGISTER OR LOGIN

- (void) performRegisterOrLogin
{
    RegisterVC *registerVC = [[RegisterVC alloc] init];
    registerVC.delegate = (id)[SBase appDelegate].mainVC;
    
    [[SBase appDelegate].mainVC presentViewController:[[UINavigationController alloc] initWithRootViewController:registerVC] animated:YES completion:^{
        
    }];

}



#pragma mark - ADD RECOMMEND DELEGATE

- (void) addRecommend:(AddRecommentVC *)addRecommend didAddRecommendSuccess:(NSDictionary *)recommendInfo
{
    [self dismissViewControllerAnimated:YES completion:^{
        [SBase showAlert:@"Thank you for added recomment for this place"];
        [_detailMainInfoCell.bt_recommend setTitle:@"Done" forState:UIControlStateNormal];
        _detailMainInfoCell.bt_recommend.enabled = NO;

    }];
}

#pragma mark - DIRECTION POPUP DELEGATE

- (void) popupDirection:(DirectionPopupVC *)popupDirection showMap:(NSDictionary *)info
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    NSLog(@"LocationVC popupDirection showMap");
    if (_flag_from == DETAIL_FLAG_FROM_MAP) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
        MapVC *mapVC = [[MapVC alloc] init];
        mapVC.type = MAP_TYPE_SINGLE;
        mapVC.map_location_id = _location_id;
        
        [self.navigationController pushViewController:mapVC animated:YES];
    }
}

- (void) popupDirection:(DirectionPopupVC *)popupDirection callTransporters:(NSDictionary *)info
{
    NSLog(@"popupDirection %@",[(ITransporter *)info descriptions]);
    NSString *phoneNumber = [@"telprompt://" stringByAppendingString:[(ITransporter *)info descriptions]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}



#pragma mark - CONTACT POPUP DELEGATE

- (void)popupContact:(ContactPopupVC *)popupContact callContact:(NSArray *)contactInfo
{
    NSString *contactType = [contactInfo objectAtIndex:0];
    if ([contactType isEqualToString:LOCATIONPOPUP_CONTACT_TYPE_URL]) {
        [self performContactURL:[contactInfo objectAtIndex:LOCATIONPOPUP_CONTACT_VALUE_INDEX]];
    }else if ([contactType isEqualToString:LOCATIONPOPUP_CONTACT_TYPE_PHONE]){
        [self performContactPhone:[contactInfo objectAtIndex:LOCATIONPOPUP_CONTACT_VALUE_INDEX]];
    }else if ([contactType isEqualToString:LOCATIONPOPUP_CONTACT_TYPE_Email]){
        [self performContactEmail:[contactInfo objectAtIndex:LOCATIONPOPUP_CONTACT_VALUE_INDEX]];
    }else{
        
    }

}

- (void) performContactPhone:(NSString *)phone
{
    NSString *phoneNumber = [@"telprompt://" stringByAppendingString:phone];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

- (void) performContactURL:(NSString *)url
{
    NSString *weburl = [@"http://" stringByAppendingString:url];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:weburl]];
}

- (void) performContactEmail:(NSString *)email
{
    if ([MFMailComposeViewController canSendMail]) {
        
        if (_mailComposeViewController) {
            _mailComposeViewController = nil;
        }
        
        _mailComposeViewController = [[MFMailComposeViewController alloc] init];
        _mailComposeViewController.mailComposeDelegate = (id)self;
        
        NSString *messageBody = [NSString stringWithFormat:@"Hi, Would you mind let me know how to make reserved of this place ?"];
        
        
        [_mailComposeViewController setSubject:@"Subject"];
        [_mailComposeViewController setMessageBody:messageBody isHTML:NO];
        [_mailComposeViewController setSubject:[NSString stringWithFormat:@"Contact Email"]];
        [_mailComposeViewController setToRecipients:@[email]];
        
        [self.navigationController presentViewController:_mailComposeViewController animated:YES completion:^{
            
        }];
    }else{
        [SBase showAlert:@"Please config your email"];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    if (error) {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"はい" otherButtonTitles:nil] show];
    }
    
    [controller dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark - BOOKING POPUP DELEGATE

- (void) bookingPopupVC:(BookingPopupVC *)bookingPopupVc didCheckAvailabilitySuccessWithAvailable:(NSDictionary *)data
{
    NSLog(@"LocationVC bookingPopupVC didCheckAvailabilitySuccessWithAvailable data %@", data);
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
    WSLocation *wsLocation = [[WSLocation alloc] init];
    wsLocation.delegate = (id)self;
    
    [wsLocation placeBooking:[data objectForKey:@"checkin"]
                 andCheckout:[data objectForKey:@"checkout"]
                 andQuantity:[data objectForKey:@"quantity"]
               forLocationId:_location_id];
    
    
}

- (void) bookingPopupVC:(BookingPopupVC *)bookingPopupVc didCheckAvailabilitySuccessWithNoAvailable:(NSDictionary *)data
{
    NSLog(@"LocationVC bookingPopupVC didCheckAvailabilitySuccessWithNoAvailable data %@", data);
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationFade];
    
    [SBase showAlert:@"Your booking has been fail to create, please try again later"];
}

#pragma mark - IBACTIONS

- (IBAction)detailScrollAddPhoto:(id)sender {
    if ([SAccount currentUser].isLoggedIn ) {
        AddPhotosVc *addPhoto = [[AddPhotosVc alloc] init];
        addPhoto.location_id = _location_id;
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:addPhoto] animated:YES completion:^{
            
        }];

    }else{
        RegisterVC *registerVC = [[RegisterVC alloc] init];
        registerVC.delegate = (id)[SBase appDelegate].mainVC;
        
        [[SBase appDelegate].mainVC presentViewController:[[UINavigationController alloc] initWithRootViewController:registerVC] animated:YES completion:^{
            
        }];
        
    }
}

- (IBAction)detailScrollAddTip:(id)sender {
    if ([SAccount currentUser].isLoggedIn ) {
        AddTipVC *addTip = [[AddTipVC alloc] init];
        addTip.location_id = _location_id;
        [self presentViewController:[[UINavigationController alloc] initWithRootViewController:addTip] animated:YES completion:^{
            
        }];
        
    }else{
        RegisterVC *registerVC = [[RegisterVC alloc] init];
        registerVC.delegate = (id)[SBase appDelegate].mainVC;
        
        [[SBase appDelegate].mainVC presentViewController:[[UINavigationController alloc] initWithRootViewController:registerVC] animated:YES completion:^{
            
        }];
        
    }
    
}

- (IBAction)recommentConfirmCancel:(id)sender {
    [_recommentConfirmView removeFromSuperview];
}

- (IBAction)recommentConfirmOk:(id)sender {
   [_recommentConfirmView removeFromSuperview];
    
    AddRecommentVC *addRecomment = [[AddRecommentVC alloc] init];
    addRecomment.delegate = (id)self;
    addRecomment.location_id = _location_id;
    
    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:addRecomment] animated:YES completion:^{
        
    }];
}
@end
