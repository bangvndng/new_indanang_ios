//
//  BookmarkVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/17/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "BookmarkVC.h"

#import "LocationVC.h"
#import "BookmarkCell.h"
#import "Bookmark.h"
#import "SBase.h"

#import "LocationCategory.h"

@interface BookmarkVC ()<UITableViewDelegate, UITableViewDataSource>{
    
    __weak IBOutlet UITableView *_tableView;
    

    NSMutableDictionary *_bookmarkCategories;
    NSMutableDictionary *_categories;
    
    //NSMutableArray *_listObjects;
    NSMutableArray *_listTypes;
    
    BOOL _editing;
    
}

@end

@implementation BookmarkVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIBarButtonItem *barButtonEdit = [[UIBarButtonItem alloc] initWithTitle:@"Edit"
                                                                      style:UIBarButtonItemStyleBordered
                                                                     target:self
                                                                     action:@selector(performEdit:)];
    self.navigationItem.rightBarButtonItem = barButtonEdit;
    
    _bookmarkCategories = [[NSMutableDictionary alloc] init];
    
    //_listObjects = [[NSMutableArray alloc] init];
    _listTypes = [[NSMutableArray alloc] init];
    
    [self registerNibsForTableView];
    
    [self loadCategories];
    [self loadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) registerNibsForTableView
{
    NSArray *cells = @[@"BookmarkCell"];
    
    for (NSString *cellClass in cells) {
        [_tableView registerNib:[UINib  nibWithNibName:cellClass
                                                bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:cellClass];
    }
}

#pragma mark - Load Data

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

- (void) loadCategories
{
    _categories = [[NSMutableDictionary alloc] init];
    
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"LocationCategory"];
    NSArray *appCategories = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    for (LocationCategory *category in appCategories) {
        [_categories setObject:category forKey:[category valueForKey:@"category_id"]];
    }
    
}

- (void) refineData
{
    
}

- (void) loadData
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Bookmark"];
    NSArray *bookmarksItems = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    _bookmarkCategories = [[NSMutableDictionary alloc] init];
    
    for (Bookmark *bookmark in bookmarksItems) {
        NSString *category_id = [bookmark valueForKey:@"category_id"];
        [self insertBookmark:category_id andBookmark:bookmark];
    }
    
    _listTypes = [[_bookmarkCategories allKeys] mutableCopy];
    
}

- (void) insertBookmark:(NSString *)category_id andBookmark:(Bookmark *)bookmark
{
    if ([self existedInBookmarkCategories:category_id]) {
        // Load the mutable array and perform insert
        NSMutableArray *bookmarks = [_bookmarkCategories objectForKey:category_id];
        [bookmarks addObject:bookmark];
        
        [_bookmarkCategories setObject:bookmarks forKey:category_id];
    }else{
        // Create new mutable array
        NSMutableArray *bookmarks = [[NSMutableArray alloc] init];
        
        // Perform insert
        [bookmarks addObject:bookmark];
        
        // Perform add key
        [_bookmarkCategories setObject:bookmarks forKey:category_id];
    }
}

- (BOOL) existedInBookmarkCategories:(NSString *)category_id
{
    return !([_bookmarkCategories objectForKey:category_id] == nil);
}

#pragma mark - TABLEVIEW DATASOURCE

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if ([tableView.dataSource tableView:tableView numberOfRowsInSection:section] == 0) {
        return 0;
    } else {
        // whatever height you'd want for a real section header
        return 30;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [[_categories objectForKey:[NSString stringWithFormat:@"%@", _listTypes[section]]] valueForKey:@"name"];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [_listTypes count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *category_id = [NSString stringWithFormat:@"%@", _listTypes[section]];
    return [[_bookmarkCategories objectForKey:category_id] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int section = indexPath.section;
    int row = indexPath.row;
    
    NSString *category_id = [NSString stringWithFormat:@"%@",[_listTypes objectAtIndex:section]];
    
    //Book mark item
    
    
    NSString *CellId = @"BookmarkCell";
    BookmarkCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (cell == nil) {
        cell = [[BookmarkCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellId];
    }
    
    cell.cellTitle.text = [[[_bookmarkCategories objectForKey:category_id] objectAtIndex:row] valueForKey:@"title"];
    
    if ([[[_bookmarkCategories objectForKey:category_id] objectAtIndex:row] valueForKey:@"recommended"]) {
        cell.cellRecommended.text = @"YES";
        cell.cellRecommended.alpha = 1;
    }else{
        cell.cellRecommended.text = @"NO";
        cell.cellRecommended.alpha = 0;
    }
    
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    int section = indexPath.section;
    int row = indexPath.row;
    
    NSString *category_id = [NSString stringWithFormat:@"%@",[_listTypes objectAtIndex:section]];
    
    LocationVC *location = [[LocationVC alloc] init];
    location.location_id = [[[_bookmarkCategories objectForKey:category_id] objectAtIndex:row] valueForKey:@"location_id"];

    [self.navigationController pushViewController:location animated:YES];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        // Commit the delete
        int section = indexPath.section;
        int row = indexPath.row;
        
        NSString *category_id = [NSString stringWithFormat:@"%@",[_listTypes objectAtIndex:section]];

        
        NSManagedObjectContext *context = [self managedObjectContext];
        [context deleteObject:[[_bookmarkCategories objectForKey:category_id] objectAtIndex:row]];
        
        NSError *error = nil;
        if (![context save:&error]) {
            NSLog(@"Can't Delete! %@ %@", error, [error localizedDescription]);
            return;
        }
        
        
        [[_bookmarkCategories objectForKey:category_id] removeObjectAtIndex:row];
        [_tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark - Actions

- (void) performEdit:(id)sender
{
    if (!_editing || _editing == NO) {
        [_tableView setEditing:YES];
        _editing = YES;
    }else{
        _editing = NO;
        [_tableView setEditing:NO];
    }
    
}

@end
