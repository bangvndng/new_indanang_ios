//
//  DetailMainInfoCell.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/22/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "DetailMainInfoCell.h"

@implementation DetailMainInfoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)recomment:(id)sender {
    NSLog(@"DetailMainInfoCell recomment");
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(detailMainInfoCell:performRecomment:)]) {
        [self.delegate detailMainInfoCell:self performRecomment:nil];
    }
}

- (IBAction)bookmark:(id)sender {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(detailMainInfoCell:performBookmark:)]) {
        [self.delegate detailMainInfoCell:self performBookmark:nil];
    }
}

- (IBAction)direction:(id)sender {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(detailMainInfoCell:performDirection:)]) {
        [self.delegate detailMainInfoCell:self performDirection:nil];
    }
}

- (IBAction)contact:(id)sender {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(detailMainInfoCell:performContact:)]) {
        [self.delegate detailMainInfoCell:self performContact:nil];
    }
}
@end
