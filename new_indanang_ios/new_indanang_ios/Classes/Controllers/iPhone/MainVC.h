//
//  MainVC.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/12/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CenterVC.h"
#import "RightVC.h"

@interface MainVC : UIViewController

@property (strong, nonatomic) CenterVC *centerVC;
@property (strong, nonatomic) RightVC *rightVC;

- (void)_hideRightView;

@end
