//
//  BookingCell.h
//  new_indanang_ios
//
//  Created by Vu Ngoc Bang on 3/21/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookingCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cellTitle;
@property (weak, nonatomic) IBOutlet UILabel *cellAddress;
@property (weak, nonatomic) IBOutlet UILabel *cellCheckin;
@property (weak, nonatomic) IBOutlet UILabel *cellCheckout;
@property (weak, nonatomic) IBOutlet UILabel *cellStatus;

@end