//
//  SelectCategoriesListVC.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/31/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICategory.h"

@protocol SelectCategoriesListVCDelegate;

@interface SelectCategoriesListVC : UIViewController

@property (weak, nonatomic) id<SelectCategoriesListVCDelegate> delegate;

@end

@protocol SelectCategoriesListVCDelegate <NSObject>

- (void) selectCategory:(SelectCategoriesListVC *)selectCategory didSelect:(ICategory *)category;

@end