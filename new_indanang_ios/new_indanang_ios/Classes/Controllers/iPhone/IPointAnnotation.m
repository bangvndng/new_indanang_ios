//
//  IPointAnnotation.m
//  indanang
//
//  Created by Bang Ngoc Vu on 06/10/2013.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "IPointAnnotation.h"

@implementation IPointAnnotation

- (id)initWithCoordinate:(CLLocationCoordinate2D)coord title:(NSString *)title subtitle:(NSString *)subtitle type:(NSString *)type
{
    self = [super init];
    if (self) {
        self.coordinate = coord;
        self.title = title;
        self.subtitle = subtitle;
        self.type = type;
    }
    
    return self;
}

@end
