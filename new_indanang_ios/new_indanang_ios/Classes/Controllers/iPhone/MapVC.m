//
//  MapVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/17/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "MapVC.h"
#import <MapKit/MapKit.h>
#import "IPointAnnotation.h"
#import "IAnnotationView.h"

#import "LocationVC.h"

#import "Define.h"
#import "SBase.h"
#import "LocationManager.h"

@interface MapVC ()<MKMapViewDelegate, IAnnotationViewProtocol>{
    
    __weak IBOutlet MKMapView *_mapView;
    NSMutableArray *_locations;
    
}

@end

@implementation MapVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    [self loadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    NSLog(@"MapVC didReceivedMemoryWarning");
    
}

#pragma mark - Load data

- (void) loadData
{
    
    switch (self.type) {
        case MAP_TYPE_DEFAULT:
            [self _loadAllLocations];
            break;
        case MAP_TYPE_SINGLE:
            [self _loadLocation];
            break;
        case MAP_TYPE_CATEGORY_DETAIL:
            [self _loadCategory];
            break;
        case MAP_TYPE_SUGGESTION:
            [self _loadSuggestion];
            break;
        case MAP_TYPE_CATEGORY_FILTER:
            [self _loadFilteredLocations];
            break;
        default:
            break;
    }
    


}

- (void) _loadFilteredLocations{
    _locations = [_filtered_locations mutableCopy];
    
    for (id location in _locations) {
        
        ILocation *iLocation = [[ILocation alloc] init];
        iLocation.location_id = [location valueForKey:@"location_id"];
        
        IPointAnnotation *point = [[IPointAnnotation alloc] init];
        point.title = [location valueForKey:@"title"];
        point.subtitle = [location valueForKey:@"address"];
        point.coordinate = CLLocationCoordinate2DMake(
                                                      [[location valueForKey:@"latitude"] doubleValue],
                                                      [[location valueForKey:@"longitude"] doubleValue]
                                                      );
        point.type = [location valueForKey:@"category_id"];
        point.locationitem = iLocation;
        [_mapView addAnnotation:point];
        
    }
    
    [self _zoomToUserLocation];
}

- (void) _loadAllLocations
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Location"];
    _locations = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    for (id location in _locations) {
        
        ILocation *iLocation = [[ILocation alloc] init];
        iLocation.location_id = [location valueForKey:@"location_id"];
        
        IPointAnnotation *point = [[IPointAnnotation alloc] init];
        point.title = [location valueForKey:@"title"];
        point.subtitle = [location valueForKey:@"address"];
        point.coordinate = CLLocationCoordinate2DMake(
                                                      [[location valueForKey:@"latitude"] doubleValue],
                                                      [[location valueForKey:@"longitude"] doubleValue]
                                                      );
        point.type = [location valueForKey:@"category_id"];
        point.locationitem = iLocation;
        [_mapView addAnnotation:point];
        
    }
    
    [self _zoomToUserLocation];
}

- (void) _loadLocation
{
    ILocation *iLocation = [[SBase dataSource] loadLocationById:_map_location_id];
    IPointAnnotation *point = [[IPointAnnotation alloc] init];
    point.title = iLocation.title;
    point.subtitle = iLocation.address;
    point.coordinate = CLLocationCoordinate2DMake(iLocation.latitude,iLocation.longitude);
    point.type = iLocation.category_id;
    point.locationitem = iLocation;
    [_mapView addAnnotation:point];
    
    CLLocation *userlocation = [[LocationManager sharedSingleton] locationManager].location;
    
    if (userlocation) {
        
        CLLocationCoordinate2D min = userlocation.coordinate;
        CLLocationCoordinate2D max = CLLocationCoordinate2DMake(iLocation.latitude ,iLocation.longitude);
        
        
        //build an array of points however you want
        CLLocationCoordinate2D points[2] = {min, max};
        
        //the magic part
        MKPolygon *poly = [MKPolygon polygonWithCoordinates:points count:2];
        
        NSLog(@"MapVC _loadLocation setting region");
        MKCoordinateRegion region = MKCoordinateRegionForMapRect([poly boundingMapRect]);
        region.span = MKCoordinateSpanMake(region.span.latitudeDelta + 0.01, region.span.longitudeDelta+ 0.01);
        [_mapView setRegion:region];
    }else{
        NSLog(@"MapVC _loadLocation can not locate user location");
    }

}

- (void) _loadCategory
{
    NSArray *locations = [[SBase dataSource] loadLocationsByCategoryId:_map_category_id];
    NSLog(@"MapVC _loadCategory locations count %i", [locations count]);
    for (ILocation *iLocation in locations) {
        //ILocation *iLocation = [[SBase dataSource] loadLocationById:_map_location_id];
        IPointAnnotation *point = [[IPointAnnotation alloc] init];
        point.title = iLocation.title;
        point.subtitle = iLocation.address;
        point.coordinate = CLLocationCoordinate2DMake(iLocation.latitude,iLocation.longitude);
        point.type = iLocation.category_id;
        point.locationitem = iLocation;
        [_mapView addAnnotation:point];
    }
    
    [self _zoomToUserLocation];
}

- (void) _loadCategories
{
    
}

- (void) _loadSuggestion
{
    NSArray *locations = [[SBase dataSource] loadLocationsBySuggestion];
    NSLog(@"MapVC _loadCategory locations count %i", [locations count]);
    for (ILocation *iLocation in locations) {
        //ILocation *iLocation = [[SBase dataSource] loadLocationById:_map_location_id];
        IPointAnnotation *point = [[IPointAnnotation alloc] init];
        point.title = iLocation.title;
        point.subtitle = iLocation.address;
        point.coordinate = CLLocationCoordinate2DMake(iLocation.latitude,iLocation.longitude);
        point.type = iLocation.category_id;
        point.locationitem = iLocation;
        [_mapView addAnnotation:point];
    }
    
    [self _zoomToUserLocation];
}

- (void)_zoomToUserLocation
{
    CLLocation *userlocation = [[LocationManager sharedSingleton] locationManager].location;
    if (userlocation) {
        MKCoordinateRegion region;
        MKCoordinateSpan span;
        span.latitudeDelta = 0.05;
        span.longitudeDelta = 0.05;
        CLLocationCoordinate2D location;
        location.latitude = userlocation.coordinate.latitude;
        location.longitude = userlocation.coordinate.longitude;
        region.span = span;
        region.center = location;
        [_mapView setRegion:region animated:YES];
    }else{
        NSLog(@"MapVC _loadLocation can not locate user location");
    }
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

#pragma mark - Mapview Delegate

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation {
    NSLog(@"MapVC didUpdateUserLocation");
}

- (IAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(IPointAnnotation *)annotation
{
    if ([annotation isKindOfClass:[IPointAnnotation class]]) {
        static NSString *annotationId = @"IPointAnnotation";
        
        IAnnotationView *idnAnnotationView = [[IAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationId];
        idnAnnotationView.idnAnnotationProtocol = (id)self;
        
        return idnAnnotationView;
    }
    
    return nil;
}

- (void)showDetailsLocation:(ILocation *)locationitem
{
    switch (self.type) {
        case MAP_TYPE_DEFAULT:{
            LocationVC *locationVC = [[LocationVC alloc] init];
            locationVC.location_id = locationitem.location_id;
            locationVC.flag_from = DETAIL_FLAG_FROM_MAP;
            [self.navigationController pushViewController:locationVC animated:YES];
        }
            
            break;
        case MAP_TYPE_SINGLE:{
            [self.navigationController popViewControllerAnimated:YES];
        }
            
            break;
        case MAP_TYPE_CATEGORY_DETAIL:{
            LocationVC *locationVC = [[LocationVC alloc] init];
            locationVC.location_id = locationitem.location_id;
            locationVC.flag_from = DETAIL_FLAG_FROM_MAP;
            [self.navigationController pushViewController:locationVC animated:YES];
        }
            
            break;
        case MAP_TYPE_SUGGESTION:{
            LocationVC *locationVC = [[LocationVC alloc] init];
            locationVC.location_id = locationitem.location_id;
            locationVC.flag_from = DETAIL_FLAG_FROM_MAP;
            [self.navigationController pushViewController:locationVC animated:YES];
        }
            
            break;
        default:{
            LocationVC *locationVC = [[LocationVC alloc] init];
            locationVC.location_id = locationitem.location_id;
            locationVC.flag_from = DETAIL_FLAG_FROM_MAP;
            [self.navigationController pushViewController:locationVC animated:YES];
        }
            break;
    }
    
}

@end
