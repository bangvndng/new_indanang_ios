//
//  AddTipVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/22/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "AddTipVC.h"
#import "SBase.h"
#import "WSTip.h"
@interface AddTipVC ()<WSTipDelegate, UITextViewDelegate>{
    
    __weak IBOutlet UITextView *textview_content;
    
    __weak IBOutlet UISwitch *_switchUpFB;
    __weak IBOutlet UIView *_viewUPFB;
}


@end

@implementation AddTipVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *barButtonCancel = [[UIBarButtonItem alloc] initWithTitle:@"Cancel"
                                                                        style:UIBarButtonItemStyleBordered
                                                                       target:self
                                                                       action:@selector(performCancel:)];
    self.navigationItem.leftBarButtonItem = barButtonCancel;
    
    UIBarButtonItem *barButtonNext = [[UIBarButtonItem alloc] initWithTitle:@"Next"
                                                                      style:UIBarButtonItemStyleBordered
                                                                     target:self
                                                                     action:@selector(performNext:)];
    self.navigationItem.rightBarButtonItem = barButtonNext;
    
    if ([SAccount currentUser].isFBUser) {
        //[FBRequest requestForPostStatusUpdate:@"Hello, I'm just testing"];
    }else{
        _viewUPFB.alpha = 0;
    }
    
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [textview_content becomeFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)performNext: (id)sender
{
    NSLog(@"AddTipVC performNext");
    
    if ([textview_content.text isEqualToString:@""]) {
        
        [SBase showAlert:@"Please complete the text field"];
        
    }else{
        
        NSArray *keys = @[@"location_id", @"content"];
        NSArray *objects = @[_location_id, textview_content.text];
        
        NSDictionary *tipInfo = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
        
        WSTip *wsTip = [[WSTip alloc] init];
        wsTip.delegate = (id)self;
        
        [wsTip addTip:tipInfo];
    }

}

- (void)performCancel: (id)sender
{
    NSLog(@"AddTipVC perfornCancel");
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    textview_content.text = @"";
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
}

#pragma mark - WSTIP Delegate 

- (void)wsTip:(WSTip *)wsTip didAddTipSuccess:(NSDictionary *)info
{
    if (_switchUpFB.on == YES) {
        [self performPublishAction:^{
            NSString *message = [NSString stringWithFormat:@"Updating status for %@ at %@", [SAccount currentUser].displayname, [NSDate date]];
            
            FBRequestConnection *connection = [[FBRequestConnection alloc] init];
            
            connection.errorBehavior = FBRequestConnectionErrorBehaviorReconnectSession
            | FBRequestConnectionErrorBehaviorAlertUser
            | FBRequestConnectionErrorBehaviorRetry;
            
            [connection addRequest:[FBRequest requestForPostStatusUpdate:message]
                 completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                     NSLog(@"AddTipVC Post Tip to Facebook success");
                     [SBase showAlert:@"Your Tip has been successfully post to your Facebook"];
                 }];
            [connection start];
        }];
    }
    
    
    
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        [SBase showAlert:@"Thank you for added tips for this place"];
    }];
}

- (void)wsTip:(WSTip *)wsTip didAddTipFail:(NSError *)error
{
    [SBase showAlert:@"Thank you for added tips for this place, please try again later"];
}

#pragma mark - POST FACEBOOK

// Convenience method to perform some action that requires the "publish_actions" permissions.
- (void) performPublishAction:(void (^)(void)) action {
    // we defer request for permission to post to the moment of post, then we check for the permission
    //if ([FBSession.activeSession.permissions indexOfObject:@"publish_actions"] == NSNotFound) {
        [FBSession openActiveSessionWithPublishPermissions:@[@"publish_actions"] defaultAudience:FBSessionDefaultAudienceFriends allowLoginUI:YES completionHandler:^(FBSession *session,FBSessionState state, NSError *error) {
            if (!error) {
                action();
            } else if (error.fberrorCategory != FBErrorCategoryUserCancelled){
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Permission denied"
                                                                    message:@"Unable to get permission to post"
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                [alertView show];
            }
        }];
        
//    } else {
//        NSLog(@"AddTipVC found publish_actions");
//        action();
//    }
    
}

@end
