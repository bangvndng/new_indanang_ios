//
//  RightSmallCell.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/15/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "RightSmallCell.h"

@implementation RightSmallCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
