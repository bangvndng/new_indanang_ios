//
//  BookingPopupVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 3/22/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import "BookingPopupVC.h"

#import "Define.h"
#import "SBase.h"

#import "WSBooking.h"

@interface BookingPopupVC ()<UITextFieldDelegate, UIActionSheetDelegate, WSBookingDelegate>{
    /* UIDATEPICKER */
    UIDatePicker    *_theDatePicker;
    UIToolbar       *_pickerToolbar;
    UIActionSheet   *_pickerViewDate;
    
    UITextField     *_selectedTextField;
    
    NSString *_checkinDateVal;
    NSString *_checkoutDateVal;
    NSString *_quantityVal;
}

@end

@implementation BookingPopupVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITEXTFIELD DELEGATE



- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField.tag == 1) {
        _quantityVal = textField.text;
        [_step_quantity setValue:[textField.text doubleValue]];
    }else if (textField.tag == 2){
        if (_checkinDateVal) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
            [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
            [dateFormatter setDateFormat:DATE_FORMAT];
            NSDate *date = [dateFormatter dateFromString:_checkinDateVal];
            NSDate *tomorrow = [NSDate dateWithTimeInterval:(24*60*60) sinceDate:date];
            
            _checkoutDateVal = [dateFormatter stringFromDate:tomorrow];
            _tf_checkout.text = _checkoutDateVal;
            
        }
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField.tag == 1) {
        return;
    }
    
    _selectedTextField = textField;
    
    NSLog(@"CategoryDetailVC textFieldDidBeginEditing");
    _pickerViewDate = [[UIActionSheet alloc] initWithTitle:@"How many?"
                                                  delegate:self
                                         cancelButtonTitle:nil
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:nil];
    _theDatePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0.0, 44.0, 0.0, 0.0)];
    _theDatePicker.datePickerMode = UIDatePickerModeDate;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [dateFormatter setDateFormat:DATE_FORMAT];
    //[dateFormatter setDateFormat:@"MM/dd/YYYY"];
    
    [_theDatePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventValueChanged];
    
    if (textField.tag == 3) {
        NSString *checkin = _tf_checkin.text;
        if ([checkin isEqualToString:@""]) {
            
        }else{
            
            
            //_theDatePicker.minimumDate = [FormatDate dateFromString:checkin];
            NSDate *date = [dateFormatter dateFromString:checkin];
            
            NSDate *tomorrow = [NSDate dateWithTimeInterval:(24*60*60) sinceDate:date];
            
            [_theDatePicker setDate:tomorrow];
            [_theDatePicker setMinimumDate:tomorrow];
        }
        
    }
    
    _pickerToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    _pickerToolbar.barStyle= UIBarStyleDefault;
    [_pickerToolbar sizeToFit];
    NSMutableArray *barItems = [[NSMutableArray alloc] init];
    
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(DatePickerDoneClick:)];
    
    
    [barItems addObject:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil]];
    [barItems addObject:flexSpace];
    
    [_pickerToolbar setItems:barItems animated:YES];
    [_pickerViewDate addSubview:_pickerToolbar];
    [_pickerViewDate addSubview:_theDatePicker];
    [_pickerViewDate  showInView:[UIApplication sharedApplication].keyWindow];
    //[_pickerViewDate  showFromToolbar:_pickerToolbar];
    [_pickerViewDate setBounds:CGRectMake(0,0,320, 464)];
    
}

-(IBAction)DatePickerDoneClick:(id)sender{
    NSLog(@"CategoryDetailVC DatePickerDoneClick");
    
    NSDateFormatter *FormatDate = [[NSDateFormatter alloc] init];
    [FormatDate setLocale: [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [FormatDate setDateFormat:DATE_FORMAT];
    _selectedTextField.text = [FormatDate stringFromDate:[_theDatePicker date]];
    
    if (_selectedTextField.tag == 2) {
        _checkinDateVal = _selectedTextField.text;
    }
    
    if(_selectedTextField.tag == 3){
        _checkoutDateVal = _selectedTextField.text;
    }
    
    [self closeDatePicker:sender];
    //tableview.frame=CGRectMake(0, 44, 320, 416);
    
}

-(IBAction)dateChanged:(id)sender{
    NSLog(@"CategoryDetailVC dateChanged");
    NSDateFormatter *FormatDate = [[NSDateFormatter alloc] init];
    [FormatDate setLocale: [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [FormatDate setDateFormat:DATE_FORMAT];
    _selectedTextField.text = [FormatDate stringFromDate:[_theDatePicker date]];
}

-(BOOL)closeDatePicker:(id)sender{
    [_pickerViewDate dismissWithClickedButtonIndex:0 animated:YES];
    [_selectedTextField resignFirstResponder];
    
    return YES;
}

- (IBAction)step_value_change:(id)sender {
    
    NSLog(@"CategoryDetailVC stepper ValueChaged");
    //double stepperValue = ourStepper.value;
    //self.stepperValueLabel.text = [NSString stringWithFormat:@"%.f", stepperValue];
    _tf_quantity.text = [NSString stringWithFormat:@"%.f",[(UIStepper *)sender value] ];
    _quantityVal = _tf_quantity.text;
    
}

- (IBAction)checkAvaiability:(id)sender {
    
    if ([self _isEnoughCriteria]) {
        
        WSBooking *wsBooking = [[WSBooking alloc] init];
        wsBooking.delegate = (id)self;
        
        [wsBooking checkAvailabilityForLocation:_location_id forCheckin:_checkinDateVal andCheckout:_checkoutDateVal andQuantity:_quantityVal];
        
        /*
        if (self.delegate && [self.delegate respondsToSelector:@selector(bookingPopupVC:didPressCheckButtonWithData:)]) {
            [self.delegate bookingPopupVC:self didPressCheckButtonWithData:nil];
        }
        */
    }

}

- (IBAction)step_editing_change:(id)sender {
    
    UITextField *textField = (UITextField *)sender;
    
    _quantityVal = textField.text;
    [_step_quantity setValue:[textField.text doubleValue]];

}

- (void) wsBooking:(WSBooking *)wsBooking didCheckAvailabilityForLocationSuccess:(NSArray *)bookings{
    NSLog(@"BookingPopupVC wsBooking didCheckAvailabilityForLocationSuccess bookings %@", bookings);
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(bookingPopupVC:didCheckAvailabilitySuccessWithAvailable:)]) {
        
        NSArray *keys   = @[@"checkin", @"checkout", @"quantity", @"location_id"];
        NSArray *values = @[_checkinDateVal, _checkoutDateVal, _quantityVal, _location_id];
        
        NSDictionary *checkinParam = [NSDictionary dictionaryWithObjects:values forKeys:keys];
        
        [self.delegate bookingPopupVC:self didCheckAvailabilitySuccessWithAvailable:checkinParam];
    }
    
}

- (void) wsBooking:(WSBooking *)wsBooking didCheckAvailabilityForLocationFail:(NSError *)error
{
    NSLog(@"BookingPopupVC wsBooking didCheckAvailabilityForLocationFail");
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(bookingPopupVC:didCheckAvailabilitySuccessWithNoAvailable:)]) {
        [self.delegate bookingPopupVC:self didCheckAvailabilitySuccessWithNoAvailable:nil];
    }
    
}

- (BOOL) _isEnoughCriteria{
    NSString *checkin   = _tf_checkin.text;
    NSString *checkout  = _tf_checkout.text;
    NSString *value     = _tf_quantity.text;
    
    if ([checkin isEqualToString:@""] || [checkout isEqualToString:@""] || [value isEqualToString:@"0"] || [value isEqualToString:@""]) {
        [SBase showAlert:@"Please fill in checkin, check out, number of people"];
        return NO;
    }else{
        NSLog(@"Performing check Availability: %@ %@ %@", checkin, checkout, value);
        return YES;
    }
    
}

@end
