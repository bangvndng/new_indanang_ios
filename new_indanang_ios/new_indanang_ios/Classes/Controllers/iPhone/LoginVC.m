//
//  LoginVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/17/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "LoginVC.h"
#import "WSUser.h"
#import "SBase.h"

@interface LoginVC ()<WSUserDelegate, SAccountDelegate>{
    
    __weak IBOutlet UIView *_formLogin;
    __weak IBOutlet UITextField *_tf_email;
    __weak IBOutlet UITextField *_tf_password;
}

- (IBAction)doLogin:(id)sender;
- (IBAction)doLoginFB:(id)sender;


@end

@implementation LoginVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [_formLogin.layer setCornerRadius:5.0f];
    
    if (_isSwitch) {
        
    }else{
        UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelLogin)];
        
        self.navigationItem.leftBarButtonItem = cancelButton;
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)cancelLogin
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didCancelLogin)]) {
        [self.delegate didCancelLogin];
    }
}

#pragma mark - DELEGATES

- (void)wsUser:(WSUser *)wsUser didLoginSuccessWith:(IUser *)user
{
    NSLog(@"LoginVC didLoginSuccessWith");
    //if (_isSwitch) {
        
    //}else{
        if (self.delegate && [self.delegate respondsToSelector:@selector(didLoginSuccess)]) {
            [self.delegate didLoginSuccess];
        }
    //}
    
}

- (void)wsUser:(WSUser *)wsUser didLoginFailWithError:(NSError *)error
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didLoginFail)]) {
        [self.delegate didLoginFail];
    }

}

- (void)wsUser:(WSUser *)wsUser didLoginFBSuccessWith:(IUser *)user
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didLoginSuccess)]) {
        [self.delegate didLoginSuccess];
    }
}

- (void)wsUser:(WSUser *)wsUser didLoginFBFailWithError:(NSError *)error
{
    
}

#pragma mark - SACCOUNT DELEGATE

- (void) saccount:(SAccount *)saccount didAuthenticateFBSuccess:(NSDictionary *)fbInfo
{
    NSLog(@"LoginVC didAuthenticateFBSuccess %@", fbInfo);
    
    NSArray *keys = @[@"fbid", @"email", @"displayname"];
    NSArray *objects = @[
                         [fbInfo objectForKey:@"id"],
                         [fbInfo objectForKey:@"email"],
                         [fbInfo objectForKey:@"name"]
                         ];
    NSDictionary *userInfo = [[NSDictionary alloc] initWithObjects:objects forKeys:keys];
    
    WSUser *wsUser = [[WSUser alloc] init];
    wsUser.delegate = (id)self;
    
    [wsUser loginFBUser:userInfo];
    
}

- (void) saccount:(SAccount *)saccount didAuthenticateFBFail:(NSError *)error
{
    
    NSLog(@"LoginVC didAuthenticateFBFail");
    
}

#pragma mark - IBACTIONS

- (IBAction)doLogin:(id)sender {
    
    // Validate log in form
    if ([_tf_email.text isEqualToString:@""] || [_tf_password.text isEqualToString:@""]) {
        
    }else{
        
        NSArray *keys       = @[@"email",@"password"];
        NSArray *objects    = @[_tf_email.text, _tf_password.text];
        
        NSDictionary *postUser = [[NSDictionary alloc]initWithObjects:objects forKeys:keys];
        
        WSUser *wsUser = [[WSUser alloc] init];
        wsUser.delegate = (id) self;
        
        [wsUser loginUser:postUser];
    }
    
}

- (IBAction)doLoginFB:(id)sender {
    [SBase currentAccount].delegate = (id)self;
    [[SBase currentAccount] authenticateFB];
}

@end
