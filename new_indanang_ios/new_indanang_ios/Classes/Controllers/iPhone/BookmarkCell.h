//
//  BookmarkCell.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/22/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookmarkCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cellTitle;
@property (weak, nonatomic) IBOutlet UILabel *cellRecommended;


@end
