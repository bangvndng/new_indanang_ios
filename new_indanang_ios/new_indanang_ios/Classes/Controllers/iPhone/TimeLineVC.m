//
//  TimeLineVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/21/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "TimeLineVC.h"
#import "Define.h"

#import "TimeLinePhotoCell.h"
#import "TimeLineTipCell.h"

#import "WSTimeline.h"

#import "ITimeline.h"
#import "NSDate+TimeAgo.h"

#import "SBase.h"
#import "LocationVC.h"

@interface TimeLineVC ()<WSTimelineDelegate>{
    
    __weak IBOutlet UITableView *_tableView;
    
    //PULL TO REFRESH
    BOOL                                    _isFirstTouch, _isLoading;
    
    UIView                                  *refreshHeaderView;
    UILabel                                 *refreshLabel;
    UIImageView                             *refreshArrow;
    UIActivityIndicatorView                 *refreshSpinner;
    BOOL                                    isDragging;
    BOOL                                    isLoading;
    NSString                                *textPull;
    NSString                                *textRelease;
    NSString                                *textLoading;
    // ------------- //
    
    NSMutableArray *_items;
    
    NSMutableDictionary *_avatarImages;
    NSMutableDictionary *_mainImages;

    int _page, _limit, _total;
}

@end

@implementation TimeLineVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupStrings];
    [self addPullToRefreshHeader];
    
    [self registerNibsForTableView];
    
    _items = [[NSMutableArray alloc] init];
    
    _avatarImages = [[NSMutableDictionary alloc] init];
    _mainImages = [[NSMutableDictionary alloc] init];
    [self loadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) registerNibsForTableView
{
    NSArray *cells = @[@"TimeLinePhotoCell", @"TimeLineTipCell"];
    
    for (NSString *cellClass in cells) {
        [_tableView registerNib:[UINib  nibWithNibName:cellClass
                                                bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:cellClass];
    }
}

#pragma mark - LoadData

- (void) loadData
{
    if (!_isLoading) {
        WSTimeline *wsTimeline = [[WSTimeline alloc] init];
        wsTimeline.delegate = (id)self;
        
        _page = 1;
        _limit = 20;
        
        _isLoading = YES;
        
        [wsTimeline getTimelineWithPage:_page andLimit:_limit];
    }
    

}

- (void) loadMoreFeeds
{
    NSLog(@"loadMoreFeeds");
    _page++;
    _isLoading      = YES;
    
    WSTimeline *wsTimeline = [[WSTimeline alloc] init];
    wsTimeline.delegate = (id)self;
    [wsTimeline getTimelineWithPage:_page andLimit:_limit];

}

- (void) wsTimeline:(WSTimeline *)wsTimeline didGetTimelineSuccess:(NSArray *)timelines page:(int)page limit:(int)limit count:(int)count
{
    NSLog(@"TimeLineVC wsTimeline didGetTimelineSuccess %@", timelines);
    
    if (page == 1) {
        [_items removeAllObjects];
    }
    
    _page = page;
    _limit = limit;
    _total = count;
    
    int fcount = [_items count];
    NSLog(@"TimeLineVC wsTimeline didGetTimelineSuccess Before addObjects");
    [_items addObjectsFromArray:timelines];
    
    NSLog(@"TimeLineVC wsTimeline didGetTimelineSuccess Before get images");
    for (ITimeline *timeline in timelines) {
        if ([_avatarImages objectForKey:timeline.user_id]) {
            
        }else{
            
            if (!timeline.user_fbid || [timeline.user_fbid isEqualToString:@""]) {
                
            }else{
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    //NSString *imageURL = [NSString stringWithFormat:@"%@/%@", COMMON_WS_URL, timeline.content];
                    
                    NSURL *profilePictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", timeline.user_fbid ]];
                    NSData *imageData = [NSData dataWithContentsOfURL:profilePictureURL];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSLog(@"setObject 1");
                        if (imageData) {
                            [_avatarImages setObject:[UIImage imageWithData:imageData] forKey:[NSString stringWithFormat:@"%@", timeline.user_id]];
                            
                            if ([timeline.type isEqualToString:@"photo"]) {
                                TimeLinePhotoCell *photoCell =(TimeLinePhotoCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:fcount inSection:0]];
                                photoCell.cellAvatar.image = [UIImage imageWithData:imageData];
                            }else{
                                TimeLineTipCell *photoCell =(TimeLineTipCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:fcount inSection:0]];
                                photoCell.cellAvatar.image = [UIImage imageWithData:imageData];
                            }
                        }
                        
                        
                        
                    });
                });
            }
            
            
        }
        
        if ([timeline.type isEqualToString:@"photo"]) {
            if ([_mainImages objectForKey:timeline.timeline_id]) {
                
            }else{
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    NSString *imageURL = [NSString stringWithFormat:@"%@/%@", COMMON_WS_URL, timeline.content];
                    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageURL]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSLog(@"setObject 2");
                        if (imageData) {
                            [_mainImages setObject:[UIImage imageWithData:imageData] forKey:[NSString stringWithFormat:@"%@", timeline.timeline_id]];
                            
                            TimeLinePhotoCell *photoCell =(TimeLinePhotoCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:fcount inSection:0]];
                            photoCell.cellMainImage.image = [UIImage imageWithData:imageData];

                        }
                        
                    });
                });
            }
            
        }
        fcount ++;
    }
    _isLoading = NO;
    [self stopLoading];
    
    [_tableView reloadData];
}

- (void) loadMainImages
{
    
}

- (void) loadAvatar
{
    
}

#pragma mark - TABLEVIEW DATASOURCE

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_items count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ITimeline *item = _items[indexPath.row];
    
    if ([item.type isEqualToString:@"tip"]) {
        return 155;
    }else{
        return 275;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ITimeline *item = _items[indexPath.row];
    
    if ([item.type isEqualToString:@"tip"]) {
        return [self setTimeLineTipCell:tableView cellForRowAtIndexPath:indexPath];
    }else{
        return [self setTimeLinePhotoCell:tableView cellForRowAtIndexPath:indexPath];
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_isLoading == YES) {
        return;
    }
    //[tableView deselectRowAtIndexPath:indexPath animated:NO];
    ITimeline *item = _items[indexPath.row];
    if (item.location_id) {
        LocationVC *locationVC = [[LocationVC alloc] init];
        locationVC.location_id = item.location_id;
        
        [self.navigationController pushViewController:locationVC animated:YES];
    }
    
    
}

#pragma mark - CELL SETUP

- (TimeLineTipCell *) setTimeLineTipCell:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellId = @"TimeLineTipCell";
    TimeLineTipCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (cell == nil) {
        cell = [[TimeLineTipCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellId];
    }
    ITimeline *item = _items[indexPath.row];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:item.created];
    
    cell.cellCreated.text = [date timeAgo];
    cell.cellContent.text = item.content;
    cell.cellUsername.text = item.user_displayname;
    
    cell.cellMessage.text = [NSString stringWithFormat:@"%@", [[[SBase dataSource] loadLocationById:item.location_id] valueForKey:@"title"]];
    
    if (item.user_fbid && ![item.user_fbid isEqualToString:@""]) {
        // border radius
        [cell.cellViewAvatar.layer setCornerRadius:23.0f];
        
        // border
        [cell.cellViewAvatar.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [cell.cellViewAvatar.layer setBorderWidth:0.5f];

    }else{
        cell.cellViewAvatar.alpha = 0;
    }
    
    if ([_avatarImages objectForKey:[NSString stringWithFormat:@"%@", item.user_id]]) {
        cell.cellAvatar.image = [_avatarImages objectForKey:[NSString stringWithFormat:@"%@", item.user_id]];
    }
    
    return cell;
}

- (TimeLinePhotoCell *) setTimeLinePhotoCell:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellId = @"TimeLinePhotoCell";
    TimeLinePhotoCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (cell == nil) {
        cell = [[TimeLinePhotoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellId];
    }
    ITimeline *item = _items[indexPath.row];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:item.created];
    
    cell.cellTime.text = [date timeAgo];
    cell.cellUsername.text = item.user_displayname;
    
    cell.cellMessage.text = [NSString stringWithFormat:@"%@", [[[SBase dataSource] loadLocationById:item.location_id] valueForKey:@"title"]];
    
    if (item.user_fbid && ![item.user_fbid isEqualToString:@""]) {
        // border radius
        [cell.cellViewAvatar.layer setCornerRadius:23.0f];
        
        // border
        [cell.cellViewAvatar.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [cell.cellViewAvatar.layer setBorderWidth:0.5f];
        
    }else{
        cell.cellViewAvatar.alpha = 0;
    }
    
    if ([_mainImages objectForKey:[NSString stringWithFormat:@"%@",item.timeline_id]]) {
        cell.cellMainImage.image = [_mainImages objectForKey:[NSString stringWithFormat:@"%@",item.timeline_id]];
    }else{
        cell.cellMainImage.image = nil;
    }
    
    if ([_avatarImages objectForKey:[NSString stringWithFormat:@"%@", item.user_id]]) {
        cell.cellAvatar.image = [_avatarImages objectForKey:[NSString stringWithFormat:@"%@", item.user_id]];
    }
    
    return cell;
}

#pragma mark - Pull CODE
- (void)setupStrings{
    textPull    = @"Pull down to refresh";             //@"Pull down to refresh...";
    textRelease = @"Release to refresh";                   //@"Release to refresh...";
    textLoading = @"Now loading";                             //@"Loading...";
}

- (void)addPullToRefreshHeader {
    refreshHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0 - REFRESH_HEADER_HEIGHT, 320, REFRESH_HEADER_HEIGHT)];
    refreshHeaderView.backgroundColor = [UIColor clearColor];
    
    refreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(110, 0, 320, REFRESH_HEADER_HEIGHT)];
    refreshLabel.backgroundColor = [UIColor clearColor];
    refreshLabel.font = [UIFont boldSystemFontOfSize:13.0];
    refreshLabel.textColor = [UIColor colorWithRed:197.0/255.0 green:187.0/255.0 blue:184.0/255.0 alpha:1.0];
    
    refreshArrow = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pull_arrow.png"]];
    refreshArrow.frame = CGRectMake(floorf((REFRESH_HEADER_HEIGHT - 13) / 2 + 70),
                                    (floorf(REFRESH_HEADER_HEIGHT - 26) / 2),
                                    13, 26);
    
    refreshSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    refreshSpinner.frame = CGRectMake(floorf(floorf(REFRESH_HEADER_HEIGHT - 20) / 2) + 70 , floorf((REFRESH_HEADER_HEIGHT - 20) / 2), 20, 20);
    refreshSpinner.hidesWhenStopped = YES;
    
    [refreshHeaderView addSubview:refreshLabel];
    [refreshHeaderView addSubview:refreshArrow];
    [refreshHeaderView addSubview:refreshSpinner];
    [_tableView addSubview:refreshHeaderView];
}

- (void)startLoading {
    isLoading = YES;
    
    // Show the header
    [UIView animateWithDuration:0.3 animations:^{
        _tableView.contentInset = UIEdgeInsetsMake(REFRESH_HEADER_HEIGHT, 0, 0, 0);
        refreshLabel.text = textLoading;
        refreshArrow.hidden = YES;
        [refreshSpinner startAnimating];
    } completion:^(BOOL finished) {
        // call WS
        [self loadData];
        
    }];
}

- (void)stopLoading {
    isLoading = NO;
    
    // Hide the header
    [UIView animateWithDuration:0.3 animations:^{
        _tableView.contentInset = UIEdgeInsetsZero;
        [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
    }
                     completion:^(BOOL finished) {
                         [self performSelector:@selector(stopLoadingComplete)];
                     }];
}

- (void)stopLoadingComplete {
    // Reset the header
    refreshLabel.text = textPull;
    refreshArrow.hidden = NO;
    [refreshSpinner stopAnimating];
}

- (void)refresh {
    // This is just a demo. Override this method with your custom reload action.
    // Don't forget to call stopLoading at the end.
    [self performSelector:@selector(stopLoading) withObject:nil afterDelay:2.0];
}



#pragma mark - UIScrollDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (scrollView == _tableView) {
        if (isLoading) return;
        isDragging = YES;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == _tableView) {
        if (isLoading) {
            // Update the content inset, good for section headers
            if (scrollView.contentOffset.y > 0)
                _tableView.contentInset = UIEdgeInsetsZero;
            else if (scrollView.contentOffset.y >= -REFRESH_HEADER_HEIGHT)
                _tableView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        } else if (isDragging && scrollView.contentOffset.y < 0) {
            // Update the arrow direction and label
            [UIView animateWithDuration:0.25 animations:^{
                if (scrollView.contentOffset.y < -REFRESH_HEADER_HEIGHT) {
                    // User is scrolling above the header
                    refreshLabel.text = textRelease;
                    [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
                } else {
                    // User is scrolling somewhere within the header
                    refreshLabel.text = textPull;
                    [refreshArrow layer].transform = CATransform3DMakeRotation(M_PI * 2, 0, 0, 1);
                }
            }];
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView == _tableView) {
        if (!decelerate) {
            if (scrollView.contentOffset.y <= scrollView.contentSize.height - 1.0 * scrollView.frame.size.height &&
                [_items count] < _total &&
                !_isLoading)
            {
                [self loadMoreFeeds];
            }
        }
        
        if (isLoading) return;
        isDragging = NO;
        if (scrollView.contentOffset.y <= -REFRESH_HEADER_HEIGHT) {
            // Released above the header
            [self startLoading];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    NSLog(@"scrollViewDidEndDecelerating");
    if (scrollView == _tableView) {
        NSLog(@"%i", [_items count]);
        NSLog(@"%i", _total);
        NSLog(@"%hhd", isLoading);
        NSLog(@"%f", scrollView.contentOffset.y );
        NSLog(@"%f", scrollView.contentSize.height);
        NSLog(@"%f", scrollView.frame.size.height);
        
        if (scrollView.contentOffset.y <= scrollView.contentSize.height - scrollView.frame.size.height &&
            [_items count] < _total  &&
            !_isLoading)
        {
            [self loadMoreFeeds];
        }
    }
}

@end
