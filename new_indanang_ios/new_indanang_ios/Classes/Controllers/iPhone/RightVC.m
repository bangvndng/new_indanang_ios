//
//  RightVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/12/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "RightVC.h"
#import "RightLargeCell.h"

#import "SBase.h"

#import "SubRightAbstract.h"
#import "SubCenterAbstract.h"

#define RIGHT_TABLE_TITLE_INDEX     0
#define RIGHT_TABLE_CLASS_INDEX     1
#define RIGHT_TABLE_ICON_INDEX      2

@interface RightVC (){
    
}

@property (strong, nonatomic) NSMutableArray *items;

@end

@implementation RightVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    if (_items == nil) {
        _items = [[NSMutableArray alloc] initWithArray:@[
                                                         @[
                                                             @"inDanang",
                                                             @"HomeVC",
                                                             @"ic_menu_home"
                                                             ],
                                                         @[
                                                             @"map",
                                                             @"MapVC",
                                                             @"ic_menu_locate"
                                                             ],
                                                         @[
                                                             @"bookmarks",
                                                             @"BookmarkVC",
                                                             @"ic_menu_star"
                                                             ],
                                                         @[
                                                             @"bookings",
                                                             @"BookingVC",
                                                             @"ic_menu_star"
                                                             ],

                                                         @[
                                                             @"about",
                                                             @"AboutVC",
                                                             @"ic_menu_info"
                                                             ],
                                                         @[
                                                             @"account",
                                                             @"AccountVC",
                                                             @"ic_menu_account"
                                                             ],
                                                         
                                                         ]];
    }
    
    
    
    [_tableView registerNib:[UINib  nibWithNibName:@"RightLargeCell"
                                            bundle:[NSBundle mainBundle]]
                            forCellReuseIdentifier:@"RightLargeCell"];
    
    
    
    if ([SDevice isIOS7]) {
        self.view.frame = CGRectMake(
                                      self.view.frame.origin.x,
                                      self.view.frame.origin.y,
                                      self.view.frame.size.width,
                                      self.view.frame.size.height
                                      );
    }
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
    NSLog(@"RightVC viewWillAppear user is admin: %@", [SAccount currentUser].isAdmin);
    
    if ([[SAccount currentUser] isAdminUser]  && ([_items count] == 6)) {
        [_items addObject:@[
                           @"admin",
                           @"AdminVC",
                           @"ic_menu_account"
                           ]];
        [_tableView reloadData];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TABLEVIEW DATASOURCE

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_items count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *CellId = @"RightLargeCell";
    RightLargeCell *cell = [tableView dequeueReusableCellWithIdentifier:CellId];
    
    if (cell == nil) {
        cell = [[RightLargeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellId];
    }
    cell._cellTitle.text = [[_items objectAtIndex:indexPath.row] objectAtIndex:RIGHT_TABLE_TITLE_INDEX];
    
    cell._cellImage.image = [UIImage imageNamed:[[_items objectAtIndex:indexPath.row] objectAtIndex:RIGHT_TABLE_ICON_INDEX]];
    
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([[[_items objectAtIndex:indexPath.row] objectAtIndex:0] isEqualToString:@"inDanang"]) {
    
        [[[SBase appDelegate].mainVC centerVC] popToRootViewControllerAnimated:YES];
        
    }else if([[[_items objectAtIndex:indexPath.row] objectAtIndex:0] isEqualToString:@"map"]){
        Class centerSubviewClass = NSClassFromString([[_items objectAtIndex:indexPath.row] objectAtIndex:1]);
        if ([[[[SBase appDelegate].mainVC centerVC] topViewController] class] == centerSubviewClass) {
        
        }else{
            BOOL found = NO;
            for (UIViewController *viewController in [[SBase appDelegate].mainVC centerVC].viewControllers) {
                if ([viewController class] == NSClassFromString(@"MapVC")) {
                    found = YES;
                    [[[SBase appDelegate].mainVC centerVC] popToViewController:viewController animated:YES];
                    break;
                }
            }
            if (found == YES) {
            }else{
                SubCenterAbstract *centerSubview = [[centerSubviewClass alloc] init];
                [[[SBase appDelegate].mainVC centerVC] pushViewController:centerSubview animated:YES];
            }
        }
    }else if([[[_items objectAtIndex:indexPath.row] objectAtIndex:0] isEqualToString:@"bookmarks"]){
        Class centerSubviewClass = NSClassFromString([[_items objectAtIndex:indexPath.row] objectAtIndex:1]);
        if ([[[[SBase appDelegate].mainVC centerVC] topViewController] class] == centerSubviewClass) {
            
        }else{
            BOOL found = NO;
            for (UIViewController *viewController in [[SBase appDelegate].mainVC centerVC].viewControllers) {
                if ([viewController class] == NSClassFromString(@"BookmarkVC")) {
                    found = YES;
                    [[[SBase appDelegate].mainVC centerVC] popToViewController:viewController animated:YES];
                    break;
                }
            }
            if (found == YES) {
            }else{
                SubCenterAbstract *centerSubview = [[centerSubviewClass alloc] init];
                [[[SBase appDelegate].mainVC centerVC] pushViewController:centerSubview animated:YES];
            }
        }
    } else{
    
        Class rightSubviewClass = NSClassFromString([[_items objectAtIndex:indexPath.row] objectAtIndex:1]);
        SubRightAbstract *rightSubview = [[rightSubviewClass alloc] init];
        
        [[[SBase appDelegate].mainVC centerVC] pushViewController:rightSubview animated:YES];
        
    }
    
    [[SBase appDelegate].mainVC _hideRightView];
}


@end
