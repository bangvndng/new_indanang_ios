//
//  SuggestionCell.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/22/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "SuggestionCell.h"

@implementation SuggestionCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)doBookmark:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(suggestionCell:doBookmark:)]) {
        [self.delegate suggestionCell:self doBookmark:_iLocation];
    }
}

- (IBAction)doMap:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(suggestionCell:doMap:)]) {
        [self.delegate suggestionCell:self doMap:_iLocation];
    }
}

- (IBAction)doDetails:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(suggestionCell:doDetails:)]) {
        [self.delegate suggestionCell:self doDetails:_iLocation];
    }
}
@end
