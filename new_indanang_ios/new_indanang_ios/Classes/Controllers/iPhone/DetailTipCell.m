//
//  DetailTipCell.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/22/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "DetailTipCell.h"

@implementation DetailTipCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)addTip:(id)sender {
    NSLog(@"DetailTipCell addTip");
    if (self.delegate && [self.delegate respondsToSelector:@selector(detailTipCell:addTip:)]) {
        [self.delegate detailTipCell:self addTip:nil];
    }
    
}

- (IBAction)addPhoto:(id)sender {
    NSLog(@"DetailTipCell addPhoto");
    if (self.delegate && [self.delegate respondsToSelector:@selector(detailTipCell:addPhoto:)]) {
        [self.delegate detailTipCell:self addPhoto:nil];
    }
}
@end
