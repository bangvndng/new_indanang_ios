//
//  AboutVC.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/17/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "AboutVC.h"
#import "Define.h"

@interface AboutVC (){
    
    __weak IBOutlet UIWebView *_webView;
    
}

@end

@implementation AboutVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSString *urlString = [NSString stringWithFormat:@"%@/%@%@", COMMON_WS_URL,URL_WS_CMS,@"?alias=about&type=wysiwyg"];
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
