//
//  WSTimeline.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/29/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "WSTimeline.h"
#import "Define.h"
#import "ITimeline.h"

@interface WSTimeline(){
    int _page, _limit, _count;
}

@end

@implementation WSTimeline

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)dealloc
{
    self.delegate = nil;
}

- (void) getTimelineWithPage:(int)page andLimit:(int)limit
{
    _page = page;
    _limit = limit;
    
    NSArray *keys   = @[@"page", @"limit"];
    NSArray *values = @[
                        [NSString stringWithFormat:@"%i", page],
                        [NSString stringWithFormat:@"%i", limit]
                        ];
    
    NSDictionary *getParams = [NSDictionary dictionaryWithObjects:values forKeys:keys];
    
    self.flag = FLAG_WSTIMELINE_LIST_ALL;
    
    [self get:URL_WS_TIMELINE_LIST_ALL andGetParams:getParams];
}

- (ITimeline *) _parseTimeline:(NSDictionary *)timelineData
{
    NSLog(@"WSTimeline _parseTimeline %@", timelineData);
    ITimeline *timeline = [[ITimeline alloc] init];
    
    timeline.timeline_id    = [timelineData objectForKey:@"id"];
    timeline.created        = [timelineData objectForKey:@"created"];
    timeline.user_id        = [timelineData objectForKey:@"user_id"];
    timeline.location_id    = [timelineData objectForKey:@"location_id"];
    timeline.content        = [timelineData objectForKey:@"content"];
    timeline.type           = [timelineData objectForKey:@"type"];
    
    timeline.user_fbid      = [timelineData objectForKey:@"user_fbid"];
    timeline.user_displayname      = [timelineData objectForKey:@"user_displayname"];
    return timeline;
}

// ABSTRACT FUNCTION
- (void) _parseGetSuccess:(id) JSON
{
    if ([JSON objectForKey:@"success"]) {
        NSLog(@"WSCategory _parseGetSuccess ");
        
        NSArray *data = [JSON objectForKey:@"data"];
        
        NSMutableArray *results = [[NSMutableArray alloc] init];
        
        if ([data count] == 0) {
            
        }else{
            
            for (NSDictionary *timeline in data) {
                
                NSDictionary *timelineData = [timeline objectForKey:@"Timeline"];
                
                [results addObject:[self _parseTimeline:timelineData]];
            }
        }
        
        _count = [[JSON objectForKey:@"count"] intValue];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(wsTimeline:didGetTimelineSuccess:page:limit:count:)]) {
            [self.delegate wsTimeline:self didGetTimelineSuccess:[results mutableCopy] page:_page limit:_limit count:_count];
        }
        
    }else{
        NSLog(@"WSCategory _parseGetSuccess FAIL %@", JSON);
    }
}

// ABSTRACT FUNCTION
- (void) _parseGetFail:(NSError *) error
{
    NSLog(@"WSCategory _parseGetFail FAIL %@", error);
}

@end
