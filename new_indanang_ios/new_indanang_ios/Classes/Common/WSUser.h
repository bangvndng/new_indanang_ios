//
//  WSUser.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/12/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//
#import "WSBase.h"
#import "IUser.h"

@protocol WSUserDelegate;

@interface WSUser : WSBase

@property (weak, nonatomic) id<WSUserDelegate> delegate;

- (void)loginUser:(NSDictionary *)user;
- (void)registerUser:(NSDictionary *)user;
- (void)loginFBUser:(NSDictionary *)user;

@end

@protocol WSUserDelegate <NSObject>

@optional

- (void)wsUser:(WSUser *)wsUser didLoginSuccessWith:(IUser *)user ;
- (void)wsUser:(WSUser *)wsUser didLoginFailWithError:(NSError *)error ;

- (void)wsUser:(WSUser *)wsUser didRegisterSuccessWith:(IUser *)user ;
- (void)wsUser:(WSUser *)wsUser didRegisterFailWithError:(NSError *)error ;

- (void)wsUser:(WSUser *)wsUser didLoginFBSuccessWith:(IUser *)user ;
- (void)wsUser:(WSUser *)wsUser didLoginFBFailWithError:(NSError *)error ;

@end
