//
//  SDevice.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/12/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "SDevice.h"

@implementation SDevice

+(BOOL)isIOS7
{
    NSString *version = [[UIDevice currentDevice] systemVersion];
    if ([version floatValue] >= 7.0) {
        return YES;
    }else {
        return NO;
    }

}

+(BOOL)isIPhone5
{
    if([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
        if([UIScreen mainScreen].bounds.size.height == 568.0){
//            NSLog(@"init app with iPhone 5 screen");
            return YES;
        }
        else{
//            NSLog(@"init app with iPhone default screen");
            return NO;
        }
    }else{
//        NSLog(@"init app with iPad default screen");
        return NO;
    }
}

@end
