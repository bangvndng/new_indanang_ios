//
//  WSBase.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/12/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

//@protocol WSBaseDelegate;

@interface WSBase : NSObject

//@property (weak, nonatomic) id<WSBaseDelegate> wsBaseDelegate;

// This flag is used to determine which parent WSBase should be responsed for
@property (nonatomic) int flag;

- (void)get:(NSString *)url andGetParams:(NSDictionary *)getParams;

- (void)post:(NSString *)url andPostParams:(NSDictionary *)postParams;

- (void)multiPartsFormPost:(NSString *)url andParams:(NSDictionary *)params andFormParts:(NSArray *)formParts;

- (void)customGet:(NSString *)url;

@end

/*
@protocol WSBaseDelegate <NSObject>

@optional

- (void) wsBase:(WSBase *)wsBase didGetSuccess:(NSDictionary *)getResult;
- (void) wsBase:(WSBase *)wsBase didGetFail:(NSError *)error;

- (void) wsBase:(WSBase *)wsBase didPostSuccess:(NSDictionary *)postResult;
- (void) wsBase:(WSBase *)wsBase didPostFail:(NSError *)error;

- (void) wsBase:(WSBase *)wsBase didMultiPartsFormPostSuccess:(NSDictionary *)postResult;
- (void) wsBase:(WSBase *)wsBase didMultiPartsFormPostFail:(NSError *)error;

@end
*/