//
//  WSRecommend.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/22/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "WSRecommend.h"
#import "Define.h"
@implementation WSRecommend

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)dealloc
{
    //self.delegate = nil;
}

- (void) addRecommend:(NSDictionary *)recommendInfo
{
    self.flag = FLAG_WSRECOMMEND_ADD;
    [self post:URL_WS_RECOMMEND_ADD andPostParams:recommendInfo];

}

// ABSTRACT FUNCTION
- (void) _parsePostSuccess:(id) JSON
{
    NSDictionary *result = [JSON copy];
    
    NSLog(@"WSRECOMMEND _parsePostSuccess JSON %@", result);
    
    if ([result objectForKey:@"success"] == 0) {
        // Fail
    }
    
    switch (self.flag) {
        case FLAG_WSRECOMMEND_ADD:
            if (self.delegate && [self.delegate respondsToSelector:@selector(wsRecommend:didAddRecommendSuccess:)]) {
                [self.delegate wsRecommend:self didAddRecommendSuccess:nil];
            }
            break;
        default:
            break;
    }
}

// ABSTRACT FUNCTION
- (void) _parsePostFail:(NSError *) error
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(wsRecommend:didAddRecommendFail:)]) {
        [self.delegate wsRecommend:self didAddRecommendFail:error];
    }
}

@end
