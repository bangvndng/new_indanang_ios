//
//  WSPhoto.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/18/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "WSPhoto.h"
#import "Define.h"

#import "UIImage+UIImageFunctions.h"

@interface WSPhoto(){
    NSMutableArray *_photos;
}
@end

@implementation WSPhoto

- (id)init
{
    self = [super init];
    if (self) {
        _photos = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc
{
    self.delegate = nil;
    _photos = nil;
    
}

# pragma mark - PUBLIC FUNCTIONS

- (void) uploadPhoto:(UIImage *)photo forLocationId:(NSString *)location_id
{
    NSArray *keys = @[@"location_id"];
    NSArray *objects = @[location_id];
    
    NSDictionary *params = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    
    NSMutableArray *formParts = [[NSMutableArray alloc] init];
    photo = [photo scaleToSize:CGSizeMake(640, 480)];
    NSData *imageToUpload = UIImageJPEGRepresentation(photo, 0.6);
    [formParts addObject:imageToUpload];

    [self multiPartsFormPost:URL_WS_PHOTO_UPLOAD andParams:params andFormParts:formParts];
}

- (void) listPhotos:(NSString *)location_id
{
    NSArray *keys = @[@"location_id"];
    NSArray *objects = @[location_id];
    
    NSDictionary *param = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    
    [self get:URL_WS_PHOTO_LIST andGetParams:param];
}

- (IPhoto *) _parsePhoto:(NSDictionary *)photoData
{
    IPhoto *iPhoto    = [[IPhoto alloc] init];
    
    iPhoto.photo_id     = [photoData objectForKey:@"id"];
    iPhoto.title        = [photoData objectForKey:@"title"];
    iPhoto.path         = [photoData objectForKey:@"path"];
    iPhoto.location_id  = [photoData objectForKey:@"location_id"];
    iPhoto.user_id      = [photoData objectForKey:@"user_id"];
    
    return iPhoto;
}

# pragma mark - INHERITANCE FUNCTIONS

- (void) _parseMultiPartFormPostSuccess:(id) JSON
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(wsPhoto:didUploadPhotoSuccess:)]) {
        [self.delegate wsPhoto:self didUploadPhotoSuccess:JSON];
    }
}


- (void) _parseMultiPartFormPostFail:(NSError *) error
{
    NSLog(@"%@",error);
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(wsPhoto:didUploadPhotoFail:)]) {
        [self.delegate wsPhoto:self didUploadPhotoFail:error];
    }
}

// ABSTRACT FUNCTION
- (void) _parseGetSuccess:(id) JSON
{
    if ([JSON objectForKey:@"success"]) {
        NSLog(@"WSPhoto _parseGetSuccess ");
        
        NSArray *data = [JSON objectForKey:@"data"];
        if ([data count] == 0) {
            
        }else{
            
            for (NSDictionary * photo in data) {
                
                NSDictionary *photoData = [photo objectForKey:@"Photo"];
                [_photos addObject:[self _parsePhoto:photoData]];
                
            }
        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(wsPhoto:didListPhotosSuccess:)]) {
            [self.delegate wsPhoto:self didListPhotosSuccess:[_photos mutableCopy]];
        }
        
    }else{
        NSLog(@"WSPhoto _parseGetSuccess FAIL %@", JSON);
    }

}

// ABSTRACT FUNCTION
- (void) _parseGetFail:(NSError *) error
{
    
}

@end
