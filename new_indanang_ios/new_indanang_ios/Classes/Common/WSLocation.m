//
//  WSLocation.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/18/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "WSLocation.h"
#import "Define.h"

#import "ITransporter.h"

#import "LocationManager.h"
#import "SBase.h"

@interface WSLocation(){

    NSMutableArray *_locations;
    CLLocation *_clLocation;
    ILocation *_iLocation;
}

@end

@implementation WSLocation

- (id)init
{
    self = [super init];
    if (self) {
        
        _locations = [[NSMutableArray alloc] init];
        _clLocation = [[LocationManager sharedSingleton] locationManager].location;
        
    }
    return self;
}

- (void)dealloc
{
    self.delegate = nil;
    _locations = nil;
}

#pragma mark - PUBLIC FUNCTIONS

- (void)getSuggestion
{
    NSArray *keys   = @[@"page", @"limit"];
    NSArray *values = @[@"1", @"99999"];
    NSDictionary *getParams = [NSDictionary dictionaryWithObjects:values forKeys:keys];
    
    self.flag = FLAG_WSLOCATION_GET_SUGGESTION;
    
    [self get:URL_WS_LOCATION_GET_SUGGESTIONS andGetParams:getParams];
}

- (void)listLocationsByCategoryId:(NSString *)categoryId
{
    
}

- (void)listAllLocations
{
    NSArray *keys   = @[@"page", @"limit"];
    NSArray *values = @[@"1", @"99999"];
    NSDictionary *getParams = [NSDictionary dictionaryWithObjects:values forKeys:keys];
    
    self.flag = FLAG_WSLOCATION_LIST_ALL;
    
    [self get:URL_WS_LOCATION_LIST_ALL andGetParams:getParams];
}

- (void)getLocation:(NSString *)locationId
{
    self.flag = FLAG_WSLOCATION_LOCATION;
    
    NSArray *keys, *values;
    if (![SAccount currentUser].user_id) {
        keys = @[@"id"];
        values = @[locationId];
    }else{
        keys = @[@"id", @"user_id"];
        values = @[locationId, [SAccount currentUser].user_id];
    }
    
    NSDictionary *getParams = [NSDictionary dictionaryWithObjects:values forKeys:keys];
    
    [self get:URL_WS_LOCATION_LOCATION andGetParams:getParams];
}

- (void)updateLocationsByCategory:(NSString *)categoryId
{
    self.flag = FLAG_WSLOCATION_UPDATE_CATEGORY;
    
    NSArray *keys = @[@"category_id",@"page",@"limit"];
    NSArray *values = @[categoryId,@"1",@"9999"];
    NSDictionary *getParams = [NSDictionary dictionaryWithObjects:values forKeys:keys];
    
    [self get:URL_WS_LOCATION_UPDATE_CATEGORY andGetParams:getParams];
}

- (void) createLocation:(NSDictionary *)locationInfo
{
    self.flag = FLAG_WSLOCATION_CREATE;
    [self post:URL_WS_LOCATION_CREATE andPostParams:locationInfo];
}

- (void) updateLocation:(NSDictionary *)locationInfo
{
    self.flag = FLAG_WSLOCATION_UPDATE;
    [self post:URL_WS_LOCATION_UPDATE andPostParams:locationInfo];
}

- (void) filterLocations:(NSString *)checkin andCheckout:(NSString *)checkout andQuantity:(NSString *)quantity
{
    self.flag = FLAG_WSLOCATION_FILTER;
    
    NSArray *keys = @[@"checkin",@"checkout",@"quantity"];
    NSArray *values = @[checkin,checkout, quantity];
    NSDictionary *getParams = [NSDictionary dictionaryWithObjects:values forKeys:keys];
    
    [self get:URL_WS_LOCATION_FILTER andGetParams:getParams];
}

- (void) placeBooking:(NSString *)checkin andCheckout:(NSString *)checkout andQuantity:(NSString *)quantity forLocationId:(NSString *)location_id
{
    self.flag = FLAG_WSLOCATION_PLACEBOOKING;
    
    NSArray *keys = @[@"checkin", @"checkout", @"quantity", @"location_id"];
    NSArray *values = @[checkin,checkout, quantity, location_id];
    NSDictionary *postParams = [NSDictionary dictionaryWithObjects:values forKeys:keys];
    
    [self post:URL_WS_LOCATION_PLACEBOOKING andPostParams:postParams];
}

#pragma mark - INHERITANCE FUNCTIONS

- (void) _parseGetSuccess:(id) JSON
{
    switch (self.flag) {
        case FLAG_WSLOCATION_LIST_ALL:
            [self _parseGetListAll:JSON];
            break;
        case FLAG_WSLOCATION_LOCATION:
            [self _parseGetLocation:JSON];
            break;
        case FLAG_WSLOCATION_UPDATE_CATEGORY:
            [self _parseUpdateByCategory:JSON];
            break;
        case FLAG_WSLOCATION_GET_SUGGESTION:
            [self _parseGetSuggestion:JSON];
            break;
        case FLAG_WSLOCATION_FILTER:
            [self _parseGetFilter:JSON];
            break;
        default:
            break;
    }
    
    
}

- (void) _parseGetFail:(NSError *) error
{
    NSLog(@"WSLocation _parseGetFail %@", error);
}


- (void) _parsePostSuccess:(id) JSON
{
    NSDictionary *result = [JSON copy];
    
    NSLog(@"WSLocation _parsePostSuccess JSON %@", result);
    
    if ([result objectForKey:@"success"] == 0) {
        // Fail
    }
    
    switch (self.flag) {
        case FLAG_WSLOCATION_CREATE:
            if (self.delegate && [self.delegate respondsToSelector:@selector(wsLocation:didCreateLocationSuccess:)]) {
                [self.delegate wsLocation:self didCreateLocationSuccess:nil];
            }
            break;
        case FLAG_WSLOCATION_UPDATE:
            if (self.delegate && [self.delegate respondsToSelector:@selector(wsLocation:didUpdateLocationSuccess:)]) {
                [self.delegate wsLocation:self didUpdateLocationSuccess:nil];
            }
            break;
        case FLAG_WSLOCATION_PLACEBOOKING:{
            [[SBase dataSource] saveBookingToDevice:[result objectForKey:@"data"]];
            if (self.delegate && [self.delegate respondsToSelector:@selector(wsLocation:didPlaceBookingSuccess:)]) {
                [self.delegate wsLocation:self didPlaceBookingSuccess:nil];
            }
        }
            break;
        default:
            break;
    }
}

- (void) _parsePostFail:(NSError *) error
{
    switch (self.flag) {
        case FLAG_WSLOCATION_CREATE:
            
            break;
        case FLAG_WSLOCATION_UPDATE:
            
            break;
        case FLAG_WSLOCATION_PLACEBOOKING:{
            if (self.delegate && [self.delegate respondsToSelector:@selector(wsLocation:didPlaceBookingFail:)]) {
                [self.delegate wsLocation:self didPlaceBookingFail:error];
            }
        }
            break;
        default:
            break;
    }
}

#pragma mark - PRIVATE FUNCTIONS

- (ILocation *) _parseLocation:(NSDictionary *)locationData
                andTranspoters:(NSDictionary *)transportersData
                  andRecommend:(NSDictionary *)recommendData
{
    ILocation *iLocation = [[ILocation alloc] init];
    iLocation.location_id   = [locationData objectForKey:@"id"];
    iLocation.title         = [locationData objectForKey:@"title"];
    iLocation.address       = [locationData objectForKey:@"address"];
    iLocation.latitude      = [[locationData objectForKey:@"latitude"] doubleValue];
    iLocation.longitude     = [[locationData objectForKey:@"longitude"] doubleValue];
    iLocation.category_id   = [locationData objectForKey:@"category_id"];
    
    iLocation.overview      = [locationData objectForKey:@"overview"];
    iLocation.short_description     = [locationData objectForKey:@"short_description"];
    iLocation.email         = [locationData objectForKey:@"email"];
    iLocation.phone         = [locationData objectForKey:@"phone"];
    iLocation.website       = [locationData objectForKey:@"website"];
    
    iLocation.thumbnail     = [locationData objectForKey:@"thumbnail"];
    iLocation.popular       = [locationData objectForKey:@"popular"];
    
    iLocation.suggestion    = [NSNumber numberWithInt:[[locationData objectForKey:@"suggestion"] intValue]];
    
    iLocation.price    = [NSNumber numberWithInt:[[locationData objectForKey:@"price"] intValue]];
    iLocation.reservable    = [NSNumber numberWithInt:[[locationData objectForKey:@"reservable"] intValue]];
    
    iLocation.wysiwyg_content = [locationData objectForKey:@"wysiwyg_content"];
    
    if (transportersData
        && ([transportersData count] > 0)) {
        
        NSMutableArray *transporters = [[NSMutableArray alloc] init];
        for (NSDictionary *transporter in transportersData) {
            
            ITransporter *iTransporter = [[ITransporter alloc] init];
            iTransporter.transporter_id     = [transporter objectForKey:@"id"];
            iTransporter.name               = [transporter objectForKey:@"name"];
            iTransporter.descriptions        = [transporter objectForKey:@"description"];
            iTransporter.type               = [transporter objectForKey:@"type"];
            
            [transporters addObject:iTransporter];
            
        }
        iLocation.tranporters = [[NSArray alloc] initWithArray:[transporters mutableCopy]] ;
        
    }
    
    if (recommendData) {
        iLocation.isRecommended = YES;
    }else{
        iLocation.isRecommended = NO;
    }
    
    if (_clLocation) {
        CLLocationDegrees latitude = iLocation.latitude;
        CLLocationDegrees longitude = iLocation.longitude;
        CLLocation *location = [[CLLocation alloc]initWithLatitude:latitude longitude:longitude];
        CLLocationDistance distance = [_clLocation distanceFromLocation:location];
        
        
        iLocation.distance = [NSString stringWithFormat:@"%.0f", distance];
        //iLocation.distance = [@(distance) stringValue];
    }else{
        iLocation.distance = @"0";
    }
    
    //[self _syncLocationToDevice:iLocation];
    return iLocation;
}

- (void) _syncLocationToDevice:(ILocation *)iLocation
{
    //[[SBase dataSource] saveLocationToDevice:iLocation];
}

- (void) _parseGetListAll:(id)JSON
{
    if ([JSON objectForKey:@"success"]) {
        NSArray *data = [JSON objectForKey:@"data"];
        if ([data count] == 0) {
            
        }else{
            
            for (NSDictionary * location in data) {
                
                NSDictionary *locationData = [location objectForKey:@"Location"];
                [_locations addObject:[self _parseLocation:locationData
                                            andTranspoters:nil
                                              andRecommend:nil]];
            }
            
        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(wsLocation:didListAllLocationsSuccess:)]) {
            [[SBase dataSource] saveLocationsToDevice:[_locations mutableCopy]];
            [self.delegate wsLocation:self didListAllLocationsSuccess:[_locations mutableCopy]];
        }
        
    }else{
        NSLog(@"WSLocation _parseGetSuccess FAIL %@", JSON);
    }
}

- (void) _parseGetLocation:(id)JSON
{
    if ([JSON objectForKey:@"success"]) {
        
        NSDictionary *data = [JSON objectForKey:@"data"];
        if ([[NSString stringWithFormat:@"%@",data] isEqualToString:@"0"]) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(wsLocation:didGetLocationFail:)]) {
                [self.delegate wsLocation:self didGetLocationFail:nil];
            }
        }else{
            NSDictionary *locationData              = [data objectForKey:@"Location"];
            NSDictionary *locationTransporterData   = [data objectForKey:@"LocationTransporter"];
            NSDictionary *locationRecommendData     = [data objectForKey:@"LocationRecommend"];
            
            _iLocation = [self _parseLocation:locationData
                               andTranspoters:locationTransporterData
                                 andRecommend:locationRecommendData];
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(wsLocation:didGetLocationSuccess:)]) {
                [[SBase dataSource]updateLocationToDevice:_iLocation];
                [self.delegate wsLocation:self didGetLocationSuccess:_iLocation];
            }

        }
        
    }else{
        NSLog(@"WSLocation _parseGetSuccess FAIL %@", JSON);
    }
}

- (void) _parseUpdateByCategory: (id)JSON
{
    if ([JSON objectForKey:@"success"]) {
        NSArray *data = [JSON objectForKey:@"data"];
        if ([data count] == 0) {
            
        }else{
            
            for (NSDictionary * location in data) {
                
                NSDictionary *locationData = [location objectForKey:@"Location"];
                [_locations addObject:[self _parseLocation:locationData andTranspoters:nil andRecommend:nil]];
            }
            
        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(wsLocation:didUpdatedByCategorySuccess:)]) {
            [[SBase dataSource] updateLocationsToDevice:[_locations mutableCopy]];
            [self.delegate wsLocation:self didUpdatedByCategorySuccess:[_locations mutableCopy]];
        }
        
    }else{
        NSLog(@"WSLocation _parseUpdateByCategory FAIL %@", JSON);
    }
}

- (void) _parseGetSuggestion:(id)JSON
{
    if ([JSON objectForKey:@"success"]) {
        NSLog(@"WSLocation _parseGetSuggestion");
        NSArray *data = [JSON objectForKey:@"data"];
        if ([data count] == 0) {
            
        }else{
            
            for (NSDictionary * location in data) {
                
                NSDictionary *locationData = [location objectForKey:@"Location"];
                [_locations addObject:[self _parseLocation:locationData
                                            andTranspoters:nil
                                              andRecommend:nil]];
            }
            
        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(wsLocation:didGetSuggestionSuccess:)]) {
            [[SBase dataSource] updateSuggestionsToDevice:[_locations mutableCopy]];
            [self.delegate wsLocation:self didGetSuggestionSuccess:[_locations mutableCopy]];
        }
        
    }else{
        NSLog(@"WSLocation _parseGetSuggestion FAIL %@", JSON);
    }
}

- (void) _parseGetFilter:(id)JSON
{
    if ([JSON objectForKey:@"success"]) {
        NSLog(@"WSLocation _parseGetFilter");
        NSArray *data = [JSON objectForKey:@"data"];
        if ([data count] == 0) {
            
        }else{
            
            for (NSDictionary * location in data) {
                
                NSDictionary *locationData = [location objectForKey:@"Location"];
                [_locations addObject:[self _parseLocation:locationData
                                            andTranspoters:nil
                                              andRecommend:nil]];
            }
            
        }
        
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(wsLocation:didFilterSuccess:)]) {
            [[SBase dataSource] updateSuggestionsToDevice:[_locations mutableCopy]];
            [self.delegate wsLocation:self didFilterSuccess:[_locations mutableCopy]];
        }
        
        
        
    }else{
        NSLog(@"WSLocation _parseGetFilter FAIL %@", JSON);
    }
}

@end
