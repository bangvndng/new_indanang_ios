//
//  WSCategoryDetail.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 2/9/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSBase.h"
@protocol WSCategoryDetailDelegate;

@interface WSCategoryDetail : WSBase

- (void) getCategoryDetail;
- (void) getCategoryDetailForCategory:(NSString *)category_id;

@property (weak, nonatomic) id<WSCategoryDetailDelegate> delegate;

@end

@protocol WSCategoryDetailDelegate <NSObject>

@optional

- (void)wsCategoryDetail:(WSCategoryDetail *)wsCategoryDetail didGetCategoryDetailSuccess:(NSDictionary *)info;
- (void)wsCategoryDetail:(WSCategoryDetail *)wsCategoryDetail didGetCategoryDetailFail:(NSError *)error;

- (void)wsCategoryDetail:(WSCategoryDetail *)wsCategoryDetail didGetCategoryDetailForCategorySuccess:(NSDictionary *)info;
- (void)wsCategoryDetail:(WSCategoryDetail *)wsCategoryDetail didGetCategoryDetailForCategoryFail:(NSError *)error;

@end
