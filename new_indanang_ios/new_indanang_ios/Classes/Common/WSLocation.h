//
//  WSLocation.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/18/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSBase.h"
#import "ILocation.h"

@protocol WSLocationDelegate;

@interface WSLocation : WSBase

@property (weak, nonatomic) id<WSLocationDelegate> delegate;

- (void) getSuggestion;

- (void) getLocation:(NSString *)locationId;

- (void) listLocationsByCategoryId: (NSString *)categoryId;

- (void) listAllLocations;

- (void) updateLocationsByCategory: (NSString *)categoryId;

- (void) createLocation:(NSDictionary *)locationInfo;

- (void) updateLocation:(NSDictionary *)locationInfo;

- (void) filterLocations:(NSString *)checkin andCheckout:(NSString *)checkout andQuantity:(NSString *)quantity;

- (void) placeBooking:(NSString *)checkin andCheckout:(NSString *)checkout andQuantity:(NSString *)quantity forLocationId:(NSString *)location_id;

@end

@protocol WSLocationDelegate <NSObject>

@optional

- (void)wsLocation:(WSLocation *)wsLocation didGetSuggestionSuccess:(NSArray *)locations;
- (void)wsLocation:(WSLocation *)wsLocation didGetSuggestionFail:(NSError *)error;

- (void)wsLocation:(WSLocation *)wsLocation didListAllLocationsSuccess:(NSArray *)locations;
- (void)wsLocation:(WSLocation *)wsLocation didListAllLocationsFail:(NSError *)error;

- (void)wsLocation:(WSLocation *)wsLocation didGetLocationSuccess:(ILocation *)location;
- (void)wsLocation:(WSLocation *)wsLocation didGetLocationFail:(NSError *)error;

- (void)wsLocation:(WSLocation *)wsLocation didUpdatedByCategorySuccess:(NSArray *)locations;
- (void)wsLocation:(WSLocation *)wsLocation didUpdatedByCategoryFail:(NSError *)error;

- (void)wsLocation:(WSLocation *)wsLocation didCreateLocationSuccess:(ILocation *)location;
- (void)wsLocation:(WSLocation *)wsLocation didCreateLocationFail:(NSError *)error;

- (void)wsLocation:(WSLocation *)wsLocation didUpdateLocationSuccess:(ILocation *)location;
- (void)wsLocation:(WSLocation *)wsLocation didUpdateLocationFail:(NSError *)error;

- (void)wsLocation:(WSLocation *)wsLocation didFilterSuccess:(NSArray *)locations;
- (void)wsLocation:(WSLocation *)wsLocation didFilterFail:(NSError *)error;

- (void)wsLocation:(WSLocation *)wsLocation didPlaceBookingSuccess:(NSDictionary *)info;
- (void)wsLocation:(WSLocation *)wsLocation didPlaceBookingFail:(NSError *)error;

@end