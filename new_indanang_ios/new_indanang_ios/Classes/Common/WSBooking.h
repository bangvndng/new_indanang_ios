//
//  WSBooking.h
//  new_indanang_ios
//
//  Created by Vu Ngoc Bang on 3/21/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSBase.h"

@protocol WSBookingDelegate;

@interface WSBooking : WSBase

@property (weak, nonatomic) id<WSBookingDelegate> delegate;

- (void) getAllBookings;

- (void) checkAvailabilityForLocation:(NSString *)location_id forCheckin:(NSString *)checkin andCheckout:(NSString *)checkout andQuantity:(NSString *)quantity;

@end

@protocol WSBookingDelegate <NSObject>

@optional

- (void)wsBooking:(WSBooking *)wsBooking didGetAllBookingsSuccess:(NSArray *)bookings;
- (void)wsBooking:(WSBooking *)wsBooking didGetAllBookingsFail:(NSError *)error;

- (void)wsBooking:(WSBooking *)wsBooking didCheckAvailabilityForLocationSuccess:(NSArray *)bookings;
- (void)wsBooking:(WSBooking *)wsBooking didCheckAvailabilityForLocationFail:(NSError *)error;

@end
