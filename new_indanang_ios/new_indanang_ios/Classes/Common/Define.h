//
//  Define.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/12/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#ifndef new_indanang_ios_Define_h
#define new_indanang_ios_Define_h

#define     APP_NAME                    @"inDaNang"

// WS URL SETTINGS

#define     COMMON_WS_URL               @"http://local.new_indanang_server.com/api"
//#define     COMMON_WS_URL               @"http://192.168.43.81/api"

//#define     COMMON_WS_URL               @"http://indanang.vn/api"

//#define     COMMON_WS_URL               @"http://192.168.200.5/api"
//#define     COMMON_WS_URL               @"http://192.168.43.10/api"


#define     URL_WS_USER_LOGIN           @"users/loginWithoutAnonymousUserInfo"
#define     URL_WS_USER_REGISTER        @"users/createWithoutAnonymousUserInfo"
#define     URL_WS_USER_LOGINFB         @"users/loginWithFacebook"


#define     URL_WS_LOCATIONDETAIL_INDEX         @"locationdetails/admin_index"
#define     URL_WS_CATEGORYDETAIL_INDEX         @"categorydetails/admin_index"

#define     URL_WS_LOCATION_GET_SUGGESTIONS     @"locations/suggestions/"
#define     URL_WS_LOCATION_LIST_ALL            @"locations/"
#define     URL_WS_LOCATION_LOCATION            @"locations/view/"
#define     URL_WS_LOCATION_UPDATE_CATEGORY     @"locations/listLocationsByCategoryId"
#define     URL_WS_LOCATION_CREATE              @"locations/create"
#define     URL_WS_LOCATION_UPDATE              @"locations/admin_edit"
#define     URL_WS_LOCATION_FILTER              @"bookings/filters"

#define     URL_WS_LOCATION_PLACEBOOKING        @"bookings/create"

#define     URL_WS_BOOKING_GET_ALL              @"bookings/index"
#define     URL_WS_BOOKING_CHECK                @"bookings/check"

#define     URL_WS_PHOTO_UPLOAD         @"photos/upload"
#define     URL_WS_PHOTO_LIST           @"photos/listPhotos"

#define     URL_WS_CATEGORY_VIEW        @"categories/view"
#define     URL_WS_CATEGORY_LIST_ALL    @"categories/"

#define     URL_WS_TIP_ADD              @"tips/create"

#define     URL_WS_RECOMMEND_ADD        @"recommends/create"

#define     URL_WS_TIMELINE_LIST_ALL    @"timelines/"

#define     URL_WS_FORECAST             @"https://api.forecast.io/forecast"
#define     WS_FORECAST_API_KEY         @"179a46d1b6641187df17081c0328af34"

#define     URL_WS_CMS                  @"contents"

// FLAGS

// MENU FLAGS
#define     CENTERVC_MENUSTATE_CLOSE    0
#define     CENTERVC_MENUSTATE_OPEN     1

// WS FLAGS
#define     FLAG_WSBASE_WSUSER_POST_LOGIN       1
#define     FLAG_WSBASE_WSUSER_POST_REGISTER    2
#define     FLAG_WSBASE_WSUSER_POST_LOGINFB     3

#define     FLAG_WSLOCATION_GET_SUGGESTION      5
#define     FLAG_WSLOCATION_LIST_ALL            1
#define     FLAG_WSLOCATION_LOCATION            2
#define     FLAG_WSLOCATION_UPDATE_CATEGORY     3
#define     FLAG_WSLOCATION_CREATE              4
#define     FLAG_WSLOCATION_UPDATE              5
#define     FLAG_WSLOCATION_FILTER              6

#define     FLAG_WSLOCATION_PLACEBOOKING        7

#define     FLAG_WSBOOKING_GET_ALL              1
#define     FLAG_WSBOOKING_CHECK                2


#define     FLAG_WSCATEGORY_LIST_ALL            1
#define     FLAG_WSCATEGORY_VIEW                2

#define     FLAG_WSTIP_ADD                      1
#define     FLAG_WSRECOMMEND_ADD                1

#define     FLAG_WSTIMELINE_LIST_ALL            1

// MAP FLAGS
#define MAP_TYPE_DEFAULT            0
#define MAP_TYPE_SINGLE             1
#define MAP_TYPE_CATEGORY_LIST      2
#define MAP_TYPE_CATEGORY_DETAIL    3
#define MAP_TYPE_SUGGESTION         4
#define MAP_TYPE_CATEGORY_FILTER    5

// Location come from:
#define DETAIL_FLAG_FROM_CATEGORY   0
#define DETAIL_FLAG_FROM_MAP        1
#define DETAIL_FLAG_FROM_SUGGESTION 2
#define DETAIL_FLAG_FROM_TIMELINE   3
#define DETAIL_FLAG_FROM_BOOKMARK   4

// FB FLAGS
#define     FLAG_FB_REQUEST_LOGIN               1
#define     FLAG_FB_REQUEST_READ_FRIENDLIST     2

// MESSAGES

// ALERTS

// UI SETTINGS
#define     REFRESH_HEADER_HEIGHT 52.0f

#define     CATEGORY_HOTEL_ID @"7"

#define DATE_FORMAT @"yyyy-MM-dd"

#endif