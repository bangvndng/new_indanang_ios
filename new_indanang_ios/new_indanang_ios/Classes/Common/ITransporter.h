//
//  ITransporter.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/19/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ITransporter : NSObject

@property (strong, nonatomic) NSString *transporter_id;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *descriptions;
@property (strong, nonatomic) NSString *type;

@end
