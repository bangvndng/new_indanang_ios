//
//  WSCategory.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/18/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSBase.h"
#import "ICategory.h"

@protocol WSCategoryDelegate;

@interface WSCategory : WSBase

@property (weak, nonatomic) id<WSCategoryDelegate> delegate;

- (void) getCategory:(NSString *) category_id;
- (void) getAllCategories;

@end

@protocol WSCategoryDelegate <NSObject>

@optional

- (void)wsCategory:(WSCategory *)wsCategory didGetCategorySuccess:(NSDictionary *)category;
- (void)wsCategory:(WSCategory *)wsCategory didGetCategoryFail:(NSError *)error;

- (void)wsCategory:(WSCategory *)wsCategory didGetAllCategoriesSuccess:(NSArray *)categories;
- (void)wsCategory:(WSCategory *)wsCategory didGetAllCategoriesFail:(NSError *)error;

@end