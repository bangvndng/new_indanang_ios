//
//  SAccount.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/12/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "SAccount.h"
#import "Define.h"

@interface SAccount(){
    
    int _fb_request_type;
    
}

@end

@implementation SAccount

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)dealloc
{
    self.delegate = nil;
}

//static int _fb_request_type;
static IUser *currentUser = nil;

#pragma mark - PUBLIC PROPERTIES AND FUNCTIONS

+ (IUser *)currentUser
{
    if (currentUser == nil)
        currentUser = [[IUser alloc] init];
    return currentUser;
}

- (void) logout
{
    [currentUser updateCurrentUser:nil];
    currentUser = nil;
}

- (void) authenticateFB
{
    NSLog(@"SAccount authenticateFB");
    [self doOpenSession];
}

#pragma mark - PRIVATE PROPERTIES AND FUNCTIONS

#pragma mark - Facebook SDK

- (void)sessionStateChanged:(FBSession *)session
                      state:(FBSessionState) state
                      error:(NSError *)error
{
    switch (state) {
        case FBSessionStateOpen: {
            NSLog(@"SAccount sessionStateChanged FBSessionStateOpen");
            switch (_fb_request_type) {
                case FLAG_FB_REQUEST_LOGIN:
                {
                    [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary *user, NSError *error) {
                        if (!error) {
                            NSLog(@"SAccount sessionStateChanged FBSessionStateOpen !error");
                            dispatch_async(dispatch_get_main_queue(), ^{
                                if (self.delegate && [self.delegate respondsToSelector:@selector(saccount:didAuthenticateFBSuccess:)]) {
                                    [self.delegate saccount:self didAuthenticateFBSuccess:user];
                                }else{
                                    NSLog(@"SAccount sessionStateChanged delegate not responds to selector");
                                }
                            });
                            
                        }
                    }];
                }
                    break;
                case FLAG_FB_REQUEST_READ_FRIENDLIST:
                {
                    /*
                    if ([Common user].fbid) {
                        [self returnFriendsFromFB];
                    }else{
                        [[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection *connection, NSDictionary *user, NSError *error){
                            NSString *email = [user objectForKey:@"email"];
                            NSString *fbid  = [user objectForKey:@"id"];
                            NSString *displayname = [user objectForKey:@"name"];
                            
                            NSArray *key            = @[@"email", @"fbid", @"displayname"];
                            NSArray *value          = @[email, fbid, displayname];
                            
                            NSDictionary *userInfo  = [NSDictionary dictionaryWithObjects:value forKeys:key];
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                LoginWS *loginWS        = [[LoginWS alloc] init];
                                loginWS.delegate        = self;
                                [loginWS loginFBWithouAnnonimousUserInfo:userInfo];
                            });
                            
                            
                            
                        }];
                    }
                     */
                }
                    break;
                default:
                    break;
            }
        }
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            NSLog(@"FBSessionStateClosed");
            // Once the user has logged in, we want them to
            // be looking at the root view.
            switch (_fb_request_type) {
                case FLAG_FB_REQUEST_LOGIN:
                    
                    if (self.delegate && [self.delegate respondsToSelector:@selector(saccount:didAuthenticateFBFail:)]) {
                        [self.delegate saccount:self didAuthenticateFBFail:nil];
                    }
                    break;
                case FLAG_FB_REQUEST_READ_FRIENDLIST:
                    /*
                    if (self.fbLoginDelegate && [self.fbLoginDelegate respondsToSelector:@selector(didGetFriendsFBFailWithError:)]) {
                        [self.fbLoginDelegate didGetFriendsFBFailWithError:error];
                    }
                    */
                    break;
                default:
                    break;
            }
            
            break;
        default:
            break;
    }
    
    if (error) {
        NSLog(@"SAccount sessionStateChanged %@", error);
    }
}

- (void)openSession
{
    
    NSArray *permissions =
    [NSArray arrayWithObjects:@"email", @"read_friendlists", nil];
    
    [FBSession openActiveSessionWithReadPermissions:permissions
                                       allowLoginUI:YES
                                  completionHandler:
     ^(FBSession *session,
       FBSessionState state, NSError *error) {
         
         [self sessionStateChanged:session state:state error:error];
     }];
}

- (void)doOpenSession
{
    _fb_request_type = FLAG_FB_REQUEST_LOGIN;
    
    [self openSession];
}

- (void)returnFriendsFromFB
{
    FBRequest* friendsRequest = [FBRequest requestForMyFriends];
    [friendsRequest startWithCompletionHandler: ^(FBRequestConnection *connection,
                                                  NSDictionary* result,
                                                  NSError *error) {
        /*
        NSArray* friends = [result objectForKey:@"data"];
        
        if (!error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self.fbLoginDelegate && [self.fbLoginDelegate respondsToSelector:@selector(didGetFriendsFBSuccessWith:)]) {
                    [self.fbLoginDelegate didGetFriendsFBSuccessWith:friends];
                }
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                if (self.fbLoginDelegate && [self.fbLoginDelegate respondsToSelector:@selector(didGetFriendsFBFailWithError:)]) {
                    [self.fbLoginDelegate didGetFriendsFBFailWithError:error];
                }
            });
        }
        */
    }];
}



@end
