//
//  IPhoto.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/18/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IPhoto : NSObject

@property (strong, nonatomic) NSString *photo_id;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *path;
@property (strong, nonatomic) NSString *location_id;
@property (strong, nonatomic) NSString *user_id;

@end
