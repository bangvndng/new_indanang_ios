//
//  SAccount.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/12/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IUser.h"

@protocol SAccountDelegate;

@interface SAccount : NSObject

@property (weak, nonatomic) id<SAccountDelegate> delegate;

+ (IUser *)currentUser;

- (void) logout;
- (void) authenticateFB;

@end

@protocol SAccountDelegate <NSObject>

@optional

- (void)saccount:(SAccount *)saccount didAuthenticateFBSuccess:(NSDictionary *)fbInfo;
- (void)saccount:(SAccount *)saccount didAuthenticateFBFail:(NSError *)error;

- (void)saccount:(SAccount *)saccount didGetFriendsFBSuccess:(NSDictionary *)fbInfo;
- (void)saccount:(SAccount *)saccount didGetFriendsFBFail:(NSError *)error;


@end