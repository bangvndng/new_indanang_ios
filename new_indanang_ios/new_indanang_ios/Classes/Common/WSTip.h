//
//  WSTip.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/18/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSBase.h"

@protocol WSTipDelegate;

@interface WSTip : WSBase

@property (weak, nonatomic) id <WSTipDelegate> delegate;

- (void)addTip:(NSDictionary *)tipInfo;

@end

@protocol WSTipDelegate <NSObject>

@optional

- (void)wsTip:(WSTip *)wsTip didAddTipSuccess:(NSDictionary *)info;
- (void)wsTip:(WSTip *)wsTip didAddTipFail:(NSError *)error;

@end