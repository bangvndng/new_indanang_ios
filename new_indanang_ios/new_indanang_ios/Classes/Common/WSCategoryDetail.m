//
//  WSCategoryDetail.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 2/9/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import "WSCategoryDetail.h"
#import "Define.h"

#import "SBase.h"

#define FLAG_GET_ALL 0
#define FLAG_GET_ONE 1

@interface WSCategoryDetail(){
    NSMutableArray *_categoryDetails;
    
    int _flag;
    NSString *_category_id;
}
@end

@implementation WSCategoryDetail

- (id)init
{
    self = [super init];
    if (self) {
        _categoryDetails = [[NSMutableArray alloc] init];
        
        _flag = FLAG_GET_ALL;
    }
    return self;
}

- (void)dealloc
{
    self.delegate = nil;
    
    _categoryDetails = nil;
    _category_id = nil;
    
}

- (void) getCategoryDetail
{
    NSArray *keys   = @[@"page", @"limit"];
    NSArray *values = @[@"1", @"99999"];
    NSDictionary *getParams = [NSDictionary dictionaryWithObjects:values forKeys:keys];
    
    _flag = FLAG_GET_ALL;
    
    [self get:URL_WS_CATEGORYDETAIL_INDEX andGetParams:getParams];

}

- (void) getCategoryDetailForCategory:(NSString *)category_id
{
    NSArray *keys   = @[@"page", @"limit"];
    NSArray *values = @[@"1", @"99999"];
    NSDictionary *getParams = [NSDictionary dictionaryWithObjects:values forKeys:keys];
    
    NSString *urlString = [NSString stringWithFormat:@"%@?node=%@",URL_WS_CATEGORYDETAIL_INDEX, category_id];
    
    _flag = FLAG_GET_ONE;
    _category_id = category_id;
    
    [self get:urlString andGetParams:getParams];
}

#pragma mark - INHERITANCE FUNCTIONS

- (void) _parseGetSuccess:(id) JSON
{
    if ([JSON objectForKey:@"success"]) {
        NSArray *data = [JSON objectForKey:@"data"];
        if ([data count] == 0) {
            
        }else{
            
            if (_flag == FLAG_GET_ONE) {
                for (NSDictionary * location in data) {
                    [_categoryDetails addObject:location];
                }
            }else{
                for (NSDictionary * location in data) {
                    NSDictionary *locationDetailData = [location objectForKey:@"Categorydetail"];
                    [_categoryDetails addObject:locationDetailData];
                }
            }
            
            
            
        }
        
        if (_flag == FLAG_GET_ONE) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(wsCategoryDetail:didGetCategoryDetailForCategorySuccess:)]) {
                [[SBase dataSource] saveCategoryDetailsToDevice:[_categoryDetails mutableCopy] forCategoryId:_category_id];
                [self.delegate wsCategoryDetail:self didGetCategoryDetailForCategorySuccess:[_categoryDetails mutableCopy]];
            }
        }else{
            if (self.delegate && [self.delegate respondsToSelector:@selector(wsCategoryDetail:didGetCategoryDetailSuccess:)]) {
                //[[SBase dataSource] saveLocationsToDevice:[_locations mutableCopy]];
                [[SBase dataSource] saveCategoryDetailsToDevice:_categoryDetails];
                [self.delegate wsCategoryDetail:self didGetCategoryDetailSuccess:[_categoryDetails mutableCopy]];
            }
        }
        
        
        
    }else{
        NSLog(@"WSCategoryDetail _parseGetSuccess FAIL %@", JSON);
    }
    
    
}

- (void) _parseGetFail:(NSError *) error
{
    NSLog(@"WSCategoryDetail _parseGetFail %@", error);
}

@end