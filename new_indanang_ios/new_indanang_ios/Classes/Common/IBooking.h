//
//  IBooking.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 3/14/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IBooking : NSObject

@property (nonatomic, strong) NSString * booking_id;
@property (nonatomic, strong) NSString * user_id;
@property (nonatomic, strong) NSString * checkin;
@property (nonatomic, strong) NSString * checkout;
@property (nonatomic, strong) NSNumber * quantity;
@property (nonatomic, strong) NSString * location_id;
@property (nonatomic, strong) NSString * created;

@end
