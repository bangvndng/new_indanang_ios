//
//  WSTip.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/18/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "WSTip.h"
#import "Define.h"

@implementation WSTip

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)dealloc
{
    self.delegate = nil;
}

- (void)addTip:(NSDictionary *)tipInfo
{
    self.flag = FLAG_WSTIP_ADD;
    [self post:URL_WS_TIP_ADD andPostParams:tipInfo];
}

// ABSTRACT FUNCTION
- (void) _parsePostSuccess:(id) JSON
{
    NSDictionary *result = [JSON copy];
    
    NSLog(@"WSTIP _parsePostSuccess JSON %@", result);
    
    if ([result objectForKey:@"success"] == 0) {
        // Fail
    }
    
    switch (self.flag) {
        case FLAG_WSTIP_ADD:
            if (self.delegate && [self.delegate respondsToSelector:@selector(wsTip:didAddTipSuccess:)]) {
                [self.delegate wsTip:self didAddTipSuccess:nil];
            }
            break;
        default:
            break;
    }
}

// ABSTRACT FUNCTION
- (void) _parsePostFail:(NSError *) error
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(wsTip:didAddTipFail:)]) {
        [self.delegate wsTip:self didAddTipFail:error];
    }
}

@end
