//
//  WSBase.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/12/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "WSBase.h"
#import "Define.h"
#import "SBase.h"

#import "UIImage+UIImageFunctions.h"

@implementation WSBase

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)dealloc
{
    
}

#pragma mark - ATTACHUSER INFORMATION
- (NSString *)_attachUserInfoForUrlString: (NSString *)urlString
{
    NSLog(@"WSBase _attachUserInfoForUrlString");
    if ([SAccount currentUser].isLoggedIn) {
        urlString = [NSString stringWithFormat:@"%@?user_id=%@&token=%@",
                     urlString,
                     [SAccount currentUser].user_id,
                     [SAccount currentUser].password
                     ];
    }
    
    return urlString;
}

#pragma mark - PERFORM GET REQUEST

- (void)get:(NSString *)url andGetParams:(NSDictionary *)getParams
{
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", COMMON_WS_URL, url];
    urlString = [self _attachUserInfoForUrlString:urlString];
    
    if ([getParams.allKeys count] == 0) {
        
    }else{
        if ([SAccount currentUser].isLoggedIn) {
            for (NSString* key in getParams.allKeys) {
                urlString = [NSString stringWithFormat:@"%@&%@=%@", urlString, key, [getParams objectForKey:key]];
            }
        }else{
            int count = 0;
            for (NSString* key in getParams.allKeys) {
                if (count == 0) {
                    urlString = [NSString stringWithFormat:@"%@?%@=%@", urlString, key, [getParams objectForKey:key]];
                }else{
                    urlString = [NSString stringWithFormat:@"%@&%@=%@", urlString, key, [getParams objectForKey:key]];
                }
                count++;
            }
        }
        
    }
    
    NSLog(@"WSBase get urlString %@", urlString);
    
    NSURL *nsUrl = [[NSURL alloc] initWithString:urlString];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    NSURLRequest *afRequest = [NSURLRequest requestWithURL:nsUrl];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:afRequest
                                                    success:^(NSURLRequest *afRequest, NSHTTPURLResponse *response, id JSON) {
                                                        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                                        [self _parseGetSuccess:JSON];
                                                    }
                                                    failure:^(NSURLRequest *afRequest, NSHTTPURLResponse *response, NSError *error, id JSON){
                                                        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                                        [self _parseGetFail:error];
                                                    }];
    [operation start];
    
}

// ABSTRACT FUNCTION
- (void) _parseGetSuccess:(id) JSON
{
    
}

// ABSTRACT FUNCTION
- (void) _parseGetFail:(NSError *) error
{
    
}

# pragma mark - PERFORM POST REQUEST

- (void)post:(NSString *)url andPostParams:(NSDictionary *)postParams
{
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", COMMON_WS_URL, url];
    urlString = [self _attachUserInfoForUrlString:urlString];
    
    NSURL *nsUrl = [[NSURL alloc] initWithString:urlString];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    NSLog(@"WSBase post url %@", urlString);
    
    AFHTTPClient * Client = [[AFHTTPClient alloc] initWithBaseURL:nsUrl];
    Client.parameterEncoding = AFJSONParameterEncoding;
    [Client setDefaultHeader:@"Accept" value:@"text/json"];
    [Client registerHTTPOperationClass:[AFJSONRequestOperation class]];
    
    [Client setParameterEncoding:AFJSONParameterEncoding];
    [Client postPath:@""
          parameters:postParams
             success:^(AFHTTPRequestOperation *operation, id JSON) {
                 [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                 [self _parsePostSuccess:JSON];
             }
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                 [self _parsePostFail:error];
             }];
}

// ABSTRACT FUNCTION
- (void) _parsePostSuccess:(id) JSON
{

}

// ABSTRACT FUNCTION
- (void) _parsePostFail:(NSError *) error
{
    
}

# pragma mark - PERFORM MULTIPARTFORM REQUEST

- (void)multiPartsFormPost:(NSString *)url andParams:(NSDictionary *)params andFormParts:(NSArray *)formParts
{
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", COMMON_WS_URL, url];
    urlString = [self _attachUserInfoForUrlString:urlString];
    
    NSURL *nsUrl = [[NSURL alloc] initWithString:urlString];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    
    NSLog(@"WSBase post url %@", urlString);
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:nsUrl];
    NSMutableURLRequest *afRequest = [httpClient multipartFormRequestWithMethod:@"POST" path:nil parameters:params constructingBodyWithBlock: ^(id <AFMultipartFormData>formData) {
        int count = 0;
        if([formParts count] > 0){
            for(NSData *aFoo in formParts) {
                count++;
                UIImage *uiImage = [UIImage imageWithData:aFoo];
                //uiImage = [uiImage scaleToSize:CGSizeMake(640, 480)];
                NSData *dataImageAva = UIImageJPEGRepresentation(uiImage, 0.6);
                NSString *imageName = [NSString stringWithFormat:@"%@",[NSDate date]];
                [formData appendPartWithFileData:dataImageAva
                                            name:[NSString stringWithFormat:@"photo-path-%i",count]
                                        fileName:[NSString stringWithFormat:@"%@.jpeg",imageName]
                                        mimeType:@"image/jpeg"];
            }
        }
    }];
    
    AFJSONRequestOperation *operation = [AFJSONRequestOperation
                                         JSONRequestOperationWithRequest:afRequest
                                         success:^(NSURLRequest *afRequest, NSHTTPURLResponse *response, id JSON) {
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             
                                             [self _parseMultiPartFormPostSuccess:JSON];
                                         }
                                         failure:^(NSURLRequest *afRequest, NSHTTPURLResponse *response, NSError *error, id JSON){
                                             [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                             NSLog(@"%@",error);
                                             [self _parseMultiPartFormPostFail:error];
                                         }];
    [operation start];
}

// ABSTRACT FUNCTION
- (void) _parseMultiPartFormPostSuccess:(id) JSON
{
    
}

// ABSTRACT FUNCTION
- (void) _parseMultiPartFormPostFail:(NSError *) error
{
    
}

- (void)customGet:(NSString *)urlString
{
    NSLog(@"WSBase get urlString %@", urlString);
    
    NSURL *nsUrl = [[NSURL alloc] initWithString:urlString];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    NSURLRequest *afRequest = [NSURLRequest requestWithURL:nsUrl];
    AFJSONRequestOperation *operation =
    [AFJSONRequestOperation JSONRequestOperationWithRequest:afRequest
                                                    success:^(NSURLRequest *afRequest, NSHTTPURLResponse *response, id JSON) {
                                                        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                                        [self _parseGetSuccess:JSON];
                                                    }
                                                    failure:^(NSURLRequest *afRequest, NSHTTPURLResponse *response, NSError *error, id JSON){
                                                        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
                                                        [self _parseGetFail:error];
                                                    }];
    [operation start];
    
}

@end
