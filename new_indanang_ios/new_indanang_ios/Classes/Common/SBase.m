//
//  SBase.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/12/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "SBase.h"

@implementation SBase

static __weak AppDelegate *appDelegate = nil;

static SAccount *currentAccount = nil;
static SData *dataSource = nil;

+(AppDelegate *)appDelegate
{
    if (appDelegate == nil) {
        appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    }
    
    return appDelegate;
}

+(SAccount *)currentAccount
{
    if (currentAccount == nil) {
        currentAccount = [[SAccount alloc] init];
    }
    
    return currentAccount;
}

+(SData *)dataSource
{
    if (dataSource == nil) {
        dataSource = [[SData alloc] init];
    }
    
    return dataSource;
}

+(void) showAlert:(NSString *)alertString
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:APP_NAME
                                                    message:alertString
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles: nil];
    [alert show];
}

@end
