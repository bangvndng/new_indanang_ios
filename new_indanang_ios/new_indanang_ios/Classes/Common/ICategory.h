//
//  ICategory.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/18/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ICategory : NSObject

@property (strong, nonatomic) NSString *category_id;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *short_description;
@property (strong, nonatomic) NSString *overview;
@property (strong, nonatomic) NSString *parent_category_id;
@property (strong, nonatomic) NSString *icon_class;
@property (strong, nonatomic) NSNumber *weight;
@property (strong, nonatomic) NSNumber *hasLocation;
@property (strong, nonatomic) NSString *wysiwyg_content;

@end
