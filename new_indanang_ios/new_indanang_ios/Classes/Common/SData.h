//
//  SData.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/29/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICategory.h"
#import "ILocation.h"

@protocol SDataDelegate;

@interface SData : NSObject

@property (weak, nonatomic) id<SDataDelegate> delegate;

- (void) loadAllLocations:(BOOL) forceReload;

- (void) loadLocationDetails;
- (void) loadCategoryDetails;

- (ILocation *) loadLocationById:(NSString *) location_id;

- (NSArray *) loadLocationsByCategoryId:(NSString *)category_id;
- (NSArray *) loadLocationsBySuggestion;

- (BOOL) isBookmarked:(NSString *)location_id;
- (void) addBookmark:(NSString *)location_id;

- (void) saveLocationToDevice:(ILocation *)iLocation;
- (void) saveLocationsToDevice:(NSArray *)locations;

- (void) updateLocationsToDevice:(NSArray *)locations;
- (void) updateLocationToDevice:(ILocation *)iLocation;

- (void) updateSuggestionsToDevice:(NSArray *)locations;

- (void) saveLocationDetailsToDevice:(NSArray *)locationDetails;
- (void) saveLocationDetailsToDevice:(NSArray *)locationDetails forLocationId:(NSString *)location_id;

- (void) saveCategoryDetailsToDevice:(NSArray *)categoryDetails;
- (void) saveCategoryDetailsToDevice:(NSArray *)categoryDetails forCategoryId:(NSString *)category_id;


- (NSArray *) loadAllCategories;
- (void) loadAllCategories:(BOOL) forceReload;
- (NSArray *) loadLeafCategories;
- (ICategory *) loadCategoryById:(NSString *) category_id;
- (NSArray *) loadChildrenCategoriesById:(NSString *) category_id;

- (void) saveCategoriesToDevice:(NSArray *)categories;
- (void) updateCategorytoDevice:(ICategory *)iCategory;

- (NSArray *) loadLocationDetailsByLocationId:(NSString *) location_id;
- (NSArray *) loadCategoryDetailsByCategoryId:(NSString *) category_id;

- (void) loadWSLocationDetailsForLocationId:(NSString *)location_id;
- (void) loadWSCategoryDetailsByCategoryId:(NSString *)category_id;

- (void) saveBookingToDevice:(NSDictionary *)bookingInfo;
- (void) saveBookingsToDevice:(NSArray *)bookings;
- (NSArray *) getBookingsForUser:(NSString *)user_id andLocation:(NSString *)location_id forDay:(NSString *)checkin;

- (void) loadAllBooking:(BOOL)forceReload;

@end

@protocol SDataDelegate <NSObject>

@optional

- (void)sData:(SData *)sData didLoadAllLocationsSuccess:(NSArray *)locations;
- (void)sData:(SData *)sData didLoadAllLocationsFail:(NSError *)error;

- (void)sData:(SData *)sData didLoadAllCategoriesSuccess:(NSArray *)categories;
- (void)sData:(SData *)sData didLoadAllCategoriesFail:(NSError *)error;

- (void)sData:(SData *)sData didLoadLocationDetailsSuccess:(NSArray *)locationDetails;
- (void)sData:(SData *)sData didLoadLocationDetailsFail:(NSError *)error;

- (void)sData:(SData *)sData didLoadCategoryDetailsSuccess:(NSArray *)categoryDetails;
- (void)sData:(SData *)sData didLoadCategoryDetailsFail:(NSError *)error;

- (void)sData:(SData *)sData didLoadAllBookingsSuccess:(NSArray *)bookings;
- (void)sData:(SData *)sData didLoadAllBookingsFail:(NSError *)error;

@end
