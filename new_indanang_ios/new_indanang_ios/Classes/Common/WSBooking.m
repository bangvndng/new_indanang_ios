//
//  WSBooking.m
//  new_indanang_ios
//
//  Created by Vu Ngoc Bang on 3/21/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import "WSBooking.h"

#import "Define.h"
#import "SBase.h"

@interface WSBooking(){
    NSMutableArray *_bookings;
}

@end

@implementation WSBooking

- (id)init
{
    self = [super init];
    if (self) {
        _bookings = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc
{
    self.delegate = nil;
    _bookings = nil;
}

#pragma mark - Pubic function

- (void)getAllBookings{
    NSArray *keys   = @[@"page", @"limit"];
    NSArray *values = @[@"1", @"99999"];
    NSDictionary *getParams = [NSDictionary dictionaryWithObjects:values forKeys:keys];
    
    self.flag = FLAG_WSBOOKING_GET_ALL;
    
    [self get:URL_WS_BOOKING_GET_ALL andGetParams:getParams];
}

- (void) checkAvailabilityForLocation:(NSString *)location_id forCheckin:(NSString *)checkin andCheckout:(NSString *)checkout andQuantity:(NSString *)quantity
{
    NSArray *keys   = @[@"checkin", @"checkout", @"quantity", @"location_id"];
    NSArray *values = @[checkin, checkout, quantity, location_id];
    
    NSDictionary *getParams = [NSDictionary dictionaryWithObjects:values forKeys:keys];
    
    self.flag = FLAG_WSBOOKING_CHECK;
    
    [self get:URL_WS_BOOKING_CHECK andGetParams:getParams];
}

#pragma mark - Parse

- (void) _parseGetSuccess:(id) JSON
{
    switch (self.flag) {
        case FLAG_WSBOOKING_GET_ALL:
            [self _parseGetGetAll:JSON];
            break;
        case FLAG_WSBOOKING_CHECK:
            [self _parseGetCheck:JSON];
            break;
        default:
            break;
    }
    
    
}

- (void) _parseGetFail:(NSError *) error
{
    NSLog(@"WSBooking _parseGetFail %@", error);
}

#pragma mark - Parse GET ALL

- (void) _parseGetGetAll:(id)JSON
{
    if ([JSON objectForKey:@"success"]) {
        NSArray *data = [JSON objectForKey:@"data"];
        if ([data count] == 0) {
            
        }else{
            
            for (NSDictionary *booking in data) {
                NSDictionary *bookingData = [booking objectForKey:@"Booking"];
                [_bookings addObject:bookingData];
            }
            
        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(wsBooking:didGetAllBookingsSuccess:)]) {
            [[SBase dataSource] saveBookingsToDevice:[_bookings mutableCopy]];
            [self.delegate wsBooking:self didGetAllBookingsSuccess:_bookings];
        }
        
    }else{
        NSLog(@"WSBooking _parseGetSuccess FAIL %@", JSON);
    }

}

- (void) _parseGetCheck:(id)JSON
{
    NSLog(@"WSBooking _parseGetCheck JSON %@", JSON);
    
    
    if ([JSON objectForKey:@"success"]) {
        NSArray *data = [JSON objectForKey:@"data"];
        if ([data count] == 0) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(wsBooking:didCheckAvailabilityForLocationFail:)]) {
                //[[SBase dataSource] saveBookingsToDevice:[_bookings mutableCopy]];
                [self.delegate wsBooking:self didCheckAvailabilityForLocationFail:nil];
            }
        }else{
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(wsBooking:didCheckAvailabilityForLocationSuccess:)]) {
                //[[SBase dataSource] saveBookingsToDevice:[_bookings mutableCopy]];
                [self.delegate wsBooking:self didCheckAvailabilityForLocationSuccess:nil];
            }
            
        }
        
        
        
    }else{
        NSLog(@"WSBooking _parseGetSuccess FAIL %@", JSON);
    }
    
    
}


@end