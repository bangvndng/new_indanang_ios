//
//  WSUser.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/12/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "WSUser.h"
#import "Define.h"
#import "SBase.h"

@interface WSUser(){
    
}

@end

@implementation WSUser

- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)dealloc
{
    self.delegate = nil;
}

#pragma mark - PUBLIC FUNCTIONS

- (void) loginUser:(NSDictionary *)user
{

    self.flag = FLAG_WSBASE_WSUSER_POST_LOGIN;
    [self post:URL_WS_USER_LOGIN andPostParams:user];

}

- (void) loginFBUser:(NSDictionary *)user
{
    
    self.flag = FLAG_WSBASE_WSUSER_POST_LOGINFB;
    [self post:URL_WS_USER_LOGINFB andPostParams:user];
    
}

- (void) registerUser:(NSDictionary *)user
{
    self.flag = FLAG_WSBASE_WSUSER_POST_REGISTER;
    [self post:URL_WS_USER_REGISTER andPostParams:user];
    
}

#pragma mark - PRIVATE FUNCTIONS

- (void) _updateIUser:(NSDictionary *) userInfo
{
    [[SAccount currentUser] updateCurrentUser:userInfo];
}

#pragma mark - INHERITANCE FUNCTIONS

- (void) _parsePostSuccess:(id) JSON
{
    NSDictionary *result = [JSON copy];
    NSLog(@"WSUser _parsePostSuccess JSON %@", result);
    
    if ([[result objectForKey:@"success"]  isEqual: @0]) {
        // Fail
    }else{
        switch (self.flag) {
            case FLAG_WSBASE_WSUSER_POST_LOGIN:
                [self _updateIUser:[[result objectForKey:@"data"] objectForKey:@"User"]];
                
                if (self.delegate && [self.delegate respondsToSelector:@selector(wsUser:didLoginSuccessWith:)]) {
                    [self.delegate wsUser:self didLoginSuccessWith:[SAccount currentUser]];
                }
                
                break;
            case FLAG_WSBASE_WSUSER_POST_LOGINFB:
                [self _updateIUser:[[result objectForKey:@"data"] objectForKey:@"User"]];
                
                if (self.delegate && [self.delegate respondsToSelector:@selector(wsUser:didLoginFBSuccessWith:)]) {
                    [self.delegate wsUser:self didLoginFBSuccessWith:[SAccount currentUser]];
                }
                
                break;
            case FLAG_WSBASE_WSUSER_POST_REGISTER:
                [self _updateIUser:[[result objectForKey:@"data"] objectForKey:@"User"]];
                
                if (self.delegate && [self.delegate respondsToSelector:@selector(wsUser:didRegisterSuccessWith:)]) {
                    [self.delegate wsUser:self didRegisterSuccessWith:[SAccount currentUser]];
                }
                
                break;
            default:
                break;
        }
    }

}

- (void) _parsePostFail:(NSError *) error
{
    NSLog(@"WSUser _parsePostFail");
    NSLog(@"%@", error);
}

#pragma mark - DELEGATES



@end
