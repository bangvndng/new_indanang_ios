//
//  SBase.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/12/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SAccount.h"
#import "SDevice.h"
#import "SData.h"
#import "AppDelegate.h"
#import "Define.h"

@interface SBase : NSObject

#pragma mark - COMMON STATIC OBJECTS

+(AppDelegate *)appDelegate;
+(SAccount *)currentAccount;
+(SData *)dataSource;

#pragma mark - COMMON FUNCTIONS

+(void) showAlert:(NSString *)alertString;

@end
