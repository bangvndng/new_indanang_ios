//
//  WSRecommend.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/22/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSBase.h"
@protocol WSRecommendDelegate;

@interface WSRecommend : WSBase

@property (weak, nonatomic) id<WSRecommendDelegate> delegate;

- (void) addRecommend: (NSDictionary *)recommendInfo;

@end

@protocol WSRecommendDelegate <NSObject>

@optional

- (void)wsRecommend:(WSRecommend *)wsRecommend didAddRecommendSuccess:(NSDictionary *)info;
- (void)wsRecommend:(WSRecommend *)wsRecommend didAddRecommendFail:(NSError *)error;

@end
