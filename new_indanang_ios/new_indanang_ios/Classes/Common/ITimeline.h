//
//  ITimeline.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/29/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ITimeline : NSObject

@property (strong, nonatomic) NSString *timeline_id;
@property (strong, nonatomic) NSString *content;
@property (strong, nonatomic) NSString *user_id;
@property (strong, nonatomic) NSString *location_id;
@property (strong, nonatomic) NSString *created;
@property (strong, nonatomic) NSString *type;

@property (strong, nonatomic) NSString *user_displayname;
@property (strong, nonatomic) NSString *user_fbid;

@property (strong, nonatomic) UIImage *thumbnail;

@end
