//
//  LocationManager.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/25/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol LocationManagerDelegate;

@interface LocationManager : NSObject

@property (weak, nonatomic) id<LocationManagerDelegate> delegate;
@property (nonatomic, strong) CLLocationManager* locationManager;

+ (LocationManager*)sharedSingleton;

@end

@protocol LocationManagerDelegate <NSObject>

- (void)ilocationManager:(LocationManager *)ilocationManager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation;

@end