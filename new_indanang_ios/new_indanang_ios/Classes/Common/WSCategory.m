//
//  WSCategory.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/18/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "WSCategory.h"
#import "Define.h"

#import "SBase.h"

@interface WSCategory(){
    NSMutableArray *_categories;
    ICategory *_iCategory;
}

@end

@implementation WSCategory

- (id)init
{
    self = [super init];
    if (self) {
        _categories = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc
{
    self.delegate = nil;
    _categories = nil;
}

- (void)getCategory:(NSString *)category_id
{
    self.flag = FLAG_WSCATEGORY_VIEW;
    
    NSArray *keys = @[@"id"];
    NSArray *values = @[category_id];
    NSDictionary *getParams = [NSDictionary dictionaryWithObjects:values forKeys:keys];
    
    [self get:URL_WS_CATEGORY_VIEW andGetParams:getParams];
}

- (void)getAllCategories
{
    
    self.flag = FLAG_WSCATEGORY_LIST_ALL;
    [self get:URL_WS_CATEGORY_LIST_ALL andGetParams:nil];
    
}

#pragma mark - INHERITANCE FUNCTIONS

- (void) _parseGetSuccess:(id) JSON
{
    switch (self.flag) {
        case FLAG_WSCATEGORY_LIST_ALL:
            [self _parseGetAllCategories:JSON];
            break;
        case FLAG_WSCATEGORY_VIEW:
            [self _parseGetCategory:JSON];
            break;
        default:
            break;
    }

}

- (ICategory *) _parseCategory:(NSDictionary *)categoryData
{
    ICategory *iCategory    = [[ICategory alloc] init];
    
    iCategory.category_id   = [categoryData objectForKey:@"id"];
    iCategory.name          = [categoryData objectForKey:@"name"];
    iCategory.overview      = [categoryData objectForKey:@"overview"];
    iCategory.short_description = [categoryData objectForKey:@"short_description"];
    iCategory.parent_category_id    = [categoryData objectForKey:@"category_id"];
    iCategory.icon_class    = [categoryData objectForKey:@"icon_class"];
    iCategory.weight        = [NSNumber numberWithInt:[[categoryData objectForKey:@"weight"] intValue]];
    iCategory.hasLocation   = [NSNumber numberWithInt:[[categoryData objectForKey:@"hasLocation"] intValue]];
    iCategory.wysiwyg_content    = [categoryData objectForKey:@"wysiwyg_content"];
    return iCategory;
}

- (void) _parseGetCategory:(id)JSON
{
    if ([JSON objectForKey:@"success"]) {
        NSDictionary *data = [JSON objectForKey:@"data"];
        //if ([data count] == 0) {
            
        //}else{
        //    for (NSDictionary * category in data) {
                NSDictionary *categoryData = [data objectForKey:@"Category"];
                NSLog(@"WSCategory _parseGetCategory %@", categoryData);
                _iCategory = [self _parseCategory:categoryData];
                //[_categories addObject:[self _parseCategory:categoryData]];
                [_categories addObject:_iCategory];
        //    }
            
        //}
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(wsCategory:didGetCategorySuccess:)]) {
            [[SBase dataSource]updateCategorytoDevice:_iCategory];
            [self.delegate wsCategory:self didGetCategorySuccess:[_categories mutableCopy]];
        }
        
    }else{
        NSLog(@"WSCategory _parseGetSuccess FAIL %@", JSON);
    }
}

- (void) _parseGetAllCategories:(id)JSON
{
    if ([JSON objectForKey:@"success"]) {
        NSArray *data = [JSON objectForKey:@"data"];
        if ([data count] == 0) {
            
        }else{
            
            for (NSDictionary * category in data) {
                
                NSDictionary *categoryData = [category objectForKey:@"Category"];
                NSLog(@"%@",categoryData);
                [_categories addObject:[self _parseCategory:categoryData]];
            }
        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(wsCategory:didGetAllCategoriesSuccess:)]) {
            [[SBase dataSource]saveCategoriesToDevice:_categories];
            [self.delegate wsCategory:self didGetAllCategoriesSuccess:[_categories mutableCopy]];
        }
        
    }else{
        NSLog(@"WSCategory _parseGetAllCategories FAIL %@", JSON);
    }

    
}

- (void) _parseGetFail:(NSError *) error
{
    NSLog(@"%@", error);
    
}

@end
