//
//  WSTimeline.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/29/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSBase.h"

@protocol WSTimelineDelegate;

@interface WSTimeline : WSBase

@property (weak, nonatomic) id<WSTimelineDelegate>delegate;

- (void) getTimelineWithPage:(int)page andLimit:(int)limit;

@end

@protocol WSTimelineDelegate <NSObject>

- (void) wsTimeline:(WSTimeline *)wsTimeline didGetTimelineSuccess:(NSArray *)timelines page:(int)page limit:(int)limit count:(int)count;

@end