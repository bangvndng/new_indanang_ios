//
//  SDevice.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/12/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SDevice : NSObject

+(BOOL)isIOS7;

+(BOOL)isIPhone5;

@end
