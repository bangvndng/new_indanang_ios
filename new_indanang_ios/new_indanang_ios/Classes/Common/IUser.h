//
//  IUser.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/12/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IUser : NSObject

@property (strong, nonatomic) NSString *user_id;
@property (strong, nonatomic) NSString *username;
@property (strong, nonatomic) NSString *displayname;
@property (strong, nonatomic) NSString *password;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *fbid;
@property (strong, nonatomic) NSString *displayname_fb;

@property (strong, nonatomic) NSString *isAdmin;

- (BOOL) isLoggedIn;
- (BOOL) isFBUser;
- (void) updateCurrentUser: (NSDictionary *)userInfo;
- (BOOL) isAdminUser;

@end