//
//  IUser.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/12/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "IUser.h"

@implementation IUser

- (id)init
{
    self = [super init];
    if (self) {
        [self _getFromDevice];
    }
    return self;
}

#pragma mark - PUBLIC FUNCTIONS

- (BOOL) isAdminUser{
    return [_isAdmin isEqualToString:@"1"];
}

- (BOOL) isLoggedIn{
    if ([_user_id isEqualToString:@""] || !_user_id) {
        [self _getFromDevice];
    }
    
    return (![_user_id isEqualToString:@""] && _user_id);
}

- (BOOL) isFBUser{
    
    return (![_fbid isEqualToString:@""] && _fbid);
}

- (void) updateCurrentUser: (NSDictionary *)userInfo
{
    _user_id        = [userInfo objectForKey:@"id"];
    _username       = [userInfo objectForKey:@"username"];
    _displayname    = [userInfo objectForKey:@"displayname"];
    _password       = [userInfo objectForKey:@"password"];
    _email          = [userInfo objectForKey:@"email"];
    _fbid           = [userInfo objectForKey:@"fbid"];
    _displayname_fb = [userInfo objectForKey:@"displayname_fb"];
    
    _isAdmin        = [userInfo objectForKey:@"admin"];
    
    [self _syncToDevice];
    
}

- (void) _getFromDevice
{
    NSLog(@"IUser _getFromDevice");

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *user_id = [defaults objectForKey:@"currentUser_user_id"];
    _user_id = user_id;
    
    NSString *password = [defaults objectForKey:@"currentUser_password"];
    _password = password;
    
    NSString *username = [defaults objectForKey:@"currentUser_username"];
    _username = username;
    
    NSString *displayname = [defaults objectForKey:@"currentUser_displayname"];
    _displayname = displayname;
    
    NSString *email = [defaults objectForKey:@"currentUser_email"];
    _email = email;
    
    NSString *fbid = [defaults objectForKey:@"currentUser_fbid"];
    _fbid = fbid;
    
    NSString *displayname_fb = [defaults objectForKey:@"currentUser_displayname_fb"];
    _displayname_fb = displayname_fb;
    
    NSString *isAdmin = [defaults objectForKey:@"currentUser_displayname_isAdmin"];
    _isAdmin = isAdmin;
    
}

- (void) _syncToDevice
{
    NSLog(@"IUser _syncToDevice");
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:_user_id    forKey:@"currentUser_user_id"];
    [defaults setObject:_password   forKey:@"currentUser_password"];
    [defaults setObject:_email      forKey:@"currentUser_email"];
    [defaults setObject:_username   forKey:@"currentUser_username"];
    [defaults setObject:_displayname forKey:@"currentUser_displayname"];
    [defaults setObject:_fbid       forKey:@"currentUser_fbid"];
    [defaults setObject:_displayname_fb       forKey:@"currentUser_displayname_fb"];
    [defaults setObject:_isAdmin       forKey:@"currentUser_displayname_isAdmin"];
    
    [defaults synchronize];
}


#pragma mark - PRIVATE FUNCTIONS



@end