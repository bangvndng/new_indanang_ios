//
//  WSPhoto.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/18/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSBase.h"
#import "IPhoto.h"

@protocol WSPhotoDelegate;

@interface WSPhoto : WSBase

@property (weak, nonatomic) id<WSPhotoDelegate> delegate;

- (void) uploadPhoto:(UIImage *)photo forLocationId:(NSString *)location_id;
- (void) listPhotos:(NSString *)location_id;

@end

@protocol WSPhotoDelegate <NSObject>

@optional

- (void)wsPhoto:(WSPhoto *)wsPhoto didUploadPhotoSuccess:(NSDictionary *)info;
- (void)wsPhoto:(WSPhoto *)wsPhoto didUploadPhotoFail:(NSError *)error;

- (void)wsPhoto:(WSPhoto *)wsPhoto didListPhotosSuccess:(NSArray *)photos;
- (void)wsPhoto:(WSPhoto *)wsPhoto didListPhotosFail:(NSError *)error;

@end