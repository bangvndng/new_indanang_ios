//
//  WSLocationDetail.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 2/9/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSBase.h"

@protocol WSLocationDetailDelegate;

@interface WSLocationDetail : WSBase

@property (weak, nonatomic) id<WSLocationDetailDelegate> delegate;

- (void)getLocationDetails;
- (void)getLocationDetailsForLocation:(NSString *)location_id;

@end

@protocol WSLocationDetailDelegate <NSObject>

@optional
- (void)wsLocationDetail:(WSLocationDetail *)wsLocationDetail didGetLocationDetailSuccess:(NSDictionary *)info;
- (void)wsLocationDetail:(WSLocationDetail *)wsLocationDetail didGetLocationDetailFail:(NSError *)error;

- (void)wsLocationDetail:(WSLocationDetail *)wsLocationDetail didGetLocationDetailForLocationSuccess:(NSDictionary *)info;
- (void)wsLocationDetail:(WSLocationDetail *)wsLocationDetail didGetLocationDetailForLocationFail:(NSError *)error;

@end