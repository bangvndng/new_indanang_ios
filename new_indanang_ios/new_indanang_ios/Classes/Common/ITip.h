//
//  ITip.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/18/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ITip : NSObject

@property (strong, nonatomic) NSString *tip_id;
@property (strong, nonatomic) NSString *content;
@property (strong, nonatomic) NSString *user_id;
@property (strong, nonatomic) NSString *created;
@property (strong, nonatomic) NSString *location_id;

@end
