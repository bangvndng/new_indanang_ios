//
//  LocationManager.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/25/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "LocationManager.h"

@interface LocationManager()<CLLocationManagerDelegate>{
    
}
@end

@implementation LocationManager

- (id)init
{
    self = [super init];
    if (self) {
        self.locationManager = [CLLocationManager new];
        [self.locationManager setDelegate:(id)self];
        [self.locationManager setDistanceFilter:kCLDistanceFilterNone];
        [self.locationManager setHeadingFilter:kCLHeadingFilterNone];
        [self.locationManager startUpdatingLocation];
        //do any more customization to your location manager
    }
    return self;
}

+ (LocationManager*)sharedSingleton {
    static LocationManager* sharedSingleton;
    if(!sharedSingleton) {
        sharedSingleton = [LocationManager new];
        //@synchronized(sharedSingleton) {
        
        //}
    }
    
    return sharedSingleton;
}

- (void)dealloc
{
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    //handle your location updates here
    //NSLog(@"LocationManager didUpdateToLocation");
    if (self.delegate && [self.delegate respondsToSelector:@selector(ilocationManager:didUpdateToLocation:fromLocation:)]) {
        [self.delegate ilocationManager:self didUpdateToLocation:newLocation fromLocation:oldLocation];
    }
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    //handle your heading updates here- I would suggest only handling the nth update, because they
    //come in fast and furious and it takes a lot of processing power to handle all of them
}

@end
