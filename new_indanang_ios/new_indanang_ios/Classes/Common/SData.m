//
//  SData.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/29/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "SData.h"
#import "WSLocation.h"
#import "WSCategory.h"
#import "WSBooking.h"
#import "WSLocationDetail.h"
#import "WSCategoryDetail.h"

@interface SData()<WSLocationDelegate, WSCategoryDelegate, WSCategoryDetailDelegate, WSLocationDetailDelegate, WSBookingDelegate>{
    
}

@end

@implementation SData
- (id)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)dealloc
{
    
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

#pragma mark - PARSE DATA

- (ICategory *) _parseICategory:(NSManagedObject *)category
{
    ICategory *iCategory = [[ICategory alloc] init];
    iCategory.category_id           = [category valueForKey:@"category_id"];
    iCategory.name                  = [category valueForKey:@"name"];
    iCategory.overview              = [category valueForKey:@"overview"];
    iCategory.short_description     = [category valueForKey:@"short_description"];
    iCategory.parent_category_id    = [category valueForKey:@"parent_category_id"];
    iCategory.icon_class            = [category valueForKey:@"icon_class"];
    iCategory.weight                = [category valueForKey:@"weight"];
    iCategory.hasLocation                = [category valueForKey:@"hasLocation"];
    iCategory.wysiwyg_content                = [category valueForKey:@"wysiwyg_content"];
    return iCategory;
}

- (ILocation *) _parseILocation:(NSManagedObject *)location
{
    ILocation *iLocation = [[ILocation alloc] init];
    iLocation.location_id = [location valueForKey:@"location_id"];
    
    iLocation.title         = [location valueForKey:@"title"];
    iLocation.address       = [location valueForKey:@"address"];
    iLocation.address       = [location valueForKey:@"address"];
    iLocation.latitude      = [[location valueForKey:@"latitude"] doubleValue];
    iLocation.longitude     = [[location valueForKey:@"longitude"] doubleValue];
    iLocation.category_id   = [location valueForKey:@"category_id"];
    iLocation.overview      = [location valueForKey:@"overview"];
    iLocation.short_description = [location valueForKey:@"short_description"];
    iLocation.email         = [location valueForKey:@"email"];
    iLocation.phone         = [location valueForKey:@"phone"];
    iLocation.website       = [location valueForKey:@"website"];
    iLocation.thumbnail     = [location valueForKey:@"thumbnail"];
    iLocation.popular       = [location valueForKey:@"popular"];
    
    iLocation.suggestion    = [location valueForKey:@"suggestion"];
    //iLocation.isRecommended = [[location valueForKey:@"isRecommended"] boolValue];
    iLocation.distance      = [location valueForKey:@"distance"];
    
    iLocation.wysiwyg_content = [location valueForKey:@"wysiwyg_content"];
    
    iLocation.price    = [location valueForKey:@"price"];
    iLocation.reservable    = [location valueForKey:@"reservable"];
    
    return iLocation;
}

#pragma mark - LOCATIONS FUNCTIONS

#pragma mark - Locations functions

- (void) loadAllLocations:(BOOL) forceReload
{
    if (forceReload) {
        
        WSLocation *wsLocation = [[WSLocation alloc] init];
        wsLocation.delegate = (id)self;
        [wsLocation listAllLocations];

    }else{
        NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Location"];
        NSArray *locations = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if ([locations count] == 0) {
            WSLocation *wsLocation = [[WSLocation alloc] init];
            wsLocation.delegate = (id)self;
            [wsLocation listAllLocations];
            
        }else{
            NSMutableArray *results = [[NSMutableArray alloc] init];
            for (NSManagedObject *location in locations) {
                [results addObject:[self _parseILocation:location]];
            }
            
            [self _responseToDelegateLoadAllLocations:[results mutableCopy]];
        }

    }
    
}

- (ILocation *) loadLocationById:(NSString *) location_id
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Location"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"location_id = %@", location_id];
    [fetchRequest setPredicate:predicate];
    
    NSManagedObject *location = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] firstObject];
    
    if (location) {
        return [self _parseILocation:location];
    }else{
        return nil;
    }
}

- (NSArray *) loadLocationsByCategoryId:(NSString *)category_id
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Location"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"category_id = %@", category_id];
    [fetchRequest setPredicate:predicate];
    
    NSArray *locations = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    NSMutableArray *results = [[NSMutableArray alloc] init];
    for (NSManagedObject *location in locations) {
        [results addObject:[self _parseILocation:location]];
    }
    
    return [results mutableCopy];
}

- (NSArray *) loadLocationsBySuggestion
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Location"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"suggestion > %@", @0];
    [fetchRequest setPredicate:predicate];
    
    NSArray *locations = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    NSMutableArray *results = [[NSMutableArray alloc] init];
    for (NSManagedObject *location in locations) {
        [results addObject:[self _parseILocation:location]];
    }
    
    return [results mutableCopy];
}


#pragma mark - WSLocation Delegates

- (void)wsLocation:(WSLocation *)wsLocation didListAllLocationsSuccess:(NSArray *)locations
{
    [self _responseToDelegateLoadAllLocations:[locations mutableCopy]];
}

- (void)wsLocation:(WSLocation *)wsLocation didListAllLocationsFail:(NSError *)error
{
    
}

- (void)_responseToDelegateLoadAllLocations: (NSArray *)locations;
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(sData:didLoadAllLocationsSuccess:)]) {
        [self.delegate sData:self didLoadAllLocationsSuccess:locations];
    }
}

#pragma mark - Location Bookmark

- (BOOL) isBookmarked:(NSString *)location_id
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Bookmark"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"location_id = %@", location_id];
    [fetchRequest setPredicate:predicate];
    
    return !([[managedObjectContext executeFetchRequest:fetchRequest error:nil] firstObject] == nil);
}

- (void) addBookmark:(NSString *)location_id
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    ILocation *iLocation = [self loadLocationById:location_id];
    
    NSManagedObject *bookmarked = [NSEntityDescription insertNewObjectForEntityForName:@"Bookmark" inManagedObjectContext:managedObjectContext];
    [bookmarked setValue:location_id            forKey:@"location_id"];
    [bookmarked setValue:0                      forKey:@"recommended"];
    [bookmarked setValue:iLocation.category_id  forKey:@"category_id"];
    [bookmarked setValue:iLocation.title        forKey:@"title"];
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![managedObjectContext save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
}

#pragma mark - Save Location to device

- (void) saveLocationToDevice:(ILocation *)iLocation
{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Location"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"location_id = %@", iLocation.location_id];
    [fetchRequest setPredicate:predicate];
    
    NSManagedObject *location = [[context executeFetchRequest:fetchRequest error:nil] firstObject];
    
    if (location) {
        // Update CoreData
        //[self _parseDataToLocationManagedObject:location withILocation:iLocation];
    }else{
        // Insert a new one
        //NSManagedObject *newLocation = [NSEntityDescription insertNewObjectForEntityForName:@"Location" inManagedObjectContext:context];
        //[self _parseDataToLocationManagedObject:newLocation withILocation:iLocation];
    }
    NSError *error;
    if (![context save:&error]) {
        NSLog(@"Whoops");
    }
}

- (void) saveLocationsToDevice:(NSArray *)locations
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Location"];
    NSArray *old_locations = [context executeFetchRequest:fetchRequest error:nil];
    
    for (NSManagedObject *location in old_locations) {
        [context deleteObject:location];
    }
    
    for (ILocation *iLocation in locations) {
        NSManagedObject *newLocation = [NSEntityDescription insertNewObjectForEntityForName:@"Location" inManagedObjectContext:context];
        [self _parseDataToLocationManagedObject:newLocation withILocation:iLocation];
    }
    
    NSError *saveError = nil;
    [context save:&saveError];
}

- (void) updateLocationsToDevice:(NSArray *)locations
{
    NSMutableArray *updateLocations = [[NSMutableArray alloc] init];
    
    for (ILocation *iLocation in locations) {
        [updateLocations addObject:iLocation.location_id];
    }
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Location"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"location_id IN %@", updateLocations];
    [fetchRequest setPredicate:predicate];
    
    NSArray *old_locations = [context executeFetchRequest:fetchRequest error:nil];
    
    for (NSManagedObject *location in old_locations) {
        [context deleteObject:location];
    }
    
    for (ILocation *iLocation in locations) {
        NSManagedObject *newLocation = [NSEntityDescription insertNewObjectForEntityForName:@"Location" inManagedObjectContext:context];
        [self _parseDataToLocationManagedObject:newLocation withILocation:iLocation];
    }
    
    NSError *saveError = nil;
    [context save:&saveError];
    
    NSLog(@"SData updateLocationsToDevice Completed");
}

- (void) updateLocationToDevice:(ILocation *)iLocation
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Location"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"location_id = %@", iLocation.location_id];
    [fetchRequest setPredicate:predicate];
    
    NSManagedObject *location = [[context executeFetchRequest:fetchRequest error:nil] firstObject];
    
    [self _parseDataToLocationManagedObject:location withILocation:iLocation];
    
    NSError *saveError = nil;
    [context save:&saveError];
    
    NSLog(@"SData updateLocationToDevice - location info has been update to device");
    
}

#pragma mark - Location Suggestions

- (void) updateSuggestionsToDevice:(NSArray *)locations
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Location"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"suggestion > %@", @0];
    [fetchRequest setPredicate:predicate];
    
    NSArray *old_suggestions = [context executeFetchRequest:fetchRequest error:nil];
    
    NSMutableDictionary *new_suggestions = [[NSMutableDictionary alloc] init];
    
    for (ILocation *iLocation in locations) {
        [new_suggestions setObject:iLocation forKey:iLocation.location_id];
    }
    
    NSMutableArray *removing_suggestions = [[NSMutableArray alloc] init];
    
    for (NSManagedObject *old_suggestion in old_suggestions) {
        if ([new_suggestions objectForKey:[old_suggestion valueForKey:@"location_id"]]) {
            // old suggestion existed in new suggestion.
            // don't made change
            [new_suggestions removeObjectForKey:[old_suggestion valueForKey:@"location_id"]];
            
        }else{
            // Otherwise old suggestion not existed in new suggestion.
            // add to remove suggestion list
            [removing_suggestions addObject:old_suggestion];
        }
    }
    
    for (NSManagedObject *removing in removing_suggestions) {
        [removing setValue:@0 forKey:@"suggestion"];
    }

    NSMutableArray *adding_suggestions = [[NSMutableArray alloc] init];
    for (NSString *key in [new_suggestions allKeys]) {
        [adding_suggestions addObject:[(ILocation *)[new_suggestions objectForKey:key] location_id]];
    }
    
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"location_id IN %@", adding_suggestions];
    [fetchRequest setPredicate:predicate2];
    
    NSArray *adding_suggestions_objects = [context executeFetchRequest:fetchRequest error:nil];
    
    for (NSManagedObject *location in adding_suggestions_objects) {
        //[context deleteObject:location];
        [location setValue:@1 forKey:@"suggestion"];
    }
    
    NSError *saveError = nil;
    [context save:&saveError];
    
    NSLog(@"SData updateSuggestionsToDevice Completed");
}

- (void) _removeFromSuggestions
{
    
}

#pragma mark - Parse Data Location

- (void) _parseDataToLocationManagedObject:(NSManagedObject *)location withILocation:(ILocation *)iLocation
{
    [location setValue:iLocation.location_id   forKey:@"location_id"];
    [location setValue:[NSString stringWithFormat:@"%@",iLocation.title]         forKey:@"title"];
    [location setValue:iLocation.address       forKey:@"address"];
    [location setValue:iLocation.category_id   forKey:@"category_id"];
    
    [location setValue:[NSNumber numberWithDouble:iLocation.latitude]   forKey:@"latitude"];
    [location setValue:[NSNumber numberWithDouble:iLocation.longitude]  forKey:@"longitude"];
    
    //[location setValue:iLocation.overview           forKey:@"overview"];
    //[location setValue:iLocation.short_description  forKey:@"short_description"];
    
    
    if (![iLocation.overview isEqual:[NSNull null]]) {
        [location setValue:iLocation.overview           forKey:@"overview"];
    }else{
        [location setValue:@""           forKey:@"overview"];
    }
    
    if (![iLocation.short_description isEqual:[NSNull null]]) {
        [location setValue:iLocation.short_description           forKey:@"short_description"];
    }else{
        [location setValue:@""           forKey:@"short_description"];
    }
    
    
    [location setValue:iLocation.email              forKey:@"email"];
    [location setValue:iLocation.phone              forKey:@"phone"];
    [location setValue:iLocation.website            forKey:@"website"];
    
    [location setValue:iLocation.thumbnail          forKey:@"thumbnail"];
    
    [location setValue:iLocation.popular            forKey:@"popular"];
    
    [location setValue:iLocation.distance           forKey:@"distance"];
    [location setValue:iLocation.suggestion         forKey:@"suggestion"];
    
    [location setValue:iLocation.wysiwyg_content forKey:@"wysiwyg_content"];
    
    [location setValue:iLocation.price         forKey:@"price"];
    [location setValue:iLocation.reservable         forKey:@"reservable"];
}

#pragma mark - CATEGORY FUNCTIONS

#pragma mark - Categories Functions

- (NSArray *) loadAllCategories
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"LocationCategory"];
    NSArray *categories = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    if ([categories count] == 0) {
        return nil;
    }else{
        NSMutableArray *results = [[NSMutableArray alloc] init];
        for (NSManagedObject *category in categories) {
            [results addObject:[self _parseICategory:category]];
        }
        
        return [results mutableCopy];
    }

}

- (void) loadAllCategories:(BOOL) forceReload
{
    if (forceReload) {
        WSCategory *wsCategory = [[WSCategory alloc] init];
        wsCategory.delegate = (id)self;
        [wsCategory getAllCategories];
    }else{
        NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"LocationCategory"];
        NSArray *categories = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if ([categories count] == 0) {
            WSCategory *wsCategory = [[WSCategory alloc] init];
            wsCategory.delegate = (id)self;
            [wsCategory getAllCategories];
        }else{
            NSMutableArray *results = [[NSMutableArray alloc] init];
            for (NSManagedObject *category in categories) {
                [results addObject:[self _parseICategory:category]];
            }
            
            [self _responseToDelegateLoadAllCategories:[results mutableCopy]];
        }
    }
}

- (ICategory *) loadCategoryById:(NSString *) category_id
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"LocationCategory"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"category_id = %@", category_id];
    [fetchRequest setPredicate:predicate];
    
    NSManagedObject *category = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] firstObject];
    
    if (category) {
        return  [self _parseICategory:category];
    }else{
        return nil;
    }
    
}

- (NSArray *) loadChildrenCategoriesById:(NSString *) category_id
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"LocationCategory"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"parent_category_id = %@", category_id];
    [fetchRequest setPredicate:predicate];
    
    [fetchRequest setSortDescriptors:@[[[NSSortDescriptor alloc] initWithKey:@"weight" ascending:YES]]];
    
    NSArray *categories = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    return categories;
}

- (NSArray *) loadLeafCategories
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"LocationCategory"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"parent_category_id != %@", @0];
    [fetchRequest setPredicate:predicate];
    
    NSArray *categories = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    NSMutableArray *results = [[NSMutableArray alloc] init];
    for (NSManagedObject *category in categories) {
        [results addObject:[self _parseICategory:category]];
    }
    
    return  [results mutableCopy];
}

#pragma mark - WSCategory Delegates

- (void)wsCategory:(WSCategory *)wsCategory didGetAllCategoriesSuccess:(NSArray *)categories
{
    [self _responseToDelegateLoadAllCategories:[categories mutableCopy]];
}

- (void)wsCategory:(WSCategory *)wsCategory didGetAllCategoriesFail:(NSError *)error
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(sData:didLoadAllCategoriesFail:)]) {
        [self.delegate sData:self didLoadAllCategoriesFail:error];
    }
}

- (void)_responseToDelegateLoadAllCategories:(NSArray *)categories
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(sData:didLoadAllCategoriesSuccess:)]) {
        [self.delegate sData:self didLoadAllCategoriesSuccess:categories];
    }
}

#pragma mark - Save categories to device

- (void) saveCategoriesToDevice:(NSArray *)categories
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"LocationCategory"];
    NSArray *old_categories = [context executeFetchRequest:fetchRequest error:nil];
    
    for (NSManagedObject *category in old_categories) {
        [context deleteObject:category];
    }
    
    for (ICategory *iCategory in categories) {
        NSManagedObject *newCategory = [NSEntityDescription insertNewObjectForEntityForName:@"LocationCategory" inManagedObjectContext:context];
        [self _parseDataToCategoryManagedObject:newCategory withICategory:iCategory];
    }
    
    NSError *saveError = nil;
    [context save:&saveError];
    
}

- (void) updateCategoriesToDevice:(NSArray *)categories
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"LocationCategory"];
    NSArray *old_categories = [context executeFetchRequest:fetchRequest error:nil];
    
    for (NSManagedObject *category in old_categories) {
        [context deleteObject:category];
    }
    
    for (ICategory *iCategory in categories) {
        NSManagedObject *newCategory = [NSEntityDescription insertNewObjectForEntityForName:@"LocationCategory" inManagedObjectContext:context];
        [self _parseDataToCategoryManagedObject:newCategory withICategory:iCategory];
    }
    
    NSError *saveError = nil;
    [context save:&saveError];
    
}

- (void) updateCategorytoDevice:(ICategory *)iCategory
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"LocationCategory"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"category_id = %@", iCategory.category_id];
    [fetchRequest setPredicate:predicate];
    
    NSManagedObject *category = [[context executeFetchRequest:fetchRequest error:nil] firstObject];
    
    [self _parseDataToCategoryManagedObject:category withICategory:iCategory];
    
    NSError *saveError = nil;
    [context save:&saveError];
    
    NSLog(@"SData updateCategorytoDevice - category info has been update to device");
    
}

#pragma mark - Parse Data Categories

- (void) _parseDataToCategoryManagedObject:(NSManagedObject *)category withICategory:(ICategory *)iCategory
{
    [category setValue:iCategory.category_id        forKey:@"category_id"];
    [category setValue:iCategory.name               forKey:@"name"];
    
    
    if (![iCategory.short_description isEqual:[NSNull null]]) {
        [category setValue:iCategory.short_description  forKey:@"short_description"];
    }else{
        [category setValue:@""  forKey:@"short_description"];
    }
    
    if (![iCategory.overview isEqual:[NSNull null]]) {
        [category setValue:iCategory.overview  forKey:@"overview"];
    }else{
        [category setValue:@""  forKey:@"overview"];
    }

    [category setValue:iCategory.parent_category_id forKey:@"parent_category_id"];
    [category setValue:iCategory.icon_class         forKey:@"icon_class"];
    [category setValue:iCategory.weight             forKey:@"weight"];
    [category setValue:iCategory.hasLocation        forKey:@"hasLocation"];
    [category setValue:iCategory.wysiwyg_content    forKey:@"wysiwyg_content"];
}


#pragma mark - LOCATION DETAILS 

#pragma mark - Location Details Functions

- (void) loadLocationDetails
{
    WSLocationDetail *wsLocationDetails = [[WSLocationDetail alloc] init];
    wsLocationDetails.delegate = (id)self;
    [wsLocationDetails getLocationDetails];
}

- (void) loadWSLocationDetailsForLocationId:(NSString *)location_id
{
    WSLocationDetail *wsLocationDetails = [[WSLocationDetail alloc] init];
    wsLocationDetails.delegate = (id)self;
    [wsLocationDetails getLocationDetailsForLocation:location_id];
}

- (NSArray *) loadLocationDetailsByLocationId:(NSString *) location_id
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"LocationDetail"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"location_id = %@", location_id];
    [fetchRequest setPredicate:predicate];
    
    NSArray *locationDetails = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    return locationDetails;
}


#pragma mark - WSLocationDetail Delegate

- (void) wsLocationDetail:(WSLocationDetail *)wsLocationDetail didGetLocationDetailSuccess:(NSDictionary *)info
{
    
}

- (void) wsLocationDetail:(WSLocationDetail *)wsLocationDetail didGetLocationDetailFail:(NSError *)error
{
    
}

#pragma mark - Location Details Parse Data

- (void) _parseDataToLocationDetailManagedObject:(NSManagedObject *)newLocationDetail withData:(NSDictionary *)data
{
    [newLocationDetail setValue:[data objectForKey:@"id"]           forKey:@"detail_id"];
    [newLocationDetail setValue:[data objectForKey:@"location_id"]  forKey:@"location_id"];
    [newLocationDetail setValue:[data objectForKey:@"header"]       forKey:@"header"];
    [newLocationDetail setValue:[data objectForKey:@"content"]      forKey:@"content"];
}

#pragma mark - Location Details Save to Device

- (void) saveLocationDetailsToDevice:(NSArray *)locationDetails
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"LocationDetail"];
    NSArray *old_locationdetails = [context executeFetchRequest:fetchRequest error:nil];
    
    for (NSManagedObject *old_locationdetail in old_locationdetails) {
        [context deleteObject:old_locationdetail];
    }
    
    for (NSDictionary *locationDetail in locationDetails) {
        NSManagedObject *newLocationDetail = [NSEntityDescription insertNewObjectForEntityForName:@"LocationDetail" inManagedObjectContext:context];
        [self _parseDataToLocationDetailManagedObject:newLocationDetail withData:locationDetail];
    }
    
    NSError *saveError = nil;
    [context save:&saveError];
}

- (void) saveLocationDetailsToDevice:(NSArray *)locationDetails forLocationId:(NSString *)location_id;
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"LocationDetail"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"location_id = %@", location_id];
    [fetchRequest setPredicate:predicate];
    
    NSArray *old_locationdetails = [context executeFetchRequest:fetchRequest error:nil];
    
    for (NSManagedObject *old_locationdetail in old_locationdetails) {
        [context deleteObject:old_locationdetail];
    }
    
    for (NSDictionary *locationDetail in locationDetails) {
        NSManagedObject *newLocationDetail = [NSEntityDescription insertNewObjectForEntityForName:@"LocationDetail" inManagedObjectContext:context];
        [self _parseDataToLocationDetailManagedObject:newLocationDetail withData:locationDetail];
    }
    
    NSError *saveError = nil;
    [context save:&saveError];
}


#pragma mark - CATEGORY DETAILS

#pragma mark - Cagegories Function

- (void) loadCategoryDetails
{
    WSCategoryDetail *wsCategoryDetail = [[WSCategoryDetail alloc] init];
    wsCategoryDetail.delegate = (id)self;
    [wsCategoryDetail getCategoryDetail];
}

- (void) loadWSCategoryDetailsByCategoryId:(NSString *)category_id
{
    WSCategoryDetail *wsCategoryDetail = [[WSCategoryDetail alloc] init];
    wsCategoryDetail.delegate = (id)self;
    [wsCategoryDetail getCategoryDetailForCategory:category_id];
    
}

- (NSArray *) loadCategoryDetailsByCategoryId:(NSString *) category_id
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"CategoryDetail"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"category_id = %@", category_id];
    [fetchRequest setPredicate:predicate];
    
    NSArray *categoryDetails = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    return categoryDetails;
}


#pragma mark - WSCategoryDetail Delegates

- (void) wsCategoryDetail:(WSCategoryDetail *)wsCategoryDetail didGetCategoryDetailSuccess:(NSDictionary *)info
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(sData:didLoadCategoryDetailsSuccess:)]) {
        [self.delegate sData:self didLoadCategoryDetailsSuccess:nil];
    }
}

- (void) wsCategoryDetail:(WSCategoryDetail *)wsCategoryDetail didGetCategoryDetailFail:(NSError *)error
{
    
}

- (void)_parseDataToCategoryDetailManagedObject:(NSManagedObject *)newCategoryDetail withData:(NSDictionary *)data
{
    [newCategoryDetail setValue:[data objectForKey:@"id"]           forKey:@"detail_id"];
    [newCategoryDetail setValue:[data objectForKey:@"category_id"]  forKey:@"category_id"];
    [newCategoryDetail setValue:[data objectForKey:@"header"]       forKey:@"header"];
    [newCategoryDetail setValue:[data objectForKey:@"content"]      forKey:@"content"];
}

- (void) saveCategoryDetailsToDevice:(NSArray *)categoryDetails
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"CategoryDetail"];
    NSArray *old_categorydetails = [context executeFetchRequest:fetchRequest error:nil];
    
    for (NSManagedObject *old_categorydetail in old_categorydetails) {
        [context deleteObject:old_categorydetail];
    }
    
    for (NSDictionary *categoryDetail in categoryDetails) {
        NSManagedObject *newCategoryDetail = [NSEntityDescription insertNewObjectForEntityForName:@"CategoryDetail" inManagedObjectContext:context];
        [self _parseDataToCategoryDetailManagedObject:newCategoryDetail withData:categoryDetail];
    }
    
    NSError *saveError = nil;
    [context save:&saveError];
}

- (void) saveCategoryDetailsToDevice:(NSArray *)categoryDetails forCategoryId:(NSString *)category_id
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"CategoryDetail"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"category_id = %@", category_id];
    [fetchRequest setPredicate:predicate];
    
    NSArray *old_categorydetails = [context executeFetchRequest:fetchRequest error:nil];
    
    for (NSManagedObject *old_categorydetail in old_categorydetails) {
        [context deleteObject:old_categorydetail];
    }
    
    for (NSDictionary *categoryDetail in categoryDetails) {
        NSManagedObject *newCategoryDetail = [NSEntityDescription insertNewObjectForEntityForName:@"CategoryDetail" inManagedObjectContext:context];
        [self _parseDataToCategoryDetailManagedObject:newCategoryDetail withData:categoryDetail];
    }
    
    NSError *saveError = nil;
    [context save:&saveError];
}


#pragma mark - BOOKINGS

#pragma mark - Bookings Functions

- (NSArray *) getBookingsForUser:(NSString *)user_id andLocation:(NSString *)location_id forDay:(NSString *)checkin
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Booking"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"location_id = %@ AND user_id=%@ AND checkin=%@",
                              location_id,
                              user_id,
                              checkin
                              ];
    [fetchRequest setPredicate:predicate];
    
    NSArray *bookings = [context executeFetchRequest:fetchRequest error:nil];
    
    return bookings;
}

- (void) loadAllBooking:(BOOL)forceReload{
    if (forceReload) {
        
        WSBooking *wsBooking = [[WSBooking alloc] init];
        wsBooking.delegate = (id)self;
        [wsBooking getAllBookings];
        
    }else{
        
        NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Booking"];
        NSArray *bookings = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
        
        if ([bookings count] == 0) {
            
            WSBooking *wsBooking = [[WSBooking alloc] init];
            wsBooking.delegate = (id)self;
            [wsBooking getAllBookings];
            
        }else{
            /*
             NSMutableArray *results = [[NSMutableArray alloc] init];
             for (NSManagedObject *booking in bookings) {
             //[results addObject:[self _parseILocation:location]];
             }
             */
            
            [self _responseToDelegateLoadAllBookings:bookings];
        }
        
    }
}

# pragma mark - Save to device

- (void) saveBookingToDevice:(NSDictionary *)bookingInfo
{
    NSLog(@"SData saveBookingToDevice bookingInfo %@", bookingInfo);
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Booking"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"location_id = %@ AND user_id=%@ AND checkin=%@",
                              [bookingInfo objectForKey:@"location_id"],
                              [bookingInfo objectForKey:@"user_id"],
                              [bookingInfo objectForKey:@"checkin"]
                              ];
    [fetchRequest setPredicate:predicate];
    
    NSArray *old_bookings = [context executeFetchRequest:fetchRequest error:nil];
    
    for (NSManagedObject *old_booking in old_bookings) {
        [context deleteObject:old_booking];
    }
    
    NSManagedObject *newBooking = [NSEntityDescription insertNewObjectForEntityForName:@"Booking" inManagedObjectContext:context];
    
    [newBooking setValue:[bookingInfo objectForKey:@"checkin"] forKey:@"checkin"];
    [newBooking setValue:[bookingInfo objectForKey:@"checkout"] forKey:@"checkout"];
    [newBooking setValue:[bookingInfo objectForKey:@"created"] forKey:@"created"];
    [newBooking setValue:[bookingInfo objectForKey:@"location_id"] forKey:@"location_id"];
    [newBooking setValue:[NSNumber numberWithInt:[[bookingInfo objectForKey:@"quantity"] intValue]] forKey:@"quantity"];
    [newBooking setValue:[bookingInfo objectForKey:@"user_id"] forKey:@"user_id"];
    [newBooking setValue:[bookingInfo objectForKey:@"id"] forKey:@"booking_id"];
    
    NSError *saveError = nil;
    [context save:&saveError];
    
}

- (void) saveBookingsToDevice:(NSArray *)bookings
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Booking"];
    
    NSArray *old_bookings = [context executeFetchRequest:fetchRequest error:nil];
    
    for (NSManagedObject *old_booking in old_bookings) {
        [context deleteObject:old_booking];
    }
    
    for (NSDictionary * bookingInfo in bookings) {
        NSManagedObject *newBooking = [NSEntityDescription insertNewObjectForEntityForName:@"Booking" inManagedObjectContext:context];
        
        [newBooking setValue:[bookingInfo objectForKey:@"checkin"] forKey:@"checkin"];
        [newBooking setValue:[bookingInfo objectForKey:@"checkout"] forKey:@"checkout"];
        [newBooking setValue:[bookingInfo objectForKey:@"created"] forKey:@"created"];
        [newBooking setValue:[bookingInfo objectForKey:@"location_id"] forKey:@"location_id"];
        [newBooking setValue:[NSNumber numberWithInt:[[bookingInfo objectForKey:@"quantity"] intValue]] forKey:@"quantity"];
        [newBooking setValue:[bookingInfo objectForKey:@"user_id"] forKey:@"user_id"];
        [newBooking setValue:[bookingInfo objectForKey:@"id"] forKey:@"booking_id"];
    }
    
    NSError *saveError = nil;
    [context save:&saveError];
}

#pragma mark - WSBooking Delegates
- (void)wsBooking:(WSBooking *)wsBooking didGetAllBookingsSuccess:(NSArray *)bookings
{
    [self _responseToDelegateLoadAllBookings:bookings];
}

- (void)wsBooking:(WSBooking *)wsBooking didGetAllBookingsFail:(NSError *)error
{
    
}

- (void) _responseToDelegateLoadAllBookings:(NSArray *)booking{
    if (self.delegate && [self.delegate respondsToSelector:@selector(sData:didLoadAllBookingsSuccess:)]) {
        [self.delegate sData:self didLoadAllBookingsSuccess:booking];
    }
}

@end
