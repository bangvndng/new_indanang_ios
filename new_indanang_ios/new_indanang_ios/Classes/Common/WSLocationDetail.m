//
//  WSLocationDetail.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 2/9/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import "WSLocationDetail.h"
#import "Define.h"
#import "SBase.h"

#define FLAG_GET_ALL 0
#define FLAG_GET_ONE 1

@interface WSLocationDetail(){
    NSMutableArray *_locationDetails;
    
    int _flag;
    NSString *_location_id;
}

@end

@implementation WSLocationDetail

- (id)init
{
    self = [super init];
    if (self) {
        _locationDetails = [[NSMutableArray alloc] init];
        
        _flag = FLAG_GET_ALL;
    }
    return self;
}

- (void)dealloc
{
    self.delegate = nil;
    
    _locationDetails = nil;
    
}

- (void)getLocationDetails
{
    NSArray *keys   = @[@"page", @"limit"];
    NSArray *values = @[@"1", @"99999"];
    NSDictionary *getParams = [NSDictionary dictionaryWithObjects:values forKeys:keys];
    
    _flag = FLAG_GET_ALL;
    [self get:URL_WS_LOCATIONDETAIL_INDEX andGetParams:getParams];
}

- (void)getLocationDetailsForLocation:(NSString *)location_id
{
    NSArray *keys   = @[@"page", @"limit"];
    NSArray *values = @[@"1", @"99999"];
    NSDictionary *getParams = [NSDictionary dictionaryWithObjects:values forKeys:keys];
    
    NSString *urlString = [NSString stringWithFormat:@"%@?node=%@",URL_WS_LOCATIONDETAIL_INDEX, location_id];
    
    _location_id = location_id;
    _flag = FLAG_GET_ONE;
    [self get:urlString andGetParams:getParams];
}

#pragma mark - INHERITANCE FUNCTIONS

- (void) _parseGetSuccess:(id) JSON
{
    if ([JSON objectForKey:@"success"]) {
        NSArray *data = [JSON objectForKey:@"data"];
        if ([data count] == 0) {
            
        }else{
            
            if (_flag == FLAG_GET_ONE) {
                for (NSDictionary * location in data) {
                    //NSDictionary *locationDetailData = [location objectForKey:@"Locationdetail"];
                    [_locationDetails addObject:location];
                }
            }else{
                for (NSDictionary * location in data) {
                    NSDictionary *locationDetailData = [location objectForKey:@"Locationdetail"];
                    [_locationDetails addObject:locationDetailData];
                }
            }
            
            
            
        }
        
        if (_flag == FLAG_GET_ONE) {
            [[SBase dataSource] saveLocationDetailsToDevice:[_locationDetails mutableCopy] forLocationId:_location_id];
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(wsLocationDetail:didGetLocationDetailForLocationSuccess:)]) {
                //[[SBase dataSource] saveLocationsToDevice:[_locations mutableCopy]];
                [self.delegate wsLocationDetail:self didGetLocationDetailForLocationSuccess:[_locationDetails mutableCopy]];
            }
            
        }else{
            [[SBase dataSource] saveLocationDetailsToDevice:[_locationDetails mutableCopy]];
            if (self.delegate && [self.delegate respondsToSelector:@selector(wsLocationDetail:didGetLocationDetailSuccess:)]) {
                //[[SBase dataSource] saveLocationsToDevice:[_locations mutableCopy]];
                [self.delegate wsLocationDetail:self didGetLocationDetailSuccess:[_locationDetails mutableCopy]];
            }
        }
        
        
        
    }else{
        NSLog(@"WSLocationDetail _parseGetSuccess FAIL %@", JSON);
    }
    
    
}

- (void) _parseGetFail:(NSError *) error
{
    NSLog(@"WSLocationDetail _parseGetFail %@", error);
}

@end
