//
//  ILocation.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/18/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ITransporter.h"

@interface ILocation : NSObject

@property (strong, nonatomic) NSString *location_id;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *short_description;
@property (strong, nonatomic) NSString *overview;
@property (strong, nonatomic) NSString *phone;
@property (strong, nonatomic) NSString *website;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *category_id;

@property (nonatomic) double latitude;
@property (nonatomic) double longitude;

@property (strong, nonatomic) NSString *thumbnail;

@property (strong, nonatomic) NSArray *tranporters;
@property (strong, nonatomic) NSString *popular;
@property (strong, nonatomic) NSString *distance;

@property (strong, nonatomic) NSNumber *suggestion;

@property (nonatomic) BOOL isRecommended;

@property (strong, nonatomic) NSString *wysiwyg_content;

@property (strong, nonatomic) NSNumber *price;
@property (strong, nonatomic) NSNumber *reservable;

@end
