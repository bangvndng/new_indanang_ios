//
//  WSForecast.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 1/17/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSBase.h"

@protocol WSForecastDelegate;

@interface WSForecast : WSBase

@property (weak, nonatomic) id<WSForecastDelegate> delegate;

- (void)getForecast:(NSDictionary *)locationInfo;

@end

@protocol WSForecastDelegate <NSObject>

- (void)wsForecast:(WSForecast *)wsForecast didGetForecaseSuccess:(NSDictionary *)forecastData;
- (void)wsForecast:(WSForecast *)wsForecast didGetForecaseFail:(NSError *)error;

@end