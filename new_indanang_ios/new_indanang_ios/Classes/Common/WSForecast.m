//
//  WSForecast.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 1/17/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import "WSForecast.h"
#import "Define.h"

@interface WSForecast(){
    NSDictionary *_forecastData;
}

@end

@implementation WSForecast

- (id)init
{
    self = [super init];
    if (self) {
        _forecastData = [[NSDictionary alloc] init];
    }
    return self;
}

- (void)dealloc
{
    self.delegate = nil;
    _forecastData = nil;
}

- (void)getForecast:(NSDictionary *)locationInfo
{
    NSString *latlong = [NSString stringWithFormat:@"%@,%@",
                         [locationInfo objectForKey:@"latitude"],
                         [locationInfo objectForKey:@"longitude"]];
    
    NSString *urlForecast = [NSString stringWithFormat:@"%@/%@/%@?units=si",
                             URL_WS_FORECAST,
                             WS_FORECAST_API_KEY,
                             latlong];
    
    [self customGet:urlForecast];
}

/*
- (void)wsForecast:(WSForecast *)wsForecast didGetForecaseSuccess:(NSDictionary *)forecastData
{
    
}
- (void)wsForecast:(WSForecast *)wsForecast didGetForecaseFail:(NSError *)error
{
    
}
*/
// ABSTRACT FUNCTION
- (void) _parseGetSuccess:(id) JSON
{
    _forecastData = [JSON mutableCopy];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(wsForecast:didGetForecaseSuccess:)]) {
        [self.delegate wsForecast:self didGetForecaseSuccess:_forecastData];
    }
}

// ABSTRACT FUNCTION
- (void) _parseGetFail:(NSError *) error
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(wsForecast:didGetForecaseFail:)]) {
        [self.delegate wsForecast:self didGetForecaseFail:error];
    }
}

@end