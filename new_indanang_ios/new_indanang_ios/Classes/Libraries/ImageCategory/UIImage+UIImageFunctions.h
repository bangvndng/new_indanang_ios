//
//  UIImage+UIImageFunctions.h
//  newindanang
//
//  Created by Bang Ngoc Vu on 08/09/2013.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (UIImageFunctions)
- (UIImage *) scaleToSize: (CGSize)size;
- (UIImage *) scaleProportionalToSize: (CGSize)size;
@end
