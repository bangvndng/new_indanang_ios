//
//  Location.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 3/10/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import "Location.h"


@implementation Location

@dynamic address;
@dynamic category_id;
@dynamic distance;
@dynamic email;
@dynamic latitude;
@dynamic location_id;
@dynamic longitude;
@dynamic overview;
@dynamic phone;
@dynamic popular;
@dynamic short_description;
@dynamic suggestion;
@dynamic thumbnail;
@dynamic title;
@dynamic website;
@dynamic wysiwyg_content;
@dynamic price;
@dynamic reservable;

@end
