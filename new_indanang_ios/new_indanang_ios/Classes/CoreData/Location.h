//
//  Location.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 3/10/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Location : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * category_id;
@property (nonatomic, retain) NSString * distance;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSString * location_id;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSString * overview;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * popular;
@property (nonatomic, retain) NSString * short_description;
@property (nonatomic, retain) NSNumber * suggestion;
@property (nonatomic, retain) NSString * thumbnail;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * website;
@property (nonatomic, retain) NSString * wysiwyg_content;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) NSNumber * reservable;

@end
