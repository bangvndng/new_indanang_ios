//
//  Bookmark.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/28/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Bookmark : NSManagedObject

@property (nonatomic, retain) NSString * category_id;
@property (nonatomic, retain) NSString * location_id;
@property (nonatomic, retain) NSNumber * recommended;
@property (nonatomic, retain) NSString * title;

@end
