//
//  Bookmark.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/28/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import "Bookmark.h"


@implementation Bookmark

@dynamic category_id;
@dynamic location_id;
@dynamic recommended;
@dynamic title;

@end
