//
//  LocationCategory.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 2/9/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface LocationCategory : NSManagedObject

@property (nonatomic, retain) NSString * category_id;
@property (nonatomic, retain) NSString * icon_class;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * overview;
@property (nonatomic, retain) NSString * parent_category_id;
@property (nonatomic, retain) NSString * short_description;
@property (nonatomic, retain) NSNumber * weight;
@property (nonatomic, retain) NSNumber * hasLocation;
@property (nonatomic, retain) NSString * wysiwyg_content;

@end
