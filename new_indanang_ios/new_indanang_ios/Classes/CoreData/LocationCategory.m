//
//  LocationCategory.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 2/9/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import "LocationCategory.h"


@implementation LocationCategory

@dynamic category_id;
@dynamic icon_class;
@dynamic name;
@dynamic overview;
@dynamic parent_category_id;
@dynamic short_description;
@dynamic weight;
@dynamic hasLocation;
@dynamic wysiwyg_content;

@end
