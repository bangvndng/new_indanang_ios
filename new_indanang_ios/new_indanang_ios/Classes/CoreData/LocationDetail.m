//
//  LocationDetail.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 2/9/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import "LocationDetail.h"


@implementation LocationDetail

@dynamic detail_id;
@dynamic location_id;
@dynamic header;
@dynamic content;

@end
