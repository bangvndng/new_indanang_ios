//
//  Booking.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 3/13/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Booking : NSManagedObject

@property (nonatomic, retain) NSString * booking_id;
@property (nonatomic, retain) NSString * user_id;
@property (nonatomic, retain) NSString * checkin;
@property (nonatomic, retain) NSString * checkout;
@property (nonatomic, retain) NSNumber * quantity;
@property (nonatomic, retain) NSString * location_id;
@property (nonatomic, retain) NSString * created;

@end
