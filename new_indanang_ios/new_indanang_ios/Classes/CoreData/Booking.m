//
//  Booking.m
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 3/13/14.
//  Copyright (c) 2014 Bang Ngoc Vu. All rights reserved.
//

#import "Booking.h"


@implementation Booking

@dynamic booking_id;
@dynamic user_id;
@dynamic checkin;
@dynamic checkout;
@dynamic quantity;
@dynamic location_id;
@dynamic created;

@end
