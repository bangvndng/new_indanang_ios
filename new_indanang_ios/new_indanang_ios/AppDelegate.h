//
//  AppDelegate.h
//  new_indanang_ios
//
//  Created by Bang Ngoc Vu on 12/12/13.
//  Copyright (c) 2013 Bang Ngoc Vu. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MainVC.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

+ (AppDelegate *)sharedSingleton;

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@property (strong, nonatomic) MainVC *mainVC;

//@property (strong, nonatomic) NSArray *locations;
//@property (strong, nonatomic) NSArray               *sharedCategories;
@property (strong, nonatomic) NSMutableDictionary   *sharedCategoriesIcon;

@end
